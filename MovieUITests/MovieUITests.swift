//
//  MovieUITests.swift
//  MovieUITests
//
//  Created by Alwyn Yeo on 1/16/21.
//

import XCTest

class MovieUITests: XCTestCase {
    var app: XCUIApplication!

    override func setUp() {
        super.setUp()
        // Since UI tests are more expensive to run, it's usually a good idea
        // to exit if a failure was encountered
        continueAfterFailure = false

        app = XCUIApplication()

        // We send a command line argument to our app,
        // to enable it to reset its state
        app.launchArguments = ["--uitesting"]
    }

    func test_going_through_home_screen() {
        // Launch App
        app.launch()

        // Make sure we are displaying the Home screen
        XCTAssertTrue(app.isDisplayingHomeScreen)

        // Wait for the data to appear
        sleep(3)

        // Make sure the spinner is disappeared
        XCTAssertFalse(app.activityIndicators.element.exists)
        // Make sure the cells exist
        XCTAssertTrue(app.cells.element.exists)

//        // Scroll down
//        app.swipeUp(velocity: .fast)
//        app.swipeUp(velocity: .fast)
//        app.swipeUp(velocity: .fast)
//
//        // Scroll up
//        app.swipeDown(velocity: .fast)
//        app.swipeDown(velocity: .fast)
//        app.swipeDown(velocity: .fast)
    }

    func test_going_through_home_detail_list_screen() {
        test_going_through_home_screen()

        // Tap the See All button
        app.buttons["See All"].firstMatch.tap()

        // Make sure the cells exist
        XCTAssertTrue(app.cells.element.exists)

        // Scroll down
        app.swipeUp(velocity: .fast)
        app.swipeUp(velocity: .fast)
        app.swipeUp(velocity: .fast)

        // Scroll up
        app.swipeDown(velocity: .fast)
        app.swipeDown(velocity: .fast)
        app.swipeDown(velocity: .fast)
    }

    func test_going_through_search_screen() {
        // Launch App
        app.launch()

        // Tap to Search screen
        app.tabBars.buttons.element(boundBy: 2).tap()

        // Make sure we are displaying the Search screen
        XCTAssertTrue(app.isDisplayingSearchScreen)

        sleep(3)

        // Make sure the spinner is disappeared
        XCTAssertFalse(app.activityIndicators.element.exists)
        // Make sure the cells exist
        XCTAssertTrue(app.cells.element.exists)

        // Start testing the search function
        // Tap the search bar
        app.searchFields.element.tap()

        // Type the query
        app.searchFields.element.typeText("Wonder woman")

        // Hit the search button
        app.keyboards.buttons["search"].tap()

        // Wait for data fetching
        sleep(3)

        // Make sure the spinner is disappeared
        XCTAssertFalse(app.activityIndicators.element.exists)
        // Make sure the cells exist
        XCTAssertTrue(app.cells.element.exists)
        // Make sure the results contain the query
        XCTAssertTrue(app.staticTexts["Wonder Woman"].exists)
    }

    func test_going_through_person_detail_screen() {
        // Launch App
        app.launch()

        // Tap to Search screen
        app.tabBars.buttons.element(boundBy: 2).tap()

        // Make sure we are displaying the Search screen
        XCTAssertTrue(app.isDisplayingSearchScreen)

        sleep(3)

        // Make sure the spinner is disappeared
        XCTAssertFalse(app.activityIndicators.element.exists)
        // Make sure the cells exist
        XCTAssertTrue(app.cells.element.exists)

        // Start testing the search function
        // Tap the search bar
        app.searchFields.element.tap()

        // Type the query
        app.searchFields.element.typeText("Yen")

        // Hit the search button
        app.keyboards.buttons["search"].tap()

        // Wait for data fetching
        sleep(3)

        // Make sure the spinner is disappeared
        XCTAssertFalse(app.activityIndicators.element.exists)
        // Make sure the cells exist
        XCTAssertTrue(app.cells.element.exists)
        // Make sure the results contain the query
        XCTAssertTrue(app.staticTexts["Donnie Yen"].exists)

        // Scroll down
        app.swipeUp(velocity: .fast)

        // Tap cell
        app.cells.element(boundBy: 3).tap()

        sleep(3)

        // Make sure the results contain the query
        XCTAssertTrue(app.staticTexts["Donnie Yen"].exists)
        // Make sure the cells exist
        XCTAssertTrue(app.cells.element.exists)
    }

    func test_going_through_search_detail_list_screen() {
        // Launch App
        app.launch()

        // Tap to Search screen
        app.tabBars.buttons.element(boundBy: 2).tap()

        // Make sure we are displaying the Search screen
        XCTAssertTrue(app.isDisplayingSearchScreen)

        sleep(3)

        // Make sure the spinner is disappeared
        XCTAssertFalse(app.activityIndicators.element.exists)
        // Make sure the cells exist
        XCTAssertTrue(app.cells.element.exists)

        // Start testing the search function
        // Tap the search bar
        app.searchFields.element.tap()

        // Type the query
        app.searchFields.element.typeText("Bad boys")

        // Hit the search button
        app.keyboards.buttons["search"].tap()

        // Wait for data fetching
        sleep(3)

        // Make sure the spinner is disappeared
        XCTAssertFalse(app.activityIndicators.element.exists)
        // Make sure the cells exist
        XCTAssertTrue(app.cells.element.exists)
        // Make sure the results contain the query
        XCTAssertTrue(app.staticTexts["Bad Boys"].exists)

        // Tap the See All button
        app.buttons["See All"].firstMatch.tap()

        // Make sure the cells exist
        XCTAssertTrue(app.cells.element.exists)

        sleep(3)

        // Scroll down
        app.swipeUp(velocity: .fast)
        sleep(1)
        app.swipeUp(velocity: .fast)
        sleep(1)
        app.swipeUp(velocity: .fast)
        sleep(1)

        // Scroll up
        app.swipeDown(velocity: .fast)
        app.swipeDown(velocity: .fast)
        app.swipeDown(velocity: .fast)
    }

    func test_going_through_detail_screen() {
        test_going_through_home_screen()

        // Tap the first cell
        app.cells.element(boundBy: 2).tap()

        XCTAssertTrue(app.isDisplayingDetailScreen)

        // Wait for the network to load data
        sleep(3)

        // Scroll down
        app.swipeUp(velocity: .fast)
        app.swipeUp(velocity: .fast)

        XCTAssertTrue(app.staticTexts["About"].exists)
        XCTAssertTrue(app.staticTexts["Information"].exists)
    }

    func test_going_through_overview_screen() {
        test_going_through_detail_screen()

        // Scroll up
        app.swipeDown(velocity: .fast)

        // Make sure the overview keyword exists so that we know we are in the Detail screen
        XCTAssertTrue(app.staticTexts["OVERVIEW"].exists)

        // Tap the overview cell
        app.cells.element(boundBy: .zero).tap()

        XCTAssertTrue(app.isDisplayingOverviewScreen)
    }

    func test_going_through_related_detail_list_screen() {
        test_going_through_detail_screen()

        // Scroll up
        app.swipeDown(velocity: .fast)

        // Make sure the related keyword exists so that we know we are in the Detail screen
        XCTAssertTrue(app.staticTexts["Related"].exists)

        // Tap the related first cell
        app.cells.element(boundBy: 1).tap()

        // Wait for the network to load data
        sleep(3)

        // Scroll down
        app.swipeUp(velocity: .fast)
        app.swipeUp(velocity: .fast)

        XCTAssertTrue(app.staticTexts["About"].exists)
        XCTAssertTrue(app.staticTexts["Information"].exists)
    }

    func test_going_through_season_screen() {
        // Launch App
        app.launch()

        // Tap to Search screen
        app.tabBars.buttons.element(boundBy: 2).tap()

        // Make sure we are displaying the Search screen
        XCTAssertTrue(app.isDisplayingSearchScreen)

        sleep(3)

        // Make sure the spinner is disappeared
        XCTAssertFalse(app.activityIndicators.element.exists)
        // Make sure the cells exist
        XCTAssertTrue(app.cells.element.exists)

        // Tap the fourth cell
        app.cells.element(boundBy: 3).tap()

        // Make sure we are displaying the Detail screen
        XCTAssertTrue(app.isDisplayingDetailScreen)

        // Scroll down
        app.swipeUp(velocity: .slow)

        // Make sure the season word exists
        XCTAssertTrue(app.staticTexts["Season"].exists)

        // Tap the first season cell
        app.cells.element(boundBy: 1).tap()

        // Make sure we are displaying the Season screen
        XCTAssertTrue(app.isDisplayingSeasonDetailScreen)
    }

    func test_going_through_episode_screen() {
        test_going_through_season_screen()

        // Wait for network to fetch all the data
        sleep(3)

        // Scroll down
        app.swipeUp(velocity: .slow)
        app.swipeUp(velocity: .slow)

        // Scroll up
        app.swipeDown(velocity: .slow)
        app.swipeDown(velocity: .slow)

        // Tap the first episode cell
        app.cells.element(boundBy: .zero).tap()

        // Make sure we are displaying the Episode Detail screen
        XCTAssertTrue(app.isDisplayingEpisodeDetailScreen)
    }

    func test_going_through_favorites_screen() {
        test_going_through_detail_screen()

        // Make sure the navigation right bar button icon is there
        XCTAssertTrue(app.navigationBars.buttons["detailRightBarButtonItem"].exists)
        // Make sure the heart icon is there
        XCTAssertFalse(app.navigationBars.buttons["detailRightBarButtonItem"].images["detailRightBarButtonImage"].exists)

        // Tap the favorite button
        app.navigationBars.buttons["detailRightBarButtonItem"].tap()

        // Make sure the favorite button is enabled
        XCTAssertTrue(app.navigationBars.buttons["detailRightBarButtonItem"].isEnabled)

        guard let badgeValue = app.tabBars.buttons.element(boundBy: 1).value as? String else {
            XCTFail("Get badge value failed")
            return
        }

        XCTAssertEqual(badgeValue, "1 item")

        // Tap the Favorites screen
        app.tabBars.buttons.element(boundBy: 1).tap()

        // Make sure we are displaying the Favorites screen
        XCTAssertTrue(app.isDisplayingFavoritesScreen)

        // Make sure the cells exist
        XCTAssertTrue(app.cells.element.exists)

        // Make sure Wonder woman exists
        XCTAssertTrue(app.staticTexts["Wonder Woman 1984"].exists)
    }

    override func tearDown() {
        app = nil
        super.tearDown()
    }
}

extension XCUIApplication {
    var isDisplayingHomeScreen: Bool {
        return otherElements["homeView"].exists
    }

    var isDisplayingFavoritesScreen: Bool {
        return otherElements["favoritesView"].exists
    }

    var isDisplayingSearchScreen: Bool {
        return otherElements["searchView"].exists
    }

    var isDisplayingDetailListScreen: Bool {
        return otherElements["detailListView"].exists
    }

    var isDisplayingDetailScreen: Bool {
        return otherElements["detailView"].exists
    }

    var isDisplayingPersonDetailScreen: Bool {
        return otherElements["personDetailView"].exists
    }

    var isDisplayingOverviewScreen: Bool {
        return otherElements["overviewView"].exists
    }

    var isDisplayingSeasonDetailScreen: Bool {
        return otherElements["seasonDetailView"].exists
    }

    var isDisplayingEpisodeDetailScreen: Bool {
        return otherElements["episodeDetailView"].exists
    }
}
