//
//  MovieMockURLSessionTests.swift
//  MovieMockURLSessionTests
//
//  Created by Alwyn Yeo on 1/15/21.
//

import XCTest
@testable import Movie

class MovieMockURLSessionTests: XCTestCase {
    private var networking: Networking?
    private var testBundle: Bundle?
    private var decoder: JSONDecoder?
    private let successCode = 200

    override func setUp() {
        super.setUp()
        testBundle = Bundle(for: type(of: self))
        decoder = JSONDecoder()
    }

    // This is to test the top rated movie api, to check if it returns the expected results.
    func test_top_rated_movies_api_results() {
        // Given
        let expectation = self.expectation(description: "Top rated movie api test runs successfully")
        let path = testBundle?.path(forResource: "MovieTopRatedResults", ofType: "json")
        let expectedData = try? Data(contentsOf: URL(fileURLWithPath: path ?? ""), options: .alwaysMapped)
        var returnData: Data?
        var statusCode: Int?
        var decodedItems: [MediaItem]?

        let urlString = "https://api.themoviedb.org/3/movie/top_rated?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1"
        guard let url = URL(string: urlString) else {
            return
        }
        let urlResponse = HTTPURLResponse(url: url, statusCode: successCode, httpVersion: nil, headerFields: nil)

        let session = URLSessionMock(data: expectedData, response: urlResponse, error: nil)
        networking = Networking()
        networking?.session = session

        // When
        XCTAssertNotNil(expectedData, "Expected data should not be empty at the initial state")
        XCTAssertNil(returnData, "Return data should be empty at the initial state")
        XCTAssertNil(decodedItems, "Decoded items should be empty at the initial state")

        let dataTask = networking?.session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard error == nil else {
                XCTAssertThrowsError(error)
                return
            }
            statusCode = (response as? HTTPURLResponse)?.statusCode
            if statusCode == self?.successCode {
                returnData = data
                guard let returnData = returnData else {
                    return
                }
                let mediaResult = try? JSONDecoder().decode(MediaResult.self, from: returnData)
                guard let results = mediaResult?.results else {
                    return
                }
                decodedItems = results
            } else {
                XCTAssertNotEqual(statusCode, self?.successCode, "The status code is not 200")
                XCTAssertThrowsError(error)
            }
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertEqual(urlString, "https://api.themoviedb.org/3/movie/top_rated?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1", "The url strings should be the same")
        XCTAssertEqual(url, session.expectedUrl, "The url and the expected url should be the same")
        XCTAssertEqual(statusCode, self.successCode, "The status code should be 200")
        XCTAssertNotNil(returnData, "The return data should not be empty")
        XCTAssertEqual(returnData, expectedData, "The Return data and the expected data should be the same")
        XCTAssertEqual(decodedItems?.count, 20, "The decoded items should be 20")
    }

    func test_now_playing_movies_api_results() {
        // Given
        let expectation = self.expectation(description: "Now playing movie api test runs successfully")
        let path = testBundle?.path(forResource: "MovieNowPlayingResults", ofType: "json")
        let expectedData = try? Data(contentsOf: URL(fileURLWithPath: path ?? ""), options: .alwaysMapped)
        var returnData: Data?
        var statusCode: Int?
        var decodedItems: [MediaItem]?

        let urlString = "https://api.themoviedb.org/3/movie/now_playing?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1"
        guard let url = URL(string: urlString) else {
            return
        }
        let urlResponse = HTTPURLResponse(url: url, statusCode: successCode, httpVersion: nil, headerFields: nil)

        let session = URLSessionMock(data: expectedData, response: urlResponse, error: nil)
        networking = Networking()
        networking?.session = session

        // When
        XCTAssertNotNil(expectedData, "Expected data should not be empty at the initial state")
        XCTAssertNil(returnData, "Return data should be empty at the initial state")
        XCTAssertNil(decodedItems, "Decoded items should be empty at the initial state")

        let dataTask = networking?.session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard error == nil else {
                XCTAssertThrowsError(error)
                return
            }
            statusCode = (response as? HTTPURLResponse)?.statusCode
            if statusCode == self?.successCode {
                returnData = data
                guard let returnData = returnData else {
                    return
                }
                let mediaResult = try? JSONDecoder().decode(MediaResult.self, from: returnData)
                guard let results = mediaResult?.results else {
                    return
                }
                decodedItems = results
            } else {
                XCTAssertNotEqual(statusCode, self?.successCode, "The status code is not 200")
                XCTAssertThrowsError(error)
            }
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertEqual(urlString, "https://api.themoviedb.org/3/movie/now_playing?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1", "The url strings should be the same")
        XCTAssertEqual(url, session.expectedUrl, "The url and the expected url should be the same")
        XCTAssertEqual(statusCode, self.successCode, "The status code should be 200")
        XCTAssertNotNil(returnData, "The return data should not be empty")
        XCTAssertEqual(returnData, expectedData, "The Return data and the expected data should be the same")
        XCTAssertEqual(decodedItems?.count, 20, "The decoded items should be 20")
    }

    func test_popular_movies_api_results() {
        // Given
        let expectation = self.expectation(description: "Popular movie api test runs successfully")
        let path = testBundle?.path(forResource: "MoviePopularResults", ofType: "json")
        let expectedData = try? Data(contentsOf: URL(fileURLWithPath: path ?? ""), options: .alwaysMapped)
        var returnData: Data?
        var statusCode: Int?
        var decodedItems: [MediaItem]?

        let urlString = "https://api.themoviedb.org/3/movie/popular?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1"
        guard let url = URL(string: urlString) else {
            return
        }
        let urlResponse = HTTPURLResponse(url: url, statusCode: successCode, httpVersion: nil, headerFields: nil)

        let session = URLSessionMock(data: expectedData, response: urlResponse, error: nil)
        networking = Networking()
        networking?.session = session

        // When
        XCTAssertNotNil(expectedData, "Expected data should not be empty at the initial state")
        XCTAssertNil(returnData, "Return data should be empty at the initial state")
        XCTAssertNil(decodedItems, "Decoded items should be empty at the initial state")

        let dataTask = networking?.session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard error == nil else {
                XCTAssertThrowsError(error)
                return
            }
            statusCode = (response as? HTTPURLResponse)?.statusCode
            if statusCode == self?.successCode {
                returnData = data
                guard let returnData = returnData else {
                    return
                }
                let mediaResult = try? JSONDecoder().decode(MediaResult.self, from: returnData)
                guard let results = mediaResult?.results else {
                    return
                }
                decodedItems = results
            } else {
                XCTAssertNotEqual(statusCode, self?.successCode, "The status code is not 200")
                XCTAssertThrowsError(error)
            }
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertEqual(urlString, "https://api.themoviedb.org/3/movie/popular?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1", "The url strings should be the same")
        XCTAssertEqual(url, session.expectedUrl, "The url and the expected url should be the same")
        XCTAssertEqual(statusCode, self.successCode, "The status code should be 200")
        XCTAssertNotNil(returnData, "The return data should not be empty")
        XCTAssertEqual(returnData, expectedData, "The Return data and the expected data should be the same")
        XCTAssertEqual(decodedItems?.count, 20, "The decoded items should be 20")
    }

    func test_upcoming_movies_api_results() {
        // Given
        let expectation = self.expectation(description: "Upcoming movie api test runs successfully")
        let path = testBundle?.path(forResource: "MovieUpcomingResults", ofType: "json")
        let expectedData = try? Data(contentsOf: URL(fileURLWithPath: path ?? ""), options: .alwaysMapped)
        var returnData: Data?
        var statusCode: Int?
        var decodedItems: [MediaItem]?

        let urlString = "https://api.themoviedb.org/3/movie/upcoming?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1"
        guard let url = URL(string: urlString) else {
            return
        }
        let urlResponse = HTTPURLResponse(url: url, statusCode: successCode, httpVersion: nil, headerFields: nil)

        let session = URLSessionMock(data: expectedData, response: urlResponse, error: nil)
        networking = Networking()
        networking?.session = session

        // When
        XCTAssertNotNil(expectedData, "Expected data should not be empty at the initial state")
        XCTAssertNil(returnData, "Return data should be empty at the initial state")
        XCTAssertNil(decodedItems, "Decoded items should be empty at the initial state")

        let dataTask = networking?.session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard error == nil else {
                XCTAssertThrowsError(error)
                return
            }
            statusCode = (response as? HTTPURLResponse)?.statusCode
            if statusCode == self?.successCode {
                returnData = data
                guard let returnData = returnData else {
                    return
                }
                let mediaResult = try? JSONDecoder().decode(MediaResult.self, from: returnData)
                guard let results = mediaResult?.results else {
                    return
                }
                decodedItems = results
            } else {
                XCTAssertNotEqual(statusCode, self?.successCode, "The status code is not 200")
                XCTAssertThrowsError(error)
            }
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertEqual(urlString, "https://api.themoviedb.org/3/movie/upcoming?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1", "The url strings should be the same")
        XCTAssertEqual(url, session.expectedUrl, "The url and the expected url should be the same")
        XCTAssertEqual(statusCode, self.successCode, "The status code should be 200")
        XCTAssertNotNil(returnData, "The return data should not be empty")
        XCTAssertEqual(returnData, expectedData, "The Return data and the expected data should be the same")
        XCTAssertEqual(decodedItems?.count, 20, "The decoded items should be 20")
    }

    func test_now_playing_tv_shows_api_results() {
        // Given
        let expectation = self.expectation(description: "Now playing tv shows api test runs successfully")
        let path = testBundle?.path(forResource: "TvShowNowPlayingResults", ofType: "json")
        let expectedData = try? Data(contentsOf: URL(fileURLWithPath: path ?? ""), options: .alwaysMapped)
        var returnData: Data?
        var statusCode: Int?
        var decodedItems: [MediaItem]?

        let urlString = "https://api.themoviedb.org/3/tv/airing_today?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1"
        guard let url = URL(string: urlString) else {
            return
        }
        let urlResponse = HTTPURLResponse(url: url, statusCode: successCode, httpVersion: nil, headerFields: nil)

        let session = URLSessionMock(data: expectedData, response: urlResponse, error: nil)
        networking = Networking()
        networking?.session = session

        // When
        XCTAssertNotNil(expectedData, "Expected data should not be empty at the initial state")
        XCTAssertNil(returnData, "Return data should be empty at the initial state")
        XCTAssertNil(decodedItems, "Decoded items should be empty at the initial state")

        let dataTask = networking?.session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard error == nil else {
                XCTAssertThrowsError(error)
                return
            }
            statusCode = (response as? HTTPURLResponse)?.statusCode
            if statusCode == self?.successCode {
                returnData = data
                guard let returnData = returnData else {
                    return
                }
                let mediaResult = try? JSONDecoder().decode(MediaResult.self, from: returnData)
                guard let results = mediaResult?.results else {
                    return
                }
                decodedItems = results
            } else {
                XCTAssertNotEqual(statusCode, self?.successCode, "The status code is not 200")
                XCTAssertThrowsError(error)
            }
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertEqual(urlString, "https://api.themoviedb.org/3/tv/airing_today?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1", "The url strings should be the same")
        XCTAssertEqual(url, session.expectedUrl, "The url and the expected url should be the same")
        XCTAssertEqual(statusCode, self.successCode, "The status code should be 200")
        XCTAssertNotNil(returnData, "The return data should not be empty")
        XCTAssertEqual(returnData, expectedData, "The Return data and the expected data should be the same")
        XCTAssertEqual(decodedItems?.count, 20, "The decoded items should be 20")
    }

    func test_popular_tv_shows_api_results() {
        // Given
        let expectation = self.expectation(description: "Popular tv shows api test runs successfully")
        let path = testBundle?.path(forResource: "TvShowPopularResults", ofType: "json")
        let expectedData = try? Data(contentsOf: URL(fileURLWithPath: path ?? ""), options: .alwaysMapped)
        var returnData: Data?
        var statusCode: Int?
        var decodedItems: [MediaItem]?

        let urlString = "https://api.themoviedb.org/3/tv/popular?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1"
        guard let url = URL(string: urlString) else {
            return
        }
        let urlResponse = HTTPURLResponse(url: url, statusCode: successCode, httpVersion: nil, headerFields: nil)

        let session = URLSessionMock(data: expectedData, response: urlResponse, error: nil)
        networking = Networking()
        networking?.session = session

        // When
        XCTAssertNotNil(expectedData, "Expected data should not be empty at the initial state")
        XCTAssertNil(returnData, "Return data should be empty at the initial state")
        XCTAssertNil(decodedItems, "Decoded items should be empty at the initial state")

        let dataTask = networking?.session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard error == nil else {
                XCTAssertThrowsError(error)
                return
            }
            statusCode = (response as? HTTPURLResponse)?.statusCode
            if statusCode == self?.successCode {
                returnData = data
                guard let returnData = returnData else {
                    return
                }
                let mediaResult = try? JSONDecoder().decode(MediaResult.self, from: returnData)
                guard let results = mediaResult?.results else {
                    return
                }
                decodedItems = results
            } else {
                XCTAssertNotEqual(statusCode, self?.successCode, "The status code is not 200")
                XCTAssertThrowsError(error)
            }
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertEqual(urlString, "https://api.themoviedb.org/3/tv/popular?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1", "The url strings should be the same")
        XCTAssertEqual(url, session.expectedUrl, "The url and the expected url should be the same")
        XCTAssertEqual(statusCode, self.successCode, "The status code should be 200")
        XCTAssertNotNil(returnData, "The return data should not be empty")
        XCTAssertEqual(returnData, expectedData, "The Return data and the expected data should be the same")
        XCTAssertEqual(decodedItems?.count, 20, "The decoded items should be 20")
    }

    func test_top_rated_tv_shows_api_results() {
        // Given
        let expectation = self.expectation(description: "Top rated tv shows api test runs successfully")
        let path = testBundle?.path(forResource: "TvShowTopRatedResults", ofType: "json")
        let expectedData = try? Data(contentsOf: URL(fileURLWithPath: path ?? ""), options: .alwaysMapped)
        var returnData: Data?
        var statusCode: Int?
        var decodedItems: [MediaItem]?

        let urlString = "https://api.themoviedb.org/3/tv/top_rated?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1"
        guard let url = URL(string: urlString) else {
            return
        }
        let urlResponse = HTTPURLResponse(url: url, statusCode: successCode, httpVersion: nil, headerFields: nil)

        let session = URLSessionMock(data: expectedData, response: urlResponse, error: nil)
        networking = Networking()
        networking?.session = session

        // When
        XCTAssertNotNil(expectedData, "Expected data should not be empty at the initial state")
        XCTAssertNil(returnData, "Return data should be empty at the initial state")
        XCTAssertNil(decodedItems, "Decoded items should be empty at the initial state")

        let dataTask = networking?.session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard error == nil else {
                XCTAssertThrowsError(error)
                return
            }
            statusCode = (response as? HTTPURLResponse)?.statusCode
            if statusCode == self?.successCode {
                returnData = data
                guard let returnData = returnData else {
                    return
                }
                let mediaResult = try? JSONDecoder().decode(MediaResult.self, from: returnData)
                guard let results = mediaResult?.results else {
                    return
                }
                decodedItems = results
            } else {
                XCTAssertNotEqual(statusCode, self?.successCode, "The status code is not 200")
                XCTAssertThrowsError(error)
            }
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertEqual(urlString, "https://api.themoviedb.org/3/tv/top_rated?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1", "The url strings should be the same")
        XCTAssertEqual(url, session.expectedUrl, "The url and the expected url should be the same")
        XCTAssertEqual(statusCode, self.successCode, "The status code should be 200")
        XCTAssertNotNil(returnData, "The return data should not be empty")
        XCTAssertEqual(returnData, expectedData, "The Return data and the expected data should be the same")
        XCTAssertEqual(decodedItems?.count, 20, "The decoded items should be 20")
    }

    func test_search_multiple_api_results() {
        // Given
        let expectation = self.expectation(description: "Search multi api test runs successfully")
        let path = testBundle?.path(forResource: "SearchMultiResults", ofType: "json")
        let expectedData = try? Data(contentsOf: URL(fileURLWithPath: path ?? ""), options: .alwaysMapped)
        var returnData: Data?
        var statusCode: Int?
        var decodedItems: [MediaItem]?

        let urlString = "https://api.themoviedb.org/3/search/multi?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&query=wonder%20woman&page=1&include_adult=false"
        guard let url = URL(string: urlString) else {
            return
        }
        let urlResponse = HTTPURLResponse(url: url, statusCode: successCode, httpVersion: nil, headerFields: nil)

        let session = URLSessionMock(data: expectedData, response: urlResponse, error: nil)
        networking = Networking()
        networking?.session = session

        // When
        XCTAssertNotNil(expectedData, "Expected data should not be empty at the initial state")
        XCTAssertNil(returnData, "Return data should be empty at the initial state")
        XCTAssertNil(decodedItems, "Decoded items should be empty at the initial state")

        let dataTask = networking?.session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard error == nil else {
                XCTAssertThrowsError(error)
                return
            }
            statusCode = (response as? HTTPURLResponse)?.statusCode
            if statusCode == self?.successCode {
                returnData = data
                guard let returnData = returnData else {
                    return
                }
                let mediaResult = try? JSONDecoder().decode(MediaResult.self, from: returnData)
                guard let results = mediaResult?.results else {
                    return
                }
                decodedItems = results
            } else {
                XCTAssertNotEqual(statusCode, self?.successCode, "The status code is not 200")
                XCTAssertThrowsError(error)
            }
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertEqual(urlString, "https://api.themoviedb.org/3/search/multi?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&query=wonder%20woman&page=1&include_adult=false", "The url strings should be the same")
        XCTAssertEqual(url, session.expectedUrl, "The url and the expected url should be the same")
        XCTAssertEqual(statusCode, self.successCode, "The status code should be 200")
        XCTAssertNotNil(returnData, "The return data should not be empty")
        XCTAssertEqual(returnData, expectedData, "The Return data and the expected data should be the same")
        XCTAssertEqual(decodedItems?.count, 16, "The decoded items should be 16")
    }

    override func tearDown() {
        networking = nil
        testBundle = nil
        decoder = nil
        super.tearDown()
    }
}
