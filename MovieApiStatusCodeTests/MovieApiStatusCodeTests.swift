//
//  MovieApiStatusCodeTests.swift
//  MovieApiStatusCodeTests
//
//  Created by Alwyn Yeo on 1/15/21.
//

import XCTest
@testable import Movie

class MovieApiStatusCodeTests: XCTestCase {
    private var networkManager: URLSession?
    private let successCode = 200

    override func setUp() {
        super.setUp()
        networkManager = URLSession(configuration: .default)
    }

    func test_top_rated_movies_api_status_code_200() {
        // Given
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/top_rated?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1") else {
            return XCTFail("Error in MovieSlotTests at line \(#line)")
        }
        let expectation = self.expectation(description: "Status code and error are passed")
        var statusCode: Int?
        var responseError: Error?

        // When
        let dataTask = networkManager?.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard let _ = self else {
                return
            }
            guard error == nil else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            guard let response = response as? HTTPURLResponse else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            statusCode = response.statusCode
            responseError = error
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, successCode)
    }

    func test_now_playing_movies_api_status_code_200() {
        // Given
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/now_playing?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1") else {
            return
        }
        let expectation = self.expectation(description: "Status code and error are passed")
        var statusCode: Int?
        var responseError: Error?

        // When
        let dataTask = networkManager?.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard let _ = self else {
                return
            }
            guard error == nil else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            guard let response = response as? HTTPURLResponse else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            statusCode = response.statusCode
            responseError = error
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }

    func test_popular_movies_api_status_code_200() {
        // Given
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/popular?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1") else {
            return
        }
        let expectation = self.expectation(description: "Status code and error are passed")
        var statusCode: Int?
        var responseError: Error?

        // When
        let dataTask = networkManager?.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard let _ = self else {
                return
            }
            guard error == nil else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            guard let response = response as? HTTPURLResponse else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            statusCode = response.statusCode
            responseError = error
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }

    func test_upcoming_movies_api_status_code_200() {
        // Given
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/upcoming?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1") else {
            return
        }
        let expectation = self.expectation(description: "Status code and error are passed")
        var statusCode: Int?
        var responseError: Error?

        // When
        let dataTask = networkManager?.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard let _ = self else {
                return
            }
            guard error == nil else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            guard let response = response as? HTTPURLResponse else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            statusCode = response.statusCode
            responseError = error
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }

    func test_top_rated_tv_shows_api_status_code_200() {
        // Given
        guard let url = URL(string: "https://api.themoviedb.org/3/tv/top_rated?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1") else {
            return
        }
        let expectation = self.expectation(description: "Status code and error are passed")
        var statusCode: Int?
        var responseError: Error?

        // When
        let dataTask = networkManager?.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard let _ = self else {
                return
            }
            guard error == nil else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            guard let response = response as? HTTPURLResponse else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            statusCode = response.statusCode
            responseError = error
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }

    func test_now_playing_tv_shows_api_status_code_200() {
        // Given
        guard let url = URL(string: "https://api.themoviedb.org/3/tv/airing_today?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1") else {
            return
        }
        let expectation = self.expectation(description: "Status code and error are passed")
        var statusCode: Int?
        var responseError: Error?

        // When
        let dataTask = networkManager?.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard let _ = self else {
                return
            }
            guard error == nil else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            guard let response = response as? HTTPURLResponse else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            statusCode = response.statusCode
            responseError = error
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }

    func test_popular_tv_shows_api_status_code_200() {
        // Given
        guard let url = URL(string: "https://api.themoviedb.org/3/tv/popular?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&page=1") else {
            return
        }
        let expectation = self.expectation(description: "Status code and error are passed")
        var statusCode: Int?
        var responseError: Error?

        // When
        let dataTask = networkManager?.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard let _ = self else {
                return
            }
            guard error == nil else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            guard let response = response as? HTTPURLResponse else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            statusCode = response.statusCode
            responseError = error
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }

    func test_search_multiple_api_status_code_200() {
        // Given
        guard let url = URL(string: "https://api.themoviedb.org/3/search/multi?api_key=47bbfb24763a46c352ef750877c887a6&language=en-US&query=wonder%20woman&page=1&include_adult=false") else {
            return
        }
        let expectation = self.expectation(description: "Status code and error are passed")
        var statusCode: Int?
        var responseError: Error?

        // When
        let dataTask = networkManager?.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard let _ = self else {
                return
            }
            guard error == nil else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            guard let response = response as? HTTPURLResponse else {
                return XCTFail("Error: \(String(describing: error?.localizedDescription)) at line \(#line)")
            }
            statusCode = response.statusCode
            responseError = error
            expectation.fulfill()
        })
        dataTask?.resume()
        wait(for: [expectation], timeout: 5)

        // Then
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }

    override func tearDown() {
        networkManager = nil
        super.tearDown()
    }
}
