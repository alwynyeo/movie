//
//  MovieUserDefaultsTests.swift
//  MovieUserDefaultsTests
//
//  Created by Alwyn Yeo on 1/15/21.
//

import XCTest
@testable import Movie

class MovieUserDefaultsTests: XCTestCase {
    private var defaults: MockUserDefaults?

    override func setUp() {
        super.setUp()
        defaults = MockUserDefaults(suiteName: "testing")
    }

    func test_add_item_to_user_defaults() {
        // Given
        // When
        XCTAssertEqual(defaults?.isFavorited, false, "The isFavorited property should be false at the initial state")
        defaults?.set(true, forKey: "testing")
        // Then
        XCTAssertEqual(defaults?.isFavorited, true, "The isFavorited property should be true")
    }

    func test_remove_item_from_user_defaults() {
        // Given
        test_add_item_to_user_defaults()
        // When
        XCTAssertEqual(defaults?.isFavorited, true, "The isFavorited property should be true at the initial state")
        defaults?.set(false, forKey: "testing")
        // Then
        XCTAssertEqual(defaults?.isFavorited, false, "The isFavorited property should be false")
    }

    override func tearDown() {
        defaults = nil
        super.tearDown()
    }
}
