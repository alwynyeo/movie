# Movie App

A personal project that provides the latest movies and tv shows.

### Technologies Used:
* Swift
* UIKit
* UserDefaults
* Swift Package Manager
* SDWebImage
* ContextMenu
* Compositional layout
* Collection view with Diffable Data Source
* Auto Layout (Programmatic)

### Features:
* Get the latest movies and tv shows.
* Search for movies, tv shows and casts
* Save movies and tv shows to Favorites
* Share movies and tv shows through Social media.
* Supports Dark mode

### Images:
![](Images/light-home-movie.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![](Images/dark-home-tv.png)

![](Images/light-detail-list.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![](Images/dark-detail-list.png)

![](Images/light-search.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![](Images/dark-search-query.png)

![](Images/dark-detail.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![](Images/dark-detail-lower.png)

![](Images/dark-overview.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![](Images/dark-favorites.png)

![](Images/light-cast.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![](Images/dark-cast-list.png)


