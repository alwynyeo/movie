//
//  SearchViewController.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/25/20.
//

import SwiftUI
import Network
import LinkPresentation

fileprivate typealias SearchHeaderRegistration = UICollectionView.SupplementaryRegistration<SearchHeader>
fileprivate typealias SearchCellRegistration = UICollectionView.CellRegistration<SearchCell, MediaItem>
fileprivate typealias SearchDiffableDataSource = UICollectionViewDiffableDataSource<SearchSection, MediaItem>
fileprivate typealias Snapshot = NSDiffableDataSourceSnapshot<SearchSection, MediaItem>

enum SearchSection: String {
    case trendingMovies = "Trending Movies"
    case trendingtvShows = "Trending TV Shows"
    case movies = "Movies"
    case tvShows = "TV Shows"
    case persons = "Cast & Crew"
}

extension SearchSection: CaseIterable {}

// MARK: - Class
final class SearchViewController: UICollectionViewController {
    
    // MARK: - Declarations (Properties)
    private var headerRegistration: SearchHeaderRegistration?
    private var cellRegistration: SearchCellRegistration?
    private var collectionViewDiffableDataSource: SearchDiffableDataSource?
    private var trendingMovies = [MediaItem]()
    private var trendingTVShows = [MediaItem]()
    private var movies = [MediaItem]()
    private var tvShows = [MediaItem]()
    private var persons = [MediaItem]()
    private var prefixedMovies = [MediaItem]()
    private var prefixedTvShows = [MediaItem]()
    private var prefixedPersons = [MediaItem]()
    private let group = DispatchGroup()
    private let main = DispatchQueue.main
    private var lastSearchedText = ""
    private var isTrending = false
    private var hasInitialDataBeenLoaded = false
    private let headerKind = UICollectionView.elementKindSectionHeader
    private let notificationCenter = NotificationCenter.default
    private lazy var interaction = UIContextMenuInteraction(delegate: self)
    private var sharingItem: MediaItem?
    private var sharingItemImageView = CustomImageView()
    private var isNetworkConnected = false
    private var isDataFailedFetching = false

    // MARK: - Declarations (UI)
    fileprivate lazy var searchController: UISearchController = {
        let controller = UISearchController(searchResultsController: nil)
        let searchBar = controller.searchBar
        let textfield = searchBar.searchTextField
        let constants = UIEdgeInsets(top: .zero, left: .zero, bottom: .zero, right: 8)
        let placeholder = "Movies, TV Shows, and More"
        controller.obscuresBackgroundDuringPresentation = false
        controller.hidesNavigationBarDuringPresentation = true
        searchBar.placeholder = placeholder
        searchBar.delegate = self
        textfield.addSubview(searchBarActivityIndicatorView)
        searchBarActivityIndicatorView.makeCenterY(to: searchBar)
        searchBarActivityIndicatorView.makeView(top: nil, leading: nil, trailing: textfield.trailingAnchor, bottom: nil, constants: constants)
        return controller
    }()

    fileprivate let searchBarActivityIndicatorView: CustomActivityIndicatorView = {
        let view = CustomActivityIndicatorView(style: .medium)
        view.stopAnimating()
        return view
    }()

    fileprivate lazy var emptyView = CustomEmptyView(frame: collectionView.frame)
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.tintColor = .systemBlue
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = nil
    }

    // MARK: - Initialize
    init() {
        super.init(collectionViewLayout: SearchLayout.searchLayout())
    }

    // MARK: - Configuration
    private func setupView() {
        view.accessibilityIdentifier = "searchView"
        configureNavigationBar()
        configureCollectionView()
        configureDiffableDataSource()
        networkChangesObserver()
        getTrendingResults()
    }

    private func configureNavigationBar() {
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
    }

    private func configureCollectionView() {
        collectionView.backgroundColor = .systemBackground
        collectionView.alwaysBounceVertical = false
        collectionView.keyboardDismissMode = .onDrag
        collectionView.backgroundView = emptyView
    }

    private func configureDiffableDataSource() {
        makeCellRegistration()
        makeHeaderRegistration()
        configureCell()
        configureHeader()
    }

    private func checkCurrentViewState() {
        guard let snapshot = collectionViewDiffableDataSource?.snapshot() else {
            return
        }

        // Check if the data is failed fetching
        if isDataFailedFetching {
            // Data fetching is failed
            // Check if the error is caused by the network
            if isNetworkConnected {
                // If not, then show something went wrong error message
                emptyView.configure(isLoading: false, message: "Something went wrong. Please try again later.")
            } else {
                // if yes, then show no network error
                emptyView.configure(isLoading: false, message: "You're not connected to the internet.")
            }
            collectionView.alwaysBounceVertical = false
        } else {
            // Data fetching is success
            // Check if there are data
            if snapshot.numberOfItems > .zero {
                // If there are, no message will be shown
                emptyView.configure(isLoading: false, message: nil)
                collectionView.alwaysBounceVertical = true
            } else {
                // If there are no data, show no results found message
                guard let text = searchController.searchBar.text else {
                    return
                }
                emptyView.configure(isLoading: false, message: "Sorry. There are no results found for \"\(text)\".")
                collectionView.alwaysBounceVertical = false
            }
        }
    }

    private func applySnapshot() {
        var snapshot = Snapshot()

        if !prefixedMovies.isEmpty {
            if isTrending {
                snapshot.appendSections([.trendingMovies]) // Append to the section
                snapshot.appendItems(prefixedMovies, toSection: .trendingMovies) // Append items
            } else {
                snapshot.appendSections([.movies]) // Append to the section
                snapshot.appendItems(prefixedMovies, toSection: .movies) // Append items
            }
        }

        if !prefixedTvShows.isEmpty {
            if isTrending {
                snapshot.appendSections([.trendingtvShows]) // Append to the section
                snapshot.appendItems(prefixedTvShows, toSection: .trendingtvShows) // Append items
            } else {
                snapshot.appendSections([.tvShows]) // Append to the section
                snapshot.appendItems(prefixedTvShows, toSection: .tvShows) // Append items
            }
        }

        if !prefixedPersons.isEmpty {
            snapshot.appendSections([.persons]) // Append to the section
            snapshot.appendItems(prefixedPersons, toSection: .persons) // Append items
        }

        collectionView.reloadData()
        collectionViewDiffableDataSource?.apply(snapshot) // Apply the snapshot
        checkCurrentViewState()
    }
    
    // MARK: - Methods
    private func getTrendingResults() {
        getTrendingMovies()
        getTrendingTvShows()
        setIsTrending(true)
        group.notify(queue: .main) {
            self.hasInitialDataBeenLoaded = true
            self.emptyView.configure(isLoading: false, message: nil)
            self.createSnapshot()
        }
    }

    private func getTrendingMovies() {
        group.enter()
        Networking.manager.getTrendings(mediaType: MediaType.movie.rawValue) { [weak self] (result) in
            switch result {
                case .success(let items):
                    print("Trending Movies: \(items.count)")
                    self?.trendingMovies.append(contentsOf: items)
                    self?.isDataFailedFetching = false
                case .failure(let error):
                    self?.failed(error: error)
            }
            self?.group.leave()
        }
    }

    private func getTrendingTvShows() {
        group.enter()
        Networking.manager.getTrendings(mediaType: MediaType.tv.rawValue) { [weak self] (result) in
            switch result {
                case .success(let items):
                    print("Trending Tv Shows: \(items.count)")
                    self?.trendingTVShows.append(contentsOf: items)
                    self?.isDataFailedFetching = false
                case .failure(let error):
                    self?.failed(error: error)
            }
            self?.group.leave()
        }
    }
    
    private func getSearchResults(searchText query: String) {
        group.enter()
        Networking.manager.getSearchResults(search: query) { [weak self] (result) in
            switch result {
                case .success(let items):
                    self?.configureMultipleResults(items: items)
                    self?.isDataFailedFetching = false
                case .failure(let error):
                    self?.failed(error: error)
            }
            self?.group.leave()
        }
        group.notify(queue: .main) {
            self.createSnapshot()
            self.isSearching(false)
        }
    }

    private func configureMultipleResults(items: [MediaItem]) {
        items.forEach { [weak self] (item) in
            switch item.media_type {
                case MediaType.movie.rawValue:
                    self?.movies.append(item)
                case MediaType.tv.rawValue:
                    self?.tvShows.append(item)
                case MediaType.person.rawValue:
                    self?.persons.append(item)
                default:
                    return
            }
        }
    }

    private func failed(error: Error) {
        isDataFailedFetching = true
        Errors.error(in: FileIndentifier.Search.rawValue, at: #line, error: error)
        main.async {
            self.emptyView.configure(isLoading: false, message: "Something went wrong. Please try again later.")
        }
    }

    private func createSnapshot() {
        if isTrending {
            if hasInitialDataBeenLoaded {
                prefixedMovies = Array(trendingMovies.prefix(3))
                prefixedTvShows = Array(trendingTVShows.prefix(3))
            }
            prefixedPersons.removeAll()
        } else {
            prefixedMovies = Array(movies.prefix(3))
            prefixedTvShows = Array(tvShows.prefix(3))
            prefixedPersons = Array(persons.prefix(3))
        }
        applySnapshot()
    }

    private func presentDetailListViewcontroller(section: SearchSection?) {
        let layout = UICollectionViewFlowLayout()
        let controller = DetailListViewController(collectionViewLayout: layout)
        guard let searchText = searchController.searchBar.text else {
            Errors.error(in: FileIndentifier.Search.rawValue, at: #line)
            return
        }
        guard let section = section else {
            Errors.error(in: FileIndentifier.Search.rawValue, at: #line)
            return
        }
        var items = [MediaItem]()
        var mediaType = ""
        switch section {
            case .trendingMovies:
                items = trendingMovies
                mediaType = MediaType.movie.rawValue
            case .trendingtvShows:
                items = trendingTVShows
                mediaType = MediaType.tv.rawValue
            case .movies:
                items = movies
                mediaType = MediaType.movie.rawValue
            case .tvShows:
                items = tvShows
                mediaType = MediaType.tv.rawValue
            case .persons:
                items = persons
                mediaType = MediaType.person.rawValue
        }
        if searchText.isEmpty {
            controller.configure(section: section.rawValue, items: items, searchText: nil, mediaType: mediaType)
        } else {
            controller.configure(section: section.rawValue, items: items, searchText: searchText, mediaType: mediaType)
        }
        navigationController?.pushViewController(controller, animated: true)
    }

    private func presentDetailViewController(item: MediaItem) {
        guard let snapshot = collectionViewDiffableDataSource?.snapshot() else {
            return
        }
        let section = snapshot.sectionIdentifier(containingItem: item)

        switch section {
            case .persons:
                let controller = PersonDetailViewController()
                controller.configure(item: item)
                navigationController?.pushViewController(controller, animated: true)
            default:
                guard let mediaType = item.media_type else {
                    return
                }
                let controller = DetailViewController(mediaType: mediaType)
                controller.configure(item: item)
                navigationController?.pushViewController(controller, animated: true)
        }
    }

    private func setIsTrending(_ trending: Bool) {
        isTrending = trending
    }

    private func isSearching(_ searching: Bool) {
        let textfield = searchController.searchBar.searchTextField
        if searching {
            textfield.clearButtonMode = . never
            searchBarActivityIndicatorView.startAnimating()
        } else {
            textfield.clearButtonMode = .always
            searchBarActivityIndicatorView.stopAnimating()
        }
    }

    private func isLastSearchedTextEmpty() -> Bool {
        if lastSearchedText.isEmpty {
            return true
        }
        return false
    }

    private func clearData() {
        if !movies.isEmpty {
            movies.removeAll()
        }

        if !tvShows.isEmpty {
            tvShows.removeAll()
        }

        if !persons.isEmpty {
            persons.removeAll()
        }
    }

    private func shareAction(item: MediaItem) -> UIAction {
        sharingItem = item
        let path = item.backdrop_path ?? item.poster_path
        sharingItemImageView.configure(path: path)
        let action = UIAction(title: "Share", image: UIImage(systemName: "square.and.arrow.up")) { [unowned self] (_) in
            let items: [Any] = [
                self
            ]
            let controller = UIActivityViewController(activityItems: items, applicationActivities: nil)
            controller.popoverPresentationController?.sourceView = view
            controller.excludedActivityTypes = [
                .addToReadingList,
                .assignToContact,
                .markupAsPDF,
                .openInIBooks,
                .print,
                .postToFlickr,
                .postToVimeo,
                .saveToCameraRoll,
            ]
            self.present(controller, animated: true)
        }
        return action
    }

    private func addOrDeleteAction(item: MediaItem) -> UIAction {
        let favoritedItems = defaults.favoritedItems()
        let isFavoritedItem = favoritedItems.contains(where: { $0.id == item.id })
        if isFavoritedItem {
            let action = UIContextMenuActions.deleteAction(item: item) { [weak self] in
                // Post a notification to notify FavoriteViewController the changes
                self?.postFavoriteItemChangesNotification()
            }
            return action
        } else {
            let action = UIContextMenuActions.addAction(item: item) { [weak self] in
                // Post a notification to notify FavoriteViewController the changes
                self?.postFavoriteItemChangesNotification()
            }
            return action
        }
    }

    private func createContextMenu(indexPath: IndexPath) -> UIMenu {
        guard let item = collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
            return UIMenu()
        }
        let menu = UIMenu(children: [
            UIContextMenuActions.copyAction(item: item),
            shareAction(item: item),
            addOrDeleteAction(item: item),
        ])
        return menu
    }

    private func postFavoriteItemChangesNotification() {
        notificationCenter.post(name: favoritedItemChangesNotification, object: nil)
        let newFavoriteCount = defaults.newFavoriteItemCount()
        if newFavoriteCount == .zero {
            mainTabBarController?.viewControllers?[1].tabBarItem.badgeValue = nil
        } else {
            mainTabBarController?.viewControllers?[1].tabBarItem.badgeValue = "\(newFavoriteCount)"
        }
    }

    private func networkChangesObserver() {
        let monitor = NWPathMonitor()
        monitor.startMonitoringNetworkChanges { [weak self] (status) in
            switch status {
                case .satisfied:
                    self?.isNetworkConnected = true
                    self?.isNetworkSatisfied(true)
                case .unsatisfied:
                    self?.isNetworkConnected = false
                    self?.isNetworkSatisfied(false)
                case .requiresConnection:
                    print("Requires Connection")
                default:
                    return
            }
        }
    }

    private func isNetworkSatisfied(_ satisfied: Bool) {
        main.async {
            if !self.hasInitialDataBeenLoaded {
                if satisfied {
                    self.emptyView.configure(isLoading: true, message: nil)
                } else {
                    self.emptyView.configure(isLoading: false, message: "You're not connected to the internet.")
                    self.collectionView.alwaysBounceVertical = false
                }
            }
        }
    }
    
    // MARK: - Objc Methods
    @objc private func performNetworkCall(_ text: String) {
        isSearching(true)
        clearData()
        getSearchResults(searchText: text)
    }
    
    // MARK: - Deinit
    deinit {
        notificationCenter.removeObserver(self)
        print("Deinit \(FileIndentifier.Search.rawValue)")
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Extension
extension SearchViewController {
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let item = collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
            Errors.error(in: FileIndentifier.Search.rawValue, at: #line)
            return
        }
        presentDetailViewController(item: item)
    }

    override func collectionView(_ collectionView: UICollectionView, contextMenuConfigurationForItemAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        .init(identifier: nil, previewProvider: nil) { [weak self] (_) -> UIMenu? in
            guard let snapshot = self?.collectionViewDiffableDataSource?.snapshot() else {
                return nil
            }
            guard let item = self?.collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
                return nil
            }
            let section = snapshot.sectionIdentifier(containingItem: item)
            switch section {
                case .persons:
                    return nil
                default:
                    return self?.createContextMenu(indexPath: indexPath)
            }
        }
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Check if search text contains text
        if searchText.containsText() {
            setIsTrending(false)
            // Check for the first time if last searched text is empty
            if isLastSearchedTextEmpty() {
                // If it's not empty, save the search text as the last search text for the first time
                lastSearchedText = searchText
            }

            // Will cancel the request for the last searched text when user finishes typing before 0.7 seconds
            // This code runs every time user types a character
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(performNetworkCall(_:)), object: lastSearchedText)

            // Set the current search text as the last searched text
            // So if the user does not finish typing before 0.7 seconds, the request for the new last searched text will be canceled
            // This code runs every time user types a character
            lastSearchedText = searchText

            // Only perform the network call after 0.7 seconds when the user finishes typing
            // This code only runs after 0.7 seconds when user finished typing.
            perform(#selector(performNetworkCall), with: searchText, afterDelay: 0.7)
        } else {
            isSearching(false)
            setIsTrending(true)
            // Cancel request for the last searched text if search text is empty.
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(performNetworkCall(_:)), object: lastSearchedText)

            // Show the default results
            if hasInitialDataBeenLoaded {
                createSnapshot()
            }
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(performNetworkCall(_:)), object: lastSearchedText)
        isSearching(false)
        setIsTrending(true)
        if hasInitialDataBeenLoaded {
            createSnapshot()
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        collectionView.endEditing(true)
    }
}

extension SearchViewController: SearchHeaderProtocol {
    func didPressSeeAllButton(section: SearchSection?) {
        presentDetailListViewcontroller(section: section)
    }
}

extension SearchViewController: UIContextMenuInteractionDelegate {
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        nil
    }
}

extension SearchViewController: UIActivityItemSource {
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        // Let UIKit knows the type of data we wanna share
        guard let title = sharingItem?.title ?? sharingItem?.name else {
            return ""
        }
        return title
    }

    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        // The custom body of what we wanna share. For instance, this will be the custom message for the mail's body or twitter's.
        let title = (sharingItem?.title ?? sharingItem?.name) ?? ""
        let description = sharingItem?.overview ?? ""
        guard let dummyLink = URL(string: "www.dummylink.com") else {
            return title + "\n\n" + description
        }

        switch activityType {
            case UIActivity.ActivityType.mail:
                let message = title + "\n\n" + description + "\n\n" + "\(dummyLink)"
                return message
            default:
                let url = "\(dummyLink)"
                return url
        }
    }

    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        // The custom subject of what we wanna share in mail
        guard let subject = sharingItem?.title ?? sharingItem?.name else {
            return "The subject"
        }
        return subject
    }

    func activityViewControllerLinkMetadata(_ activityViewController: UIActivityViewController) -> LPLinkMetadata? {
        let metadata = LPLinkMetadata()
        guard let image = sharingItemImageView.image else {
            return metadata
        }
        metadata.iconProvider = .init(object: image)
        guard let item = sharingItem else {
            return metadata
        }
        let title = item.title ?? item.name
        metadata.title = title
        return metadata
    }
}

extension SearchViewController {
    private func makeCellRegistration() {
        cellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            guard let self = self else {
                return
            }
            cell.configure(item: item)
            cell.addInteraction(self.interaction)
        })
    }

    private func makeHeaderRegistration() {
        headerRegistration = .init(elementKind: headerKind, handler: { [weak self] (header, kind, indexPath) in
            guard let self = self else {
                return
            }
            header.delegate = self
            let snapshot = self.collectionViewDiffableDataSource?.snapshot()
            guard let item = self.collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
                Errors.error(in: FileIndentifier.Search.rawValue, at: #line)
                return
            }
            guard let section = snapshot?.sectionIdentifier(containingItem: item) else {
                Errors.error(in: FileIndentifier.Search.rawValue, at: #line)
                return
            }
            switch section {
                case .trendingMovies:
                    header.configure(section: .trendingMovies, numberOfItems: self.trendingMovies.count)
                case .trendingtvShows:
                    header.configure(section: .trendingtvShows, numberOfItems: self.trendingTVShows.count)
                case .movies:
                    header.configure(section: .movies, numberOfItems: self.movies.count)
                case .tvShows:
                    header.configure(section: .tvShows, numberOfItems: self.tvShows.count)
                case .persons:
                    header.configure(section: .persons, numberOfItems: self.persons.count)
            }
        })
    }

    private func configureCell() {
        guard let registration = cellRegistration else {
            return
        }
        collectionViewDiffableDataSource = .init(collectionView: collectionView, cellProvider: { [weak self] (collectionView, indexPath, item) -> UICollectionViewCell? in
            guard let _ = self else {
                return UICollectionViewCell()
            }
            let cell = collectionView.dequeueConfiguredReusableCell(using: registration, for: indexPath, item: item)
            return cell
        })
    }

    private func configureHeader() {
        guard let registration = headerRegistration else {
            return
        }
        collectionViewDiffableDataSource?.supplementaryViewProvider = .some({ [weak self] (collectionView, kind, indexPath) -> UICollectionReusableView? in
            guard let _ = self else {
                return UICollectionReusableView()
            }
            let header = collectionView.dequeueConfiguredReusableSupplementary(using: registration, for: indexPath)
            return header
        })
    }
}

// MARK: - Previews
struct SearchContentView: UIViewControllerRepresentable {
    func makeUIViewController(context: Context) -> UIViewController {
        let controller = SearchViewController()
        return controller
    }

    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {}

    typealias UIViewControllerType = UIViewController
}

struct SearchContentView_Previews: PreviewProvider {
    static var previews: some View {
        SearchContentView()
            .edgesIgnoringSafeArea(.all)
    }
}
