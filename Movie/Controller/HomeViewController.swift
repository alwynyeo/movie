//
//  HomeViewController.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/25/20.
//

import SwiftUI
import Network
import LinkPresentation

fileprivate typealias HomeHeaderRegistration = UICollectionView.SupplementaryRegistration<SectionHeader>
fileprivate typealias HomeMovieCellRegistration = UICollectionView.CellRegistration<HomeMovieCell, MediaItem>
fileprivate typealias HomeTvShowCellRegistration = UICollectionView.CellRegistration<HomeTvShowCell, MediaItem>
fileprivate typealias HomeDiffableDataSource = UICollectionViewDiffableDataSource<Section, MediaItem>
fileprivate typealias Snapshot = NSDiffableDataSourceSnapshot<Section, MediaItem>

fileprivate enum Section: String {
    case topRatedMovies = "Top Rated Movies"
    case nowPlayingMovies = "Now Playing Movies"
    case popularMovies = "Popular Movies"
    case upcomingMovies = "Upcoming Movies"
    case topRatedTvShows = "Top Rated TV Shows"
    case nowPlayingTvShows = "Now Playing TV Shows"
    case popularTvShows = "Popular TV Shows"
}

extension Section: CaseIterable {}

// MARK: - Class
class HomeViewController: UICollectionViewController {
    
    // MARK: - Declarations (Properties)
    private var headerRegistration: HomeHeaderRegistration?
    private var movieCellRegistration: HomeMovieCellRegistration?
    private var tvShowCellRegistration: HomeTvShowCellRegistration?
    private var collectionViewDiffableDataSource: HomeDiffableDataSource?
    private var topRatedMovies = [MediaItem]()
    lazy var mockTopRatedMovies = topRatedMovies
    private var nowPlayingMovies = [MediaItem]()
    private var popularMovies = [MediaItem]()
    private var upcomingMovies = [MediaItem]()
    private var topRatedTvShows = [MediaItem]()
    private var nowPlayingTvShows = [MediaItem]()
    private var popularTvShows = [MediaItem]()
    private let headerKind = UICollectionView.elementKindSectionHeader
    private let group = DispatchGroup()
    private let main = DispatchQueue.main
    private var selectedMediaType = ""
    private var hasInitialDataBeenLoaded = false
    private lazy var interaction = UIContextMenuInteraction(delegate: self)
    private let notificationCenter = NotificationCenter.default
    private var sharingItem: MediaItem?
    private var sharingItemImageView = CustomImageView()
    private var isDataFailedFetching = false
    private var isNetworkConnected = false
    
    // MARK: - Declarations (UI)
    fileprivate lazy var emptyView = CustomEmptyView(frame: collectionView.frame)

    // MARK: - Initialize
    init() {
        super.init(collectionViewLayout: HomeLayout.layout())
    }

    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: - Configuration
    private func setupView() {
        view.accessibilityIdentifier = "homeView"
        configureCollectionView()
        configureDiffableDataSource()
        networkChangesObserver()
        configureItems()
    }
    
    private func configureCollectionView() {
        collectionView.backgroundColor = .systemBackground
        collectionView.backgroundView = emptyView
        collectionView.alwaysBounceVertical = false
    }

    private func configureDiffableDataSource() {
        makeCellRegistration()
        makeHeaderRegistration()
        configureCell()
        configureHeader()
    }

    private func applySnapshot() {
        var snapshot = Snapshot()

        snapshot.appendSections(Section.allCases)
        snapshot.appendItems(topRatedMovies, toSection: .topRatedMovies)
        snapshot.appendItems(nowPlayingMovies, toSection: .nowPlayingMovies)
        snapshot.appendItems(popularMovies, toSection: .popularMovies)
        snapshot.appendItems(upcomingMovies, toSection: .upcomingMovies)
        snapshot.appendItems(topRatedTvShows, toSection: .topRatedTvShows)
        snapshot.appendItems(nowPlayingTvShows, toSection: .nowPlayingTvShows)
        snapshot.appendItems(popularTvShows, toSection: .popularTvShows)

        collectionViewDiffableDataSource?.apply(snapshot)
    }
    
    // MARK: - Methods
    private func configureItems() {
        getItems(of: .movie)
        getItems(of: .tv)
        group.notify(queue: .main) {
            self.applySnapshot()
            self.hasInitialDataBeenLoaded = true
            if self.isDataFailedFetching {
                if self.isNetworkConnected {
                    self.emptyView.configure(isLoading: false, message: "You're not connected to the internet.")
                } else {
                    self.emptyView.configure(isLoading: false, message: "Something went wrong. Please try again later.")
                }
            } else {
                self.emptyView.configure(isLoading: false, message: nil)
            }
            self.collectionView.alwaysBounceVertical = true
        }
    }

    private func getItems(of type: MediaType) {
        switch type {
            case .movie:
                getResults(of: .topRated, mediaType: .movie)
                getResults(of: .nowPlaying, mediaType: .movie)
                getResults(of: .popular, mediaType: .movie)
                getResults(of: .upcoming, mediaType: .movie)
            default:
                getResults(of: .topRated, mediaType: .tv)
                getResults(of: .airToday, mediaType: .tv)
                getResults(of: .popular, mediaType: .tv)
        }
    }

    private func getResults(of section: HomeSectionType, mediaType type: MediaType) {
        group.enter()
        Networking.manager.getHomeResults(of: section, mediaType: type.rawValue, completion: { [weak self] (result) in
            switch result {
                case .success(let items):
                    self?.success(section: section, mediaType: type, items: items)
                case .failure(let error):
                    self?.failed(error: error)
            }
            self?.group.leave()
        })
    }

    private func success(section: HomeSectionType, mediaType type: MediaType, items: [MediaItem]) {
        switch section {
            case .topRated:
                switch type {
                    case .movie:
                        topRatedMovies.append(contentsOf: items)
                    default:
                        topRatedTvShows.append(contentsOf: items)
                }
            case .nowPlaying:
                nowPlayingMovies.append(contentsOf: items)
            case .airToday:
                nowPlayingTvShows.append(contentsOf: items)
            case .popular:
                switch type {
                    case .movie:
                        popularMovies.append(contentsOf: items)
                    default:
                        popularTvShows.append(contentsOf: items)
                }
            default:
                upcomingMovies.append(contentsOf: items)
        }
        isDataFailedFetching = false
    }

    private func failed(error: Error) {
        isDataFailedFetching = true
        Errors.error(in: FileIndentifier.Home.rawValue, at: #line, error: error)
        main.async {
            self.emptyView.configure(isLoading: false, message: "Something went wrong. Please try again later.")
        }
    }

    private func presentDetailViewController(item: MediaItem) {
        let controller = DetailViewController(mediaType: selectedMediaType)
        controller.configure(item: item)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func presentDetailListViewController(section title: String, homeSectionType type: HomeSectionType) {
        let layout = UICollectionViewFlowLayout()
        let controller = DetailListViewController(collectionViewLayout: layout)
        var items = [MediaItem]()
        var mediaType = ""
        switch title {
            case Section.topRatedMovies.rawValue:
                items = topRatedMovies
                mediaType = MediaType.movie.rawValue
            case Section.nowPlayingMovies.rawValue:
                items = nowPlayingMovies
                mediaType = MediaType.movie.rawValue
            case Section.popularMovies.rawValue:
                items = popularMovies
                mediaType = MediaType.movie.rawValue
            case Section.upcomingMovies.rawValue:
                items = upcomingMovies
                mediaType = MediaType.movie.rawValue
            case Section.topRatedTvShows.rawValue:
                items = topRatedTvShows
                mediaType = MediaType.tv.rawValue
            case Section.nowPlayingTvShows.rawValue:
                items = nowPlayingTvShows
                mediaType = MediaType.tv.rawValue
            case Section.popularTvShows.rawValue:
                items = popularTvShows
                mediaType = MediaType.tv.rawValue
            default:
                return
        }
        controller.configure(section: title, items: items, searchText: nil, mediaType: mediaType, homeSectionType: type)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func shareAction(item: MediaItem) -> UIAction {
        sharingItem = item
        let path = item.backdrop_path ?? item.poster_path
        sharingItemImageView.configure(path: path)
        let action = UIAction(title: "Share", image: UIImage(systemName: "square.and.arrow.up")) { [unowned self] (_) in
            let items: [Any] = [
                self
            ]
            let controller = UIActivityViewController(activityItems: items, applicationActivities: nil)
            controller.popoverPresentationController?.sourceView = view
            controller.excludedActivityTypes = [
                .addToReadingList,
                .assignToContact,
                .markupAsPDF,
                .openInIBooks,
                .print,
                .postToFlickr,
                .postToVimeo,
                .saveToCameraRoll,
            ]
            self.present(controller, animated: true)
        }
        return action
    }

    private func addOrDeleteAction(item: MediaItem) -> UIAction {
        let favoritedItems = defaults.favoritedItems()
        let isFavoritedItem = favoritedItems.contains(where: { $0.id == item.id })
        if isFavoritedItem {
            let action = UIContextMenuActions.deleteAction(item: item) { [weak self] in
                // Post a notification to notify FavoriteViewController the changes
                self?.postFavoriteItemChangesNotification()
            }
            return action
        } else {
            let action = UIContextMenuActions.addAction(item: item, selectedMediaType: selectedMediaType) { [weak self] in
                // Post a notification to notify FavoriteViewController the changes
                self?.postFavoriteItemChangesNotification()
            }
            return action
        }
    }

    private func createContextMenu(indexPath: IndexPath) -> UIMenu {
        guard let item = collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
            return UIMenu()
        }
        let menu = UIMenu(children: [
            UIContextMenuActions.copyAction(item: item),
            shareAction(item: item),
            addOrDeleteAction(item: item),
        ])
        return menu
    }

    private func postFavoriteItemChangesNotification() {
        notificationCenter.post(name: favoritedItemChangesNotification, object: nil)
        let newFavoriteCount = defaults.newFavoriteItemCount()
        if newFavoriteCount == .zero {
            mainTabBarController?.viewControllers?[1].tabBarItem.badgeValue = nil
        } else {
            mainTabBarController?.viewControllers?[1].tabBarItem.badgeValue = "\(newFavoriteCount)"
        }
    }

    private func networkChangesObserver() {
        let monitor = NWPathMonitor()
        monitor.startMonitoringNetworkChanges { [weak self] (status) in
            switch status {
                case .satisfied:
                    self?.isNetworkConnected = true
                    self?.isNetworkSatisfied(true)
                case .unsatisfied:
                    self?.isNetworkConnected = false
                    self?.isNetworkSatisfied(false)
                case .requiresConnection:
                    print("Requires Connection")
                default:
                    return
            }
        }
    }

    private func isNetworkSatisfied(_ satisfied: Bool) {
        // Check if the initial data has been loaded
        // If not loaded, empty state will be shown
        main.async {
            if !self.hasInitialDataBeenLoaded {
                if satisfied {
                    // Show activity indicator view
                    self.emptyView.configure(isLoading: true, message: nil)
                } else {
                    // Show error message
                    self.emptyView.configure(isLoading: false, message: "No Data Available.")
                }
            }
        }
    }

    // MARK: - Objc Methods
    
    // MARK: - Deinit
    deinit {
        notificationCenter.removeObserver(self)
        print("Deinit \(FileIndentifier.Home.rawValue)")
    }

    // MARK: - //
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Extension
extension HomeViewController {
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let snapshot = collectionViewDiffableDataSource?.snapshot() else {
            return
        }
        guard let item = collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
            return
        }
        let section = snapshot.sectionIdentifier(containingItem: item)
        switch section {
            case .topRatedMovies:
                selectedMediaType = MediaType.movie.rawValue
            case .nowPlayingMovies:
                selectedMediaType = MediaType.movie.rawValue
            case .popularMovies:
                selectedMediaType = MediaType.movie.rawValue
            case .upcomingMovies:
                selectedMediaType = MediaType.movie.rawValue
            default:
                selectedMediaType = MediaType.tv.rawValue
        }
        presentDetailViewController(item: item)
    }

    override func collectionView(_ collectionView: UICollectionView, contextMenuConfigurationForItemAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        guard let snapshot = collectionViewDiffableDataSource?.snapshot() else {
            return nil
        }
        guard let item = collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
            return nil
        }
        let section = snapshot.sectionIdentifier(containingItem: item)
        switch section {
            case .topRatedMovies:
                selectedMediaType = MediaType.movie.rawValue
            case .nowPlayingMovies:
                selectedMediaType = MediaType.movie.rawValue
            case .popularMovies:
                selectedMediaType = MediaType.movie.rawValue
            case .upcomingMovies:
                selectedMediaType = MediaType.movie.rawValue
            default:
                selectedMediaType = MediaType.tv.rawValue
        }
        return .init(identifier: nil, previewProvider: nil) { [weak self] (_) -> UIMenu? in
            self?.createContextMenu(indexPath: indexPath)
        }
    }
}

extension HomeViewController: SectionHeaderProtocol {
    func didPressSeeAllButton(section title: String, homeSectionType type: HomeSectionType) {
        presentDetailListViewController(section: title, homeSectionType: type)
    }
}

extension HomeViewController: UIContextMenuInteractionDelegate {
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        nil
    }
}

extension HomeViewController: UIActivityItemSource {
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        // Let UIKit knows the type of data we wanna share
        guard let title = sharingItem?.title ?? sharingItem?.name else {
            return ""
        }
        return title
    }

    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        // The custom body of what we wanna share. For instance, this will be the custom message for the mail's body or twitter's.
        let title = (sharingItem?.title ?? sharingItem?.name) ?? ""
        let description = sharingItem?.overview ?? ""
        guard let dummyLink = URL(string: "www.dummylink.com") else {
            return title + "\n\n" + description
        }

        switch activityType {
            case UIActivity.ActivityType.mail:
                let message = title + "\n\n" + description + "\n\n" + "\(dummyLink)"
                return message
            default:
                let url = "\(dummyLink)"
                return url
        }
    }

    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        // The custom subject of what we wanna share in mail
        guard let subject = sharingItem?.title ?? sharingItem?.name else {
            return "The subject"
        }
        return subject
    }

    func activityViewControllerLinkMetadata(_ activityViewController: UIActivityViewController) -> LPLinkMetadata? {
        let metadata = LPLinkMetadata()
        guard let image = sharingItemImageView.image else {
            return metadata
        }
        metadata.iconProvider = .init(object: image)
        guard let item = sharingItem else {
            return metadata
        }
        let title = item.title ?? item.name
        metadata.title = title
        return metadata
    }
}

extension HomeViewController {
    private func makeCellRegistration() {
        movieCellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            guard let self = self else {
                return
            }
            cell.configure(item: item)
            cell.addInteraction(self.interaction)
        })
        tvShowCellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            guard let self = self else {
                return
            }
            cell.configure(item: item)
            cell.addInteraction(self.interaction)
        })
    }

    private func makeHeaderRegistration() {
        headerRegistration = .init(elementKind: headerKind, handler: { [weak self] (header, kind, indexPath) in
            guard let snapshot = self?.collectionViewDiffableDataSource?.snapshot() else {
                return
            }
            guard let item = self?.collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
                return
            }
            let section = snapshot.sectionIdentifier(containingItem: item)

            switch section {
                case .topRatedMovies:
                    header.configure(title: Section.topRatedMovies.rawValue, homeSectionType: .topRated)
                    header.enableConstraint(true)
                    header.delegate = self
                    if self?.topRatedMovies.count ?? .zero > 1 {
                        header.showSeeAllButton()
                    } else {
                        header.showSeeAllButton(false)
                    }
                case .nowPlayingMovies:
                    header.configure(title: Section.nowPlayingMovies.rawValue, homeSectionType: .nowPlaying)
                    header.enableConstraint(true)
                    header.delegate = self
                    if self?.nowPlayingMovies.count ?? .zero > 1 {
                        header.showSeeAllButton()
                    } else {
                        header.showSeeAllButton(false)
                    }
                case .popularMovies:
                    header.configure(title: Section.popularMovies.rawValue, homeSectionType: .popular)
                    header.enableConstraint(true)
                    header.delegate = self
                    if self?.popularMovies.count ?? .zero > 1 {
                        header.showSeeAllButton()
                    } else {
                        header.showSeeAllButton(false)
                    }
                case .upcomingMovies:
                    header.configure(title: Section.upcomingMovies.rawValue, homeSectionType: .upcoming)
                    header.enableConstraint(true)
                    header.delegate = self
                    if self?.upcomingMovies.count ?? .zero > 1 {
                        header.showSeeAllButton()
                    } else {
                        header.showSeeAllButton(false)
                    }
                case .topRatedTvShows:
                    header.configure(title: Section.topRatedTvShows.rawValue, homeSectionType: .topRated)
                    header.enableConstraint(true)
                    header.delegate = self
                    if self?.topRatedTvShows.count ?? .zero > 1 {
                        header.showSeeAllButton()
                    } else {
                        header.showSeeAllButton(false)
                    }
                case .nowPlayingTvShows:
                    header.configure(title: Section.nowPlayingTvShows.rawValue, homeSectionType: .airToday)
                    header.enableConstraint(true)
                    header.delegate = self
                    if self?.nowPlayingTvShows.count ?? .zero > 1 {
                        header.showSeeAllButton()
                    } else {
                        header.showSeeAllButton(false)
                    }
                case .popularTvShows:
                    header.configure(title: Section.popularTvShows.rawValue, homeSectionType: .popular)
                    header.enableConstraint(true)
                    header.delegate = self
                    if self?.popularTvShows.count ?? .zero > 1 {
                        header.showSeeAllButton()
                    } else {
                        header.showSeeAllButton(false)
                    }
                default:
                    return
            }
        })
    }

    private func configureCell() {
        guard let movieRegistration = movieCellRegistration else {
            return
        }
        guard let tvShowRegistration = tvShowCellRegistration else {
            return
        }
        collectionViewDiffableDataSource = .init(collectionView: collectionView, cellProvider: { [weak self] (collectionView, indexPath, item) -> UICollectionViewCell? in
            guard let snapshot = self?.collectionViewDiffableDataSource?.snapshot() else {
                return UICollectionViewCell()
            }
            let section = snapshot.sectionIdentifier(containingItem: item)

            switch section {
                case .topRatedMovies:
                    let cell = collectionView.dequeueConfiguredReusableCell(using: movieRegistration, for: indexPath, item: item)
                    return cell
                case .nowPlayingMovies:
                    let cell = collectionView.dequeueConfiguredReusableCell(using: movieRegistration, for: indexPath, item: item)
                    return cell
                case .popularMovies:
                    let cell = collectionView.dequeueConfiguredReusableCell(using: movieRegistration, for: indexPath, item: item)
                    return cell
                case .upcomingMovies:
                    let cell = collectionView.dequeueConfiguredReusableCell(using: movieRegistration, for: indexPath, item: item)
                    return cell
                default:
                    let cell = collectionView.dequeueConfiguredReusableCell(using: tvShowRegistration, for: indexPath, item: item)
                    return cell
            }
        })
    }

    private func configureHeader() {
        guard let registration = headerRegistration else {
            return
        }
        collectionViewDiffableDataSource?.supplementaryViewProvider = .some({ [weak self] (collectionView, kind, indexPath) -> UICollectionReusableView? in
            guard let _ = self else {
                return UICollectionReusableView()
            }
            let header = collectionView.dequeueConfiguredReusableSupplementary(using: registration, for: indexPath)
            return header
        })
    }
}

// Previews
struct HomeContentView: UIViewControllerRepresentable {
    func makeUIViewController(context: Context) -> UIViewController {
        let controller = HomeViewController()
        return controller
    }

    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
    }

    typealias UIViewControllerType = UIViewController
}

struct HomeContentView_Previews: PreviewProvider {
    static var previews: some View {
        HomeContentView()
            .edgesIgnoringSafeArea(.all)
    }
}
