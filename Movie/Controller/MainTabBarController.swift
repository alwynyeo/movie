//
//  MainTabBarController.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/25/20.
//

import SwiftUI

// MARK: - Class
final class MainTabBarController: UITabBarController {
    
    // MARK: - Declarations (Properties)
    private let newFavoriteItemCount = defaults.newFavoriteItemCount()
    
    // MARK: - Declarations (UI)
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        view.backgroundColor = .clear
        configureViewControllers()
    }
    
    private func configureViewControllers() {
        viewControllers = [
            makeHomeViewController(),
            makeFavoriteViewController(),
            makeSearchViewController(),
        ]

        guard let items = tabBar.items else {
            Errors.error(in: FileIndentifier.Main.rawValue, at: #line, error: nil)
            return
        }

        for item in items {
            item.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        }
    }

    private func makeHomeViewController() -> UIViewController {
        let unselectedImage = UIImage(systemName: "house")?.withTintColor(.label, renderingMode: .alwaysOriginal)
        let selectedImage = UIImage(systemName: "house.fill")?.withTintColor(.label, renderingMode: .alwaysOriginal)
        let controller = HomeViewController()
        let navigationController = makeNavigationController(title: "Home", unselectedImage: unselectedImage, selectedImage: selectedImage, viewController: controller)
        return navigationController
    }

    private func makeSearchViewController() -> UIViewController {
        let unselectedImage = UIImage(systemName: "magnifyingglass.circle")?.withTintColor(.label, renderingMode: .alwaysOriginal)
        let selectedImage = UIImage(systemName: "magnifyingglass.circle.fill")?.withTintColor(.label, renderingMode: .alwaysOriginal)
        let controller = SearchViewController()
        let navigationController = makeNavigationController(title: "Search", unselectedImage: unselectedImage, selectedImage: selectedImage, viewController: controller)
        return navigationController
    }

    private func makeFavoriteViewController() -> UIViewController {
        let unselectedImage = UIImage(systemName: "heart")?.withTintColor(.label, renderingMode: .alwaysOriginal)
        let selectedImage = UIImage(systemName: "heart.fill")?.withTintColor(.label, renderingMode: .alwaysOriginal)
        let layout = UICollectionViewFlowLayout()
        let controller = FavoriteViewController(collectionViewLayout: layout)
        let navigationController = makeNavigationController(title: "Favorites", unselectedImage: unselectedImage, selectedImage: selectedImage, viewController: controller)
        if newFavoriteItemCount <= .zero {
            navigationController.tabBarItem.badgeValue = nil
        } else {
            navigationController.tabBarItem.badgeValue = "\(newFavoriteItemCount)"
        }
        return navigationController
    }
    
    // MARK: - Methods
    private func makeNavigationController(title: String, unselectedImage: UIImage?, selectedImage: UIImage?, viewController controller: UIViewController = UIViewController(), isExtendedLayoutIncludesOpaqueBars extendedLayoutIncludesOpaqueBars: Bool = false) -> UINavigationController {
        
        let navigationController = CustomNavigationController(rootViewController: controller)

        navigationController.navigationBar.topItem?.title = title
        navigationController.navigationBar.prefersLargeTitles = true
        navigationController.extendedLayoutIncludesOpaqueBars = extendedLayoutIncludesOpaqueBars

        navigationController.tabBarItem.image = unselectedImage
        navigationController.tabBarItem.selectedImage = selectedImage

        navigationController.viewRespectsSystemMinimumLayoutMargins = false
        navigationController.navigationBar.preservesSuperviewLayoutMargins = true
        navigationController.navigationBar.directionalLayoutMargins = .init(top: .zero, leading: 32, bottom: .zero, trailing: 32)

        tabBarController?.viewRespectsSystemMinimumLayoutMargins = false
        tabBar.preservesSuperviewLayoutMargins = true
        tabBar.directionalLayoutMargins = .init(top: .zero, leading: 32, bottom: .zero, trailing: 32)

        return navigationController
    }
    
    // MARK: - Objc Methods
    
    // MARK: - Deinit
    deinit {
        print("Deinit \(FileIndentifier.Main.rawValue)")
    }
    
}

// MARK: - Extension
extension MainTabBarController {}

// Previews
struct MainContentView: UIViewControllerRepresentable {
    func makeUIViewController(context: Context) -> UIViewController {
        let controller = MainTabBarController()
        return controller
    }

    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
    }

    typealias UIViewControllerType = UIViewController
}

struct MainContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainContentView()
            .edgesIgnoringSafeArea(.all)
    }
}
