//
//  PersonDetailViewController.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/11/20.
//

import UIKit

fileprivate typealias PersonHeaderRegistration = UICollectionView.SupplementaryRegistration<SectionHeader>
fileprivate typealias PersonCellRegistration = UICollectionView.CellRegistration<DetailRelatedCell, MediaItem>
fileprivate typealias PersonDiffableDataSource = UICollectionViewDiffableDataSource<Section, MediaItem>
fileprivate typealias Snapshot = NSDiffableDataSourceSnapshot<Section, MediaItem>

fileprivate enum Section: String {
    case movies = "Movies"
    case tvShows = "TV Shows"
}

// MARK: - Class
class PersonDetailViewController: UICollectionViewController {
    
    // MARK: - Declarations (Properties)
    private var headerRegistration: PersonHeaderRegistration?
    private var cellRegistration: PersonCellRegistration?
    private var collectionViewDiffableDataSource: PersonDiffableDataSource?
    private var item: AnyHashable? {
        didSet {
            setData()
        }
    }
    private var personId = 0
    private var movies = [MediaItem]()
    private var tvShows = [MediaItem]()
    private let group = DispatchGroup()
    private let main = DispatchQueue.main
    private let headerKind = UICollectionView.elementKindSectionHeader
    
    // MARK: - Declarations (UI)
    fileprivate lazy var emptyView = CustomEmptyView(frame: collectionView.frame)
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    // MARK: - Initialization
    init() {
        super.init(collectionViewLayout: PersonDetailLayout.layout())
    }

    // MARK: - Configuration
    private func setupView() {
        view.accessibilityIdentifier = "personDetailView"
        configureNavigationBar()
        configureCollectionView()
        configureDiffableDataSource()
        getData()
    }

    private func configureNavigationBar() {
        navigationItem.largeTitleDisplayMode = .always
    }
    
    private func configureCollectionView() {
        collectionView.backgroundColor = .systemBackground
        collectionView.alwaysBounceVertical = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.backgroundView = emptyView
    }

    private func configureDiffableDataSource() {
        makeCellRegistration()
        makeHeaderRegistration()
        configureCell()
        configureHeader()
    }

    private func applySnapshot() {
        var snapshot = Snapshot()

        if !movies.isEmpty {
            snapshot.appendSections([.movies])
            snapshot.appendItems(movies, toSection: .movies)
        }

        if !tvShows.isEmpty {
            snapshot.appendSections([.tvShows])
            snapshot.appendItems(tvShows, toSection: .tvShows)
        }

        collectionViewDiffableDataSource?.apply(snapshot)
    }
    
    // MARK: - Methods
    func configure(item: AnyHashable) {
        self.item = item
    }

    private func setData() {
        if let item = self.item as? MediaItem {
            guard let id = item.id else {
                return
            }
            personId = id
            navigationItem.title = item.name
        } else {
            guard let item = self.item as? CastItem else {
                return
            }
            guard let id = item.id else {
                return
            }
            personId = id
            navigationItem.title = item.name
        }
    }

    private func getData() {
        getCredits()
        group.notify(queue: .main) {
            self.applySnapshot()
            self.emptyView.configure(isLoading: false, message: nil)
        }
    }

    
    private func getCredits() {
        group.enter()
        Networking.manager.getCredits(personId: personId) { [weak self] (result) in
            switch result {
                case .success(let items):
                    self?.success(items: items)
                case .failure(let error):
                    Errors.error(in: FileIndentifier.PersonDetail.rawValue, at: #line, error: error)
            }
            self?.group.leave()
        }
    }

    private func success(items: [MediaItem]) {
        items.forEach { (item) in
            guard let type = item.media_type else {
                return
            }
            switch type {
                case MediaType.movie.rawValue:
                    movies.append(item)
                case MediaType.tv.rawValue:
                    tvShows.append(item)
                default:
                    return
            }
        }
    }

    private func presentDetailViewController(item: MediaItem) {
        guard let mediaType = item.media_type else {
            return
        }
        let controller = DetailViewController(mediaType: mediaType)
        controller.configure(item: item)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func presentDetailListViewcontroller(section: String) {
        let layout = UICollectionViewFlowLayout()
        let controller = DetailListViewController(collectionViewLayout: layout)
        var mediaType = ""

        switch section {
            case Section.movies.rawValue:
                mediaType = MediaType.movie.rawValue
                controller.configure(section: Section.movies.rawValue, items: movies, searchText: nil, mediaType: mediaType)
            default:
                mediaType = MediaType.tv.rawValue
                controller.configure(section: Section.tvShows.rawValue, items: tvShows, searchText: nil, mediaType: mediaType)
        }
        navigationController?.pushViewController(controller, animated: true)
    }

    // MARK: - Objc Methods
    
    // MARK: - Deinit
    deinit {
        print("Deinit \(FileIndentifier.PersonDetail.rawValue)")
    }

    // MARK: - //
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Extension
extension PersonDetailViewController {
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let item = collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
            return
        }
        presentDetailViewController(item: item)
    }
}

extension PersonDetailViewController: SectionHeaderProtocol {
    func didPressSeeAllButton(section: String) {
        presentDetailListViewcontroller(section: section)
    }
}

extension PersonDetailViewController {
    private func makeCellRegistration() {
        cellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            guard let _ = self else {
                return
            }
            cell.configure(item: item)
        })
    }

    private func makeHeaderRegistration() {
        headerRegistration = .init(elementKind: headerKind, handler: { [weak self] (header, kind, indexPath) in
            header.delegate = self
            guard let snapshot = self?.collectionViewDiffableDataSource?.snapshot() else {
                return
            }

            guard let item = self?.collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
                return
            }

            let section = snapshot.sectionIdentifier(containingItem: item)

            switch section {
                case .movies:
                    header.configure(title: Section.movies.rawValue)
                    if self?.movies.count ?? .zero > 1 {
                        header.showSeeAllButton()
                    } else {
                        header.showSeeAllButton(false)
                    }
                case .tvShows:
                    header.configure(title: Section.tvShows.rawValue)
                    if self?.tvShows.count ?? .zero > 1 {
                        header.showSeeAllButton()
                    } else {
                        header.showSeeAllButton(false)
                    }
                default:
                    return
            }
        })
    }

    private func configureCell() {
        guard let registration = cellRegistration else {
            return
        }
        collectionViewDiffableDataSource = .init(collectionView: collectionView, cellProvider: { [weak self] (collectionView, indexPath, item) -> UICollectionViewCell? in
            guard let _ = self else {
                return UICollectionViewCell()
            }
            let cell = collectionView.dequeueConfiguredReusableCell(using: registration, for: indexPath, item: item)
            return cell
        })
    }

    private func configureHeader() {
        guard let registration = headerRegistration else {
            return
        }
        collectionViewDiffableDataSource?.supplementaryViewProvider = .some({ [weak self] (collectionView, kind, indexPath) -> UICollectionReusableView? in
            guard let _ = self else {
                return UICollectionReusableView()
            }
            let header = collectionView.dequeueConfiguredReusableSupplementary(using: registration, for: indexPath)
            return header
        })
    }
}
