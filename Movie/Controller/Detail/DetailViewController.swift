//
//  DetailViewController.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/7/20.
//

import UIKit
import Network
import LinkPresentation

fileprivate typealias DetailHeaderRegistration = UICollectionView.SupplementaryRegistration<DetailHeader>
fileprivate typealias DetailSectionHeaderRegistration = UICollectionView.SupplementaryRegistration<SectionHeader>
fileprivate typealias DetailHeaderCellRegistration = UICollectionView.CellRegistration<DetailHeaderCell, MediaItem>
fileprivate typealias DetailSeasonCellRegistration = UICollectionView.CellRegistration<DetailSeasonCell, SeasonItem>
fileprivate typealias DetailRelatedCellRegistration = UICollectionView.CellRegistration<DetailRelatedCell, MediaItem>
fileprivate typealias DetailPersonCellRegistration = UICollectionView.CellRegistration<DetailPersonCell, CastItem>
fileprivate typealias DetailAboutCellRegistration = UICollectionView.CellRegistration<DetailAboutCell, MediaItem>
fileprivate typealias DetailInfoCellRegistration = UICollectionView.CellRegistration<DetailInfoCell, MediaItem>
fileprivate typealias DetailDiffableDataSource = UICollectionViewDiffableDataSource<DetailSection, AnyHashable>
fileprivate typealias Snapshot = NSDiffableDataSourceSnapshot<DetailSection, AnyHashable>

enum DetailSection: String {
    case header
    case season = "Season"
    case related = "Related"
    case cast = "Cast"
    case about = "About"
    case info = "Information"
}

extension DetailSection: CaseIterable {}

// MARK: - Class
final class DetailViewController: UICollectionViewController {
    
    // MARK: - Declarations (Properties)
    private var headerRegistration: DetailHeaderRegistration?
    private var sectionHeaderRegistration: DetailSectionHeaderRegistration?
    private var headerCellRegistration: DetailHeaderCellRegistration?
    private var seasonCellRegistration: DetailSeasonCellRegistration?
    private var relatedCellRegistration: DetailRelatedCellRegistration?
    private var personCellRegistration: DetailPersonCellRegistration?
    private var aboutCellRegistration: DetailAboutCellRegistration?
    private var infoCellRegistration: DetailInfoCellRegistration?
    private var collectionViewDiffableDataSource: DetailDiffableDataSource?
    private var item: MediaItem?
    private var items = [MediaItem]()
    private var seasons = [SeasonItem]()
    private var relatedItems = [MediaItem]()
    private var casts = [CastItem]()
    private var aboutItems = [MediaItem()]
    private var infoItems = [MediaItem()]
    private var scrollContentOffsetY: CGFloat = .zero
    private var contentOffsetY: CGFloat = .zero
    private var mediaId = 0
    private var mediaType = ""
    private let group = DispatchGroup()
    private let main = DispatchQueue.main
    private var style: UIStatusBarStyle = .lightContent
    private let heartIcon: UIImage? = {
        let image = UIImage(systemName: "heart")?.withRenderingMode(.alwaysTemplate)
        image?.accessibilityLabel = "heartIcon"
        return image
    }()
    private let heartIconFill: UIImage? = {
        let image = UIImage(systemName: "heart.fill")?.withRenderingMode(.alwaysTemplate)
        image?.accessibilityLabel = "heartIconFill"
        return image
    }()
    private let notificationCenter = NotificationCenter.default
    private lazy var interaction = UIContextMenuInteraction(delegate: self)
    private var hasInitialDataBeenLoaded = false
    private var sharingItem: MediaItem?
    private var sharingItemImageView = CustomImageView()
    private var isDataFailedFetching = false
    private var isNetworkConnected = false

    // MARK: - Declarations (UI)
    fileprivate lazy var emptyView = CustomEmptyView(frame: collectionView.frame)

    fileprivate let titleView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()

    fileprivate let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 17)
        label.textColor = .label
        label.backgroundColor = .clear
        label.alpha = .zero
        return label
    }()

    fileprivate lazy var rightBarButtonItem: UIBarButtonItem = {
        let item = UIBarButtonItem(image: nil, style: .plain, target: self, action: #selector(handleRightBarButtonCustomView(_:)))
        item.image?.accessibilityLabel = "detailRightBarButtonImage"
        item.accessibilityIdentifier = "detailRightBarButtonItem"
        item.tintColor = .white
        return item
    }()

    // MARK: - Initialize
    init(mediaType type: String) {
        super.init(collectionViewLayout: DetailLayout(sectionProvider: { (indexPath, _) -> NSCollectionLayoutSection? in
            switch type {
                case MediaType.tv.rawValue:
                    switch indexPath {
                        case 0:
                            return DetailViewController.verticalSection()
                        case 1:
                            return DetailViewController.seasonSection()
                        case 2:
                            return DetailViewController.relatedSection()
                        case 3:
                            return DetailViewController.castSection()
                        case 4:
                            return DetailViewController.aboutSection()
                        default:
                            return DetailViewController.infoSection()
                    }
                default:
                    switch indexPath {
                        case 0:
                            return DetailViewController.verticalSection()
                        case 1:
                            return DetailViewController.relatedSection()
                        case 2:
                            return DetailViewController.castSection()
                        case 3:
                            return DetailViewController.aboutSection()
                        default:
                            return DetailViewController.infoSection()
                    }
            }
        }))
        mediaType = type
    }

    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkForNavigationBarStyle()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.tintColor = .systemBlue
        setNavigationBarTransparent(false)
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollContentOffsetY = scrollView.contentOffset.y
        checkForNavigationBarStyle()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return style
    }

    // MARK: - Configuration
    private static func verticalSection() -> NSCollectionLayoutSection {
        let headerHeight: CGFloat = UIDevice.current.hasNotch ? 700 : 565
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
        let item = NSCollectionLayoutSupplementaryItem(layoutSize: itemSize)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(100))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(headerHeight))
        let supplementaryItems = [NSCollectionLayoutBoundarySupplementaryItem.init(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)]
        section.boundarySupplementaryItems = supplementaryItems
        return section
    }

    private static func seasonSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
        let item = NSCollectionLayoutSupplementaryItem(layoutSize: itemSize)
        item.contentInsets = .init(top: .zero, leading: .zero, bottom: .zero, trailing: 16)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.72), heightDimension: .absolute(300))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(60))
        let supplementaryItems = [NSCollectionLayoutBoundarySupplementaryItem.init(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)]
        section.boundarySupplementaryItems = supplementaryItems
        section.orthogonalScrollingBehavior = .continuousGroupLeadingBoundary
        section.contentInsets = .init(top: .zero, leading: 32, bottom: 16, trailing: 16)
        return section
    }

    private static func relatedSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
        let item = NSCollectionLayoutSupplementaryItem(layoutSize: itemSize)
        item.contentInsets = .init(top: .zero, leading: .zero, bottom: .zero, trailing: 16)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.6), heightDimension: .absolute(120))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(60))
        let supplementaryItems = [NSCollectionLayoutBoundarySupplementaryItem.init(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)]
        section.boundarySupplementaryItems = supplementaryItems
        section.orthogonalScrollingBehavior = .continuousGroupLeadingBoundary
        section.contentInsets = .init(top: .zero, leading: 32, bottom: 32, trailing: 16)
        return section
    }

    private static func castSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
        let item = NSCollectionLayoutSupplementaryItem(layoutSize: itemSize)
        item.contentInsets = .init(top: .zero, leading: .zero, bottom: .zero, trailing: 16)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.3), heightDimension: .absolute(200))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(60))
        section.boundarySupplementaryItems = [NSCollectionLayoutBoundarySupplementaryItem.init(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)]
        section.orthogonalScrollingBehavior = .continuousGroupLeadingBoundary
        section.contentInsets = .init(top: .zero, leading: 32, bottom: 32, trailing: 16)
        return section
    }

    private static func aboutSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
        let item = NSCollectionLayoutSupplementaryItem(layoutSize: itemSize)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(200))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(60))
        let supplementaryItems = [NSCollectionLayoutBoundarySupplementaryItem.init(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)]
        section.boundarySupplementaryItems = supplementaryItems
        return section
    }

    private static func infoSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
        let item = NSCollectionLayoutSupplementaryItem(layoutSize: itemSize)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(300))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(60))
        let supplementaryItems = [NSCollectionLayoutBoundarySupplementaryItem.init(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)]
        section.boundarySupplementaryItems = supplementaryItems
        return section
    }

    private func setupView() {
        view.accessibilityIdentifier = "detailView"
        configureNavigationBar()
        configureCollectionView()
        configureDiffableDataSource()
        networkChangesObserver()
        notificationObserver()
        getData()
        configureRightBarButtonIcon()
    }

    private func configureNavigationBar() {
        navigationItem.largeTitleDisplayMode = .never
        setNavigationBarTransparent(true)
        configureTitleView()
        navigationItem.titleView = titleView
        navigationItem.rightBarButtonItem = rightBarButtonItem
    }

    private func setNavigationBarTransparent(_ transparent: Bool) {
        if transparent {
            let image = UIImage()
            navigationController?.navigationBar.setBackgroundImage(image, for: .default)
            navigationController?.navigationBar.shadowImage = image
        } else {
            navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
            navigationController?.navigationBar.shadowImage = nil
        }
    }

    private func configureCollectionView() {
        collectionView.backgroundColor = .systemBackground
        collectionView.alwaysBounceVertical = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.contentInsetAdjustmentBehavior = .never
        let tabBarHeight = tabBarController?.tabBar.frame.height ?? .zero
        collectionView.contentInset = .init(top: .zero, left: .zero, bottom: tabBarHeight, right: .zero)
        collectionView.backgroundView = emptyView

        contentOffsetY += UIDevice.current.hasNotch ? 610 : 502
    }

    private func configureTitleView() {
        titleView.addSubview(titleLabel)
        titleLabel.makeSuperView(withSafeArea: true)
    }

    private func configureDiffableDataSource() {
        makeCellRegistration()
        makeHeaderRegistration()
        configureCell()
        configureHeader()
    }

    private func applySnapshot() {
        var snapshot = Snapshot()

        if !items.isEmpty {
            snapshot.appendSections([.header])
            snapshot.appendItems(items, toSection: .header)
        }

        if !seasons.isEmpty {
            snapshot.appendSections([.season])
            snapshot.appendItems(seasons, toSection: .season)
        }

        if !relatedItems.isEmpty {
            snapshot.appendSections([.related])
            snapshot.appendItems(relatedItems, toSection: .related)
        }

        if !casts.isEmpty {
            snapshot.appendSections([.cast])
            snapshot.appendItems(casts, toSection: .cast)
        }

        if !aboutItems.isEmpty {
            snapshot.appendSections([.about])
            snapshot.appendItems(aboutItems, toSection: .about)

        }

        if !infoItems.isEmpty {
            snapshot.appendSections([.info])
            snapshot.appendItems(infoItems, toSection: .info)
        }

        collectionViewDiffableDataSource?.apply(snapshot)
    }

    private func checkForNavigationBarStyle() {
        if scrollContentOffsetY >= contentOffsetY {
            setNavigationBarTransparent(false)
            titleLabel.alpha = 1
            style = .default
            rightBarButtonItem.tintColor = .systemPink
            navigationController?.navigationBar.tintColor = .systemBlue
        } else {
            setNavigationBarTransparent(true)
            titleLabel.alpha = .zero
            style = .lightContent
            rightBarButtonItem.tintColor = .white
            navigationController?.navigationBar.tintColor = .white
        }
        setNeedsStatusBarAppearanceUpdate()
    }

    private func configureRightBarButtonIcon() {
        guard let item = item else {
            return
        }
        let favoritedItems = defaults.favoritedItems()
        let isFavoritedItem = favoritedItems.contains(where: { $0.id == item.id })
        if isFavoritedItem {
            setHeartIcon(isFill: true)
        } else {
            setHeartIcon(isFill: false)
        }
    }

    // MARK: - Methods
    func configure(item: MediaItem) {
        self.item = item
        guard let id = item.id else {
            return
        }
        mediaId = id
        titleLabel.text = item.title ?? item.name
    }

    private func getData() {
        getDetailItem()
        getRelatedItems()
        getCastItems()
        group.notify(queue: .main) {
            self.setData()
            self.hasInitialDataBeenLoaded = true
            self.collectionView.alwaysBounceVertical = true
        }
    }

    private func getDetailItem() {
        group.enter()
        Networking.manager.getDetail(mediaType: mediaType, id: mediaId) { [weak self] (result) in
            switch result {
                case .success(let item):
                    self?.success(item: item)
                case .failure(let error):
                    self?.failed(error: error)
            }
            self?.group.leave()
        }
    }

    private func getSeasonItems(item: MediaItem) {
        if mediaType == MediaType.tv.rawValue {
            guard let seasons = item.seasons else {
                return
            }
            self.seasons.append(contentsOf: seasons)
        }
    }

    private func getRelatedItems() {
        group.enter()
        Networking.manager.getRelated(mediaType: mediaType, mediaId: mediaId) { [weak self] (result) in
            switch result {
                case .success(let items):
                    if !items.isEmpty {
                        self?.relatedItems.append(contentsOf: items)
                    } else {
                        self?.relatedItems.append(MediaItem())
                    }
                    self?.isDataFailedFetching = false
                case .failure(let error):
                    self?.failed(error: error)
            }
            self?.group.leave()
        }
    }

    private func getCastItems() {
        group.enter()
        Networking.manager.getCasts(mediaType: mediaType, id: mediaId) { [weak self] (result) in
            switch result {
                case .success(let items):
                    if !items.isEmpty {
                        self?.casts.append(contentsOf: items)
                    } else {
                        self?.casts.append(CastItem())
                    }
                    self?.isDataFailedFetching = false
                case .failure(let error):
                    self?.failed(error: error)
            }
            self?.group.leave()
        }
    }

    private func success(item: MediaItem) {
        if let _ = item.id {
            self.item = item
        }
        guard let item = self.item else {
            return
        }
        items.append(item)
        getSeasonItems(item: item)
        isDataFailedFetching = false
    }

    private func failed(error: Error) {
        isDataFailedFetching = true
        Errors.error(in: FileIndentifier.Detail.rawValue, at: #line, error: error)
    }

    private func setData() {
        main.async {
            self.applySnapshot()
            if self.isDataFailedFetching {
                if self.isNetworkConnected {
                    self.emptyView.configure(isLoading: false, message: "You're not connectedt to the internet.")
                } else {
                    self.emptyView.configure(isLoading: false, message: "Something went wrong. Please try again later.")
                }
            } else {
                self.emptyView.configure(isLoading: false, message: nil)
            }
        }
    }

    private func setHeartIcon(isFill fill: Bool) {
        if fill {
            rightBarButtonItem.image = heartIconFill
        } else {
            rightBarButtonItem.image = heartIcon
        }
    }

    private func presentOverviewViewController(overview text: String) {
        let controller = OverviewViewController()
        guard let title = titleLabel.text else {
            return
        }
        controller.configure(title: title, overview: text)
        let navigationController = UINavigationController(rootViewController: controller)
        present(navigationController, animated: true)
    }

    private func presentDetailListViewcontroller(title: String) {
        let layout = UICollectionViewFlowLayout()
        let controller = DetailListViewController(collectionViewLayout: layout)
        controller.configure(section: title, items: relatedItems, searchText: nil, mediaId: mediaId, mediaType: mediaType)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func presentSeasonViewController(item: SeasonItem) {
        if mediaType == MediaType.tv.rawValue {
            let layout = UICollectionViewFlowLayout()
            let controller = SeasonViewController(collectionViewLayout: layout)
            guard let name = item.name else {
                return
            }
            guard let season = item.season_number else {
                return
            }
            controller.configure(tvId: mediaId, seasonId: season, seasonName: name)
            navigationController?.pushViewController(controller, animated: true)
        }
    }

    private func presentPersonDetailViewController(item: CastItem) {
        guard let _ = item.id else {
            return
        }
        let controller = PersonDetailViewController()
        controller.configure(item: item)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func updateFavoritedItem() {
        guard var item = item else {
            return
        }
        let favoritedItems = defaults.favoritedItems()
        item.setMediaType(value: mediaType)
        let isFavoritedItem = favoritedItems.contains(where: { $0.id == item.id })
        if isFavoritedItem {
            defaults.unfavoriteItem(item: item)
            setHeartIcon(isFill: false)
        } else {
            defaults.favoritedItem(item: item)
            setHeartIcon(isFill: true)
        }
    }

    private func shareAction(item: MediaItem) -> UIAction {
        sharingItem = item
        let path = item.backdrop_path ?? item.poster_path
        sharingItemImageView.configure(path: path)
        let action = UIAction(title: "Share", image: UIImage(systemName: "square.and.arrow.up")) { [unowned self] (_) in
            let items: [Any] = [
                self
            ]
            let controller = UIActivityViewController(activityItems: items, applicationActivities: nil)
            controller.popoverPresentationController?.sourceView = view
            controller.excludedActivityTypes = [
                .addToReadingList,
                .assignToContact,
                .markupAsPDF,
                .openInIBooks,
                .print,
                .postToFlickr,
                .postToVimeo,
                .saveToCameraRoll,
            ]
            self.present(controller, animated: true)
        }
        return action
    }

    private func addOrDeleteAction(item: MediaItem) -> UIAction {
        let favoritedItems = defaults.favoritedItems()
        let isFavoritedItem = favoritedItems.contains(where: { $0.id == item.id })
        if isFavoritedItem {
            let action = UIContextMenuActions.deleteAction(item: item) { [weak self] in
                // Post a notification to notify FavoriteViewController the changes
                self?.postFavoriteItemChangesNotification()
            }
            return action
        } else {
            let action = UIContextMenuActions.addAction(item: item, selectedMediaType: mediaType) { [weak self] in
                // Post a notification to notify FavoriteViewController the changes
                self?.postFavoriteItemChangesNotification()
            }
            return action
        }
    }

    private func createContextMenu(indexPath: IndexPath) -> UIMenu {
        guard let item = collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) as? MediaItem else {
            return UIMenu()
        }
        guard let snapshot = collectionViewDiffableDataSource?.snapshot() else {
            return UIMenu()
        }
        let section = snapshot.sectionIdentifier(containingItem: item)
        switch section {
            case .related:
                guard let _ = item.id else {
                    return UIMenu()
                }
            default:
                return UIMenu()
        }
        let menu = UIMenu(children: [
            UIContextMenuActions.copyAction(item: item),
            shareAction(item: item),
            addOrDeleteAction(item: item),
        ])
        return menu
    }

    private func postFavoriteItemChangesNotification() {
        notificationCenter.post(name: favoritedItemChangesNotification, object: nil)
        let newFavoriteCount = defaults.newFavoriteItemCount()
        if newFavoriteCount == .zero {
            mainTabBarController?.viewControllers?[1].tabBarItem.badgeValue = nil
        } else {
            mainTabBarController?.viewControllers?[1].tabBarItem.badgeValue = "\(newFavoriteCount)"
        }
    }

    private func notificationObserver() {
        notificationCenter.addObserver(self, selector: #selector(observeItemsChanges(_:)), name: unfavoritedItemNotification, object: nil)
    }

    private func networkChangesObserver() {
        let monitor = NWPathMonitor()
        monitor.startMonitoringNetworkChanges { [weak self] (status) in
            switch status {
                case .satisfied:
                    self?.isNetworkConnected = true
                    self?.isNetworkSatisfied(true)
                case .unsatisfied:
                    self?.isNetworkConnected = false
                    self?.isNetworkSatisfied(false)
                case .requiresConnection:
                    print("Requires Connection")
                default:
                    return
            }
        }
    }

    private func isNetworkSatisfied(_ satisfied: Bool) {
        main.async {
            if !self.hasInitialDataBeenLoaded {
                if satisfied {
                    self.emptyView.configure(isLoading: true, message: nil)
                } else {
                    self.emptyView.configure(isLoading: false, message: "No Data Available.")
                }
            }
        }
    }

    // MARK: - Objc Methods
    @objc private func handleRightBarButtonCustomView(_ sender: UIButton?) {
        updateFavoritedItem()
        postFavoriteItemChangesNotification()
    }

    @objc private func observeItemsChanges(_ sender: NotificationCenter?) {
        configureRightBarButtonIcon()
    }
    
    // MARK: - Deinit
    deinit {
        setNavigationBarTransparent(false)
        notificationCenter.removeObserver(self)
        print("Deinit \(FileIndentifier.Detail.rawValue)")
    }

    // MARK: - //
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Extension
extension DetailViewController {
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let snapshot = collectionViewDiffableDataSource?.snapshot() else {
            return
        }
        guard let item = collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
            return
        }
        let section = snapshot.sectionIdentifier(containingItem: item)
        switch section {
            case .related:
                let controller = DetailViewController(mediaType: mediaType)
                guard let item = item as? MediaItem else {
                    return
                }
                guard let _ = item.id else {
                    return
                }
                controller.configure(item: item)
                navigationController?.pushViewController(controller, animated: true)
            case .about:
                guard let item = self.item else {
                    return
                }
                guard let overview = item.overview else {
                    return
                }
                if overview.count == .zero {
                    return
                }
                presentOverviewViewController(overview: overview)
            case .season:
                guard let item = item as? SeasonItem else {
                    return
                }
                presentSeasonViewController(item: item)
            case .cast:
                guard let item = item as? CastItem else {
                    return
                }
                presentPersonDetailViewController(item: item)
            default:
                return
        }
    }

    override func collectionView(_ collectionView: UICollectionView, contextMenuConfigurationForItemAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        .init(identifier: nil, previewProvider: nil) { [weak self] (_) -> UIMenu? in
            self?.createContextMenu(indexPath: indexPath)
        }
    }
}

extension DetailViewController: DetailHeaderCellProtocol {
    func didPressOverviewLabel(overview text: String) {
        presentOverviewViewController(overview: text)
    }
}

extension DetailViewController: SectionHeaderProtocol {
    func didPressSeeAllButton(section title: String) {
        presentDetailListViewcontroller(title: title)
    }
}

extension DetailViewController: UIContextMenuInteractionDelegate {
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        nil
    }
}

extension DetailViewController: UIActivityItemSource {
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        // Let UIKit knows the type of data we wanna share
        guard let title = sharingItem?.title ?? sharingItem?.name else {
            return ""
        }
        return title
    }

    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        // The custom body of what we wanna share. For instance, this will be the custom message for the mail's body or twitter's.
        let title = (sharingItem?.title ?? sharingItem?.name) ?? ""
        let description = sharingItem?.overview ?? ""
        guard let dummyLink = URL(string: "www.dummylink.com") else {
            return title + "\n\n" + description
        }

        switch activityType {
            case UIActivity.ActivityType.mail:
                let message = title + "\n\n" + description + "\n\n" + "\(dummyLink)"
                return message
            default:
                let url = "\(dummyLink)"
                return url
        }
    }

    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        // The custom subject of what we wanna share in mail
        guard let subject = sharingItem?.title ?? sharingItem?.name else {
            return "The subject"
        }
        return subject
    }

    func activityViewControllerLinkMetadata(_ activityViewController: UIActivityViewController) -> LPLinkMetadata? {
        let metadata = LPLinkMetadata()
        guard let image = sharingItemImageView.image else {
            return metadata
        }
        metadata.iconProvider = .init(object: image)
        guard let item = sharingItem else {
            return metadata
        }
        let title = item.title ?? item.name
        metadata.title = title
        return metadata
    }
}

extension DetailViewController {
    private func makeCellRegistration() {
        headerCellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            guard let _ = self else {
                return
            }
            cell.delegate = self
            cell.configure(item: item)
        })
        seasonCellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            guard let _ = self else {
                return
            }
            cell.configure(item: item)
        })
        relatedCellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            guard let self = self else {
                return
            }
            cell.addInteraction(self.interaction)
            cell.configure(item: item)
        })
        personCellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            guard let _ = self else {
                return
            }
            cell.configure(item: item)
        })
        aboutCellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            cell.configure(item: self?.item)
        })
        infoCellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            cell.configure(item: self?.item)
        })
    }

    private func makeHeaderRegistration() {
        headerRegistration = .init(elementKind: UICollectionView.elementKindSectionHeader, handler: { [weak self] (header, kind, indexPath) in
            guard let item = self?.collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
                return
            }
            header.configure(item: item)
        })

        sectionHeaderRegistration = .init(elementKind: UICollectionView.elementKindSectionHeader, handler: { [weak self] (header, kind, indexPath) in

            guard let snapshot = self?.collectionViewDiffableDataSource?.snapshot() else {
                return
            }

            guard let item = self?.collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
                return
            }

            let section = snapshot.sectionIdentifier(containingItem: item)

            switch section {
                case .season:
                    header.configure(title: DetailSection.season.rawValue)
                    header.showSeeAllButton(false)
                case .related:
                    header.configure(title: DetailSection.related.rawValue)
                    guard let _ = self?.relatedItems.first?.id else {
                        return
                    }
                    header.delegate = self
                    if self?.relatedItems.count ?? .zero > 1 {
                        header.showSeeAllButton()
                    } else {
                        header.showSeeAllButton(false)
                    }
                case .cast:
                    header.configure(title: DetailSection.cast.rawValue)
                    header.showSeeAllButton(false)
                case .about:
                    header.configure(title: DetailSection.about.rawValue)
                    header.showSeeAllButton(false)
                case .info:
                    header.configure(title: DetailSection.info.rawValue)
                    header.showSeeAllButton(false)
                default:
                    return
            }
        })
    }

    private func configureCell() {
        guard let headerCellRegistration = headerCellRegistration else {
            return
        }
        guard let seasonCellRegistration = seasonCellRegistration else {
            return
        }
        guard let relatedCellRegistration = relatedCellRegistration else {
            return
        }
        guard let personCellRegistration = personCellRegistration else {
            return
        }
        guard let aboutCellRegistration = aboutCellRegistration else {
            return
        }
        guard let infoCellRegistration = infoCellRegistration else {
            return
        }

        collectionViewDiffableDataSource = .init(collectionView: collectionView, cellProvider: { [weak self] (collectionView, indexPath, item) -> UICollectionViewCell? in

            guard let _ = self else {
                return UICollectionViewCell()
            }

            guard let snapshot = self?.collectionViewDiffableDataSource?.snapshot() else {
                return UICollectionViewCell()
            }

            let section = snapshot.sectionIdentifier(containingItem: item)

            let mediaItem = item as? MediaItem
            let seasonItem = item as? SeasonItem
            let castItem = item as? CastItem

            switch section {
                case .header:
                    let cell = collectionView.dequeueConfiguredReusableCell(using: headerCellRegistration, for: indexPath, item: mediaItem)
                    return cell
                case .season:
                    let cell = collectionView.dequeueConfiguredReusableCell(using: seasonCellRegistration, for: indexPath, item: seasonItem)
                    return cell
                case .related:
                    let cell = collectionView.dequeueConfiguredReusableCell(using: relatedCellRegistration, for: indexPath, item: mediaItem)
                    return cell
                case .cast:
                    let cell = collectionView.dequeueConfiguredReusableCell(using: personCellRegistration, for: indexPath, item: castItem)
                    return cell
                case .about:
                    let cell = collectionView.dequeueConfiguredReusableCell(using: aboutCellRegistration, for: indexPath, item: mediaItem)
                    return cell
                case .info:
                    let cell = collectionView.dequeueConfiguredReusableCell(using: infoCellRegistration, for: indexPath, item: mediaItem)
                    return cell
                default:
                    return UICollectionViewCell()
            }
        })
    }

    private func configureHeader() {
        guard let headerRegistration = headerRegistration else {
            return
        }

        guard let sectionHeaderRegistration = sectionHeaderRegistration else {
            return
        }

        collectionViewDiffableDataSource?.supplementaryViewProvider = .some({ [weak self] (collectionView, kind, indexPath) -> UICollectionReusableView? in

            guard let _ = self else {
                return UICollectionViewCell()
            }

            guard let snapshot = self?.collectionViewDiffableDataSource?.snapshot() else {
                return UICollectionViewCell()
            }

            guard let item = self?.collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
                return UICollectionViewCell()
            }

            let section = snapshot.sectionIdentifier(containingItem: item)

            switch section {
                case .header:
                    let header = collectionView.dequeueConfiguredReusableSupplementary(using: headerRegistration, for: indexPath)
                    return header
                default:
                    let header = collectionView.dequeueConfiguredReusableSupplementary(using: sectionHeaderRegistration, for: indexPath)
                    return header
            }
        })
    }
}
