//
//  DetailListViewController.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/28/20.
//

import SwiftUI
import LinkPresentation

fileprivate typealias DetailListFooterRegistration = UICollectionView.SupplementaryRegistration<DetailListFooterView>
fileprivate typealias DetailListCellRegistration = UICollectionView.CellRegistration<DetailListCell, MediaItem>
fileprivate typealias DetailListDiffableDataSource = UICollectionViewDiffableDataSource<Section, MediaItem>
fileprivate typealias Snapshot = NSDiffableDataSourceSnapshot<Section, MediaItem>

fileprivate enum Section {
    case main
}

// MARK: - Class
final class DetailListViewController: UICollectionViewController {
    
    // MARK: - Declarations (Properties)
    private var footerRegistration: DetailListFooterRegistration?
    private var cellRegistration: DetailListCellRegistration?
    private var collectionViewDiffableDataSource: DetailListDiffableDataSource?
    private var section: String?
    private var items: [MediaItem]?
    private var searchText: String?
    private var homeSectionType: HomeSectionType? = .none
    private var mediaId: Int = .zero
    private var mediaType = ""
    private var page = 1
    private var isPaging = false
    private var isDonePaging = false
    private let footerKind = UICollectionView.elementKindSectionFooter
    private let group = DispatchGroup()
    private let main = DispatchQueue.main
    private var isPagingAvailable = false
    private let notificationCenter = NotificationCenter.default
    private lazy var interaction = UIContextMenuInteraction(delegate: self)
    private var sharingItem: MediaItem?
    private var sharingItemImageView = CustomImageView()
    
    // MARK: - Declarations (UI)
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.tintColor = .systemBlue
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = nil
    }

    // MARK: - Configuration
    private func setupView() {
        view.accessibilityIdentifier = "detailListView"
        configureNavigationBar()
        configureCollectionView()
        configureDiffableDataSource()
    }

    private func configureNavigationBar() {
        navigationItem.largeTitleDisplayMode = .always
    }
    
    private func configureCollectionView() {
        collectionView.backgroundColor = .systemBackground
        collectionView.alwaysBounceVertical = true
    }

    private func configureDiffableDataSource() {
        makeCellRegistration()
        makeFooterRegistration()
        configureCell()
        configureFooter()
        applySnapshot()
    }

    private func applySnapshot() {
        var snapshot = Snapshot()
        guard let items = items else {
            return
        }
        snapshot.appendSections([.main])
        snapshot.appendItems(items, toSection: .main)
        collectionViewDiffableDataSource?.apply(snapshot)
    }
    
    // MARK: - Methods
    func configure(section: String, items: [MediaItem], searchText query: String?, mediaId: Int = .zero, mediaType type: String, homeSectionType: HomeSectionType? = .none) {
        navigationItem.title = section
        self.section = section
        self.items = items
        searchText = query
        self.mediaId = mediaId
        mediaType = type

        if let homeSectionType = homeSectionType {
            self.homeSectionType = homeSectionType
        }
        if self.mediaId != .zero || self.homeSectionType != .none || searchText != nil && section != SearchSection.persons.rawValue {
            isPagingAvailable = true
        } else {
            isPagingAvailable = false
        }
    }

    private func getHomeSectionItems() {
        guard let homeSectionType = homeSectionType else {
            return
        }
        group.enter()
        Networking.manager.getHomeResults(of: homeSectionType, mediaType: mediaType, page: page) { [weak self] (result) in
            switch result {
                case .success(let items):
                    self?.success(items: items)
                case .failure(let error):
                    Errors.error(in: FileIndentifier.DetailList.rawValue, at: #line, error: error)
            }
            self?.group.leave()
        }
        groupNotifyMainThread()
    }

    private func getSearchedItems(query text: String) {
        group.enter()
        Networking.manager.getSearchResults(search: text, page: page, mediaType: mediaType) { [weak self] (result) in
            switch result {
                case .success(let items):
                    self?.success(items: items)
                case .failure(let error):
                    Errors.error(in: FileIndentifier.DetailList.rawValue, at: #line, error: error)
            }
            self?.group.leave()
        }
        groupNotifyMainThread()
    }

    private func getRelatedResults() {
        group.enter()
        Networking.manager.getRelated(mediaType: mediaType, mediaId: mediaId, page: page) { [weak self] (result) in
            switch result {
                case .success(let items):
                    self?.success(items: items)
                case .failure(let error):
                    Errors.error(in: FileIndentifier.DetailList.rawValue, at: #line, error: error)
            }
            self?.group.leave()
        }
        groupNotifyMainThread()
    }

    private func success(items: [MediaItem]) {
        isPaging = false

        if items.isEmpty {
            isDonePaging = true
            return
        }

        self.items?.append(contentsOf: items)
    }

    private func groupNotifyMainThread() {
        group.notify(queue: .main) {
            self.applySnapshot()
        }
    }

    private func fetchResults() {
        if !isPaging && !isDonePaging {
            if mediaId != .zero {
                isPaging = true
                page += 1
                getRelatedResults()
            } else if homeSectionType != .none {
                isPaging = true
                page += 1
                getHomeSectionItems()
            } else {
                guard let searchText = searchText else {
                    return
                }
                if searchText.containsText() {
                    isPaging = true
                    page += 1
                    getSearchedItems(query: searchText)
                }
            }
        }
    }

    private func shareAction(item: MediaItem) -> UIAction {
        sharingItem = item
        let path = item.backdrop_path ?? item.poster_path
        sharingItemImageView.configure(path: path)
        let action = UIAction(title: "Share", image: UIImage(systemName: "square.and.arrow.up")) { [unowned self] (_) in
            let items: [Any] = [
                self
            ]
            let controller = UIActivityViewController(activityItems: items, applicationActivities: nil)
            controller.popoverPresentationController?.sourceView = view
            controller.excludedActivityTypes = [
                .addToReadingList,
                .assignToContact,
                .markupAsPDF,
                .openInIBooks,
                .print,
                .postToFlickr,
                .postToVimeo,
                .saveToCameraRoll,
            ]
            self.present(controller, animated: true)
        }
        return action
    }

    private func addOrDeleteAction(item: MediaItem) -> UIAction {
        let favoritedItems = defaults.favoritedItems()
        let isFavoritedItem = favoritedItems.contains(where: { $0.id == item.id })
        if isFavoritedItem {
            let action = UIContextMenuActions.deleteAction(item: item) { [weak self] in
                // Post a notification to notify FavoriteViewController the changes
                self?.postFavoriteItemChangesNotification()
            }
            return action
        } else {
            let action = UIContextMenuActions.addAction(item: item) { [weak self] in
                // Post a notification to notify FavoriteViewController the changes
                self?.postFavoriteItemChangesNotification()
            }
            return action
        }
    }

    private func createContextMenu(indexPath: IndexPath) -> UIMenu {
        guard let item = collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
            return UIMenu()
        }
        let menu = UIMenu(children: [
            UIContextMenuActions.copyAction(item: item),
            shareAction(item: item),
            addOrDeleteAction(item: item),
        ])
        return menu
    }

    private func postFavoriteItemChangesNotification() {
        notificationCenter.post(name: favoritedItemChangesNotification, object: nil)
        let newFavoriteCount = defaults.newFavoriteItemCount()
        if newFavoriteCount == .zero {
            mainTabBarController?.viewControllers?[1].tabBarItem.badgeValue = nil
        } else {
            mainTabBarController?.viewControllers?[1].tabBarItem.badgeValue = "\(newFavoriteCount)"
        }
    }
    
    // MARK: - Objc Methods
    
    // MARK: - Deinit
    deinit {
        notificationCenter.removeObserver(self)
        print("Deinit \(FileIndentifier.DetailList.rawValue)")
    }
}

// MARK: - Extension
extension DetailListViewController {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let padding: CGFloat = 32
        let insets = UIEdgeInsets(top: .zero, left: padding, bottom: padding, right: padding)
        return insets
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        1
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        46
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if !isPagingAvailable || isDonePaging {
            return .zero
        }
        let width = collectionView.frame.width
        let height: CGFloat = 100
        let size = CGSize(width: width, height: height)
        return size
    }

    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if !isPagingAvailable || isDonePaging {
            return
        }
        guard let items = items else {
            return
        }
        let lastItem = items.count - 1
        let item = indexPath.item
        if item == lastItem {
            fetchResults()
        }
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let section = section else {
            return
        }
        guard let item = collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
            Errors.error(in: FileIndentifier.DetailList.rawValue, at: #line)
            return
        }
        switch section {
            case SearchSection.persons.rawValue:
                let controller = PersonDetailViewController()
                controller.configure(item: item)
                navigationController?.pushViewController(controller, animated: true)
            default:
                let controller = DetailViewController(mediaType: mediaType)
                controller.configure(item: item)
                navigationController?.pushViewController(controller, animated: true)
        }
    }

    override func collectionView(_ collectionView: UICollectionView, contextMenuConfigurationForItemAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        .init(identifier: nil, previewProvider: nil) { [weak self] (_) -> UIMenu? in
            if self?.navigationItem.title == SearchSection.persons.rawValue {
                return nil
            }
            return self?.createContextMenu(indexPath: indexPath)
        }
    }
}

// Delegate Flow Layout
extension DetailListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width: CGFloat = .zero
        var height: CGFloat = .zero
        guard let items = items else {
            return .zero
        }
        let item = items[indexPath.item]
        switch item.media_type {
            case MediaType.person.rawValue:
                width = (collectionView.frame.width - 64 - 32 - 2) / 3
                height = collectionView.frame.height / 4
            default:
                width = (collectionView.frame.width - 64 - 16) / 2
                height = collectionView.frame.height / 8
        }
        let size = CGSize(width: width, height: height)
        return size
    }
}

extension DetailListViewController: UIContextMenuInteractionDelegate {
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        nil
    }
}

extension DetailListViewController: UIActivityItemSource {
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        // Let UIKit knows the type of data we wanna share
        guard let title = sharingItem?.title ?? sharingItem?.name else {
            return ""
        }
        return title
    }

    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        // The custom body of what we wanna share. For instance, this will be the custom message for the mail's body or twitter's.
        let title = (sharingItem?.title ?? sharingItem?.name) ?? ""
        let description = sharingItem?.overview ?? ""
        guard let dummyLink = URL(string: "www.dummylink.com") else {
            return title + "\n\n" + description
        }

        switch activityType {
            case UIActivity.ActivityType.mail:
                let message = title + "\n\n" + description + "\n\n" + "\(dummyLink)"
                return message
            default:
                let url = "\(dummyLink)"
                return url
        }
    }

    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        // The custom subject of what we wanna share in mail
        guard let subject = sharingItem?.title ?? sharingItem?.name else {
            return "The subject"
        }
        return subject
    }

    func activityViewControllerLinkMetadata(_ activityViewController: UIActivityViewController) -> LPLinkMetadata? {
        let metadata = LPLinkMetadata()
        guard let image = sharingItemImageView.image else {
            return metadata
        }
        metadata.iconProvider = .init(object: image)
        guard let item = sharingItem else {
            return metadata
        }
        let title = item.title ?? item.name
        metadata.title = title
        return metadata
    }
}

extension DetailListViewController {
    private func makeFooterRegistration() {
        footerRegistration = .init(elementKind: footerKind, handler: { [weak self] (footer, kind, indexPath) in
            guard let _ = self else {
                return
            }
        })
    }

    private func makeCellRegistration() {
        cellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            guard let _ = self else {
                return
            }
            cell.configure(item: item)
        })
    }

    private func configureCell() {
        guard let registration = cellRegistration else {
            return
        }
        collectionViewDiffableDataSource = .init(collectionView: collectionView, cellProvider: { (collectionView, indexPath, item) -> UICollectionViewCell? in
            let cell = collectionView.dequeueConfiguredReusableCell(using: registration, for: indexPath, item: item)
            return cell
        })
    }

    private func configureFooter() {
        guard let registration = footerRegistration else {
            return
        }
        collectionViewDiffableDataSource?.supplementaryViewProvider = .some({ [weak self] (collectionView, kind, indexPath) -> UICollectionReusableView? in
            guard let _ = self else {
                return UICollectionReusableView()
            }
            let footer = collectionView.dequeueConfiguredReusableSupplementary(using: registration, for: indexPath)
            return footer
        })
    }
}

// MARK: - Previews
struct DetailListContentView: UIViewControllerRepresentable {
    func makeUIViewController(context: Context) -> UIViewController {
        let layout = UICollectionViewFlowLayout()
        let controller = DetailListViewController(collectionViewLayout: layout)
        return controller
    }

    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {}

    typealias UIViewControllerType = UIViewController
}

struct DetailListContentView_Previews: PreviewProvider {
    static var previews: some View {
        DetailListContentView()
            .edgesIgnoringSafeArea(.all)
    }
}
