//
//  OverviewViewController.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/17/20.
//

import UIKit

// MARK: - Class
class OverviewViewController: UIViewController {
    
    // MARK: - Declarations (Properties)
    
    // MARK: - Declarations (UI)
    fileprivate lazy var rightBarButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(handleRightBarButtonItem(_:)))

    fileprivate let textView: UITextView = {
        let view = UITextView()
        view.backgroundColor = .systemBackground
        view.font = .systemFont(ofSize: 17, weight: .regular)
        view.isEditable = false
        view.textColor = .label
        view.textAlignment = .natural
        view.showsVerticalScrollIndicator = false
        return view
    }()

    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: - Configuration
    private func setupView() {
        view.accessibilityIdentifier = "overviewView"
        view.backgroundColor = .systemBackground
        configureNavigationBar()
        configureTextView()
    }

    private func configureNavigationBar() {
        navigationItem.rightBarButtonItem = rightBarButton
    }

    private func configureTextView() {
        view.addSubview(textView)
        let padding: CGFloat = 16
        let constants = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        textView.makeView(top: view.safeAreaLayoutGuide.topAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, constants: constants)
    }
    
    // MARK: - Methods
    func configure(title: String, overview text: String) {
        navigationItem.title = title
        textView.text = text
    }
    
    // MARK: - Objc Methods
    @objc private func handleRightBarButtonItem(_ sender: UIBarButtonItem?) {
        dismiss(animated: true)
    }
    
    // MARK: - Deinit
    deinit {
        print("Deinit \(FileIndentifier.Overview.rawValue)")
    }
    
}

// MARK - Extension
extension OverviewViewController {}
