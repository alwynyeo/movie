//
//  EpisodeViewController.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/26/20.
//

import SwiftUI

fileprivate typealias EpisodeHeaderRegistration = UICollectionView.SupplementaryRegistration<DetailHeader>
fileprivate typealias EpisodeSectionHeaderRegistration = UICollectionView.SupplementaryRegistration<SectionHeader>
fileprivate typealias EpisodeHeaderCellRegistration = UICollectionView.CellRegistration<DetailHeaderCell, EpisodeItem>
fileprivate typealias EpisodePersonCellRegistration = UICollectionView.CellRegistration<DetailPersonCell, CastItem>
fileprivate typealias EpisodeAboutCellRegistration = UICollectionView.CellRegistration<DetailAboutCell, EpisodeItem>
fileprivate typealias EpisodeInfoCellRegistration = UICollectionView.CellRegistration<DetailInfoCell, EpisodeItem>
fileprivate typealias EpisodeDiffableDataSource = UICollectionViewDiffableDataSource<Section, AnyHashable>
fileprivate typealias Snapshot = NSDiffableDataSourceSnapshot<Section, AnyHashable>

fileprivate enum Section: String {
    case header
    case cast = "Guest Stars"
    case about = "About"
    case info = "Information"
}

extension Section: CaseIterable {}

// MARK: - Class
class EpisodeViewController: UICollectionViewController {
    
    // MARK: - Declarations (Properties)
    private var headerRegistration: EpisodeHeaderRegistration?
    private var sectionHeaderRegistration: EpisodeSectionHeaderRegistration?
    private var headerCellRegistration: EpisodeHeaderCellRegistration?
    private var personCellRegistration: EpisodePersonCellRegistration?
    private var aboutCellRegistration: EpisodeAboutCellRegistration?
    private var infoCellRegistration: EpisodeInfoCellRegistration?
    private var collectionViewDiffableDataSource: EpisodeDiffableDataSource?
    private var item: EpisodeItem? {
        didSet {
            setData()
        }
    }
    private var items = [EpisodeItem]()
    private var casts = [CastItem]()
    private var aboutItems = [EpisodeItem()]
    private var infoItems = [EpisodeItem()]
    private let navigationBarBorder = "hidesShadow"
    private var contentOffsetY: CGFloat = 300
    private let headerKind = UICollectionView.elementKindSectionHeader

    // MARK: - Declarations (UI)
    fileprivate lazy var emptyView = CustomEmptyView(frame: collectionView.frame)

    fileprivate let titleView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()

    fileprivate let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 17)
        label.textColor = .label
        label.backgroundColor = .clear
        label.alpha = .zero
        return label
    }()

    // MARK: - Initialize
    init() {
        super.init(collectionViewLayout: EpisodeLayout.layout())
    }

    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationBarBorder(true)
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let headerSize: CGFloat = UIDevice.current.hasNotch ? 92 : 64
        let scrollContentOffsetY = scrollView.contentOffset.y + headerSize
        if scrollContentOffsetY >= contentOffsetY {
            titleLabel.alpha = 1
        } else {
            titleLabel.alpha = .zero
        }
    }
    
    // MARK: - Configuration
    private func setupView() {
        view.accessibilityIdentifier = "episodeDetailView"
        configureNavigationBar()
        configureCollectionView()
        configurerDiffableDataSource()
    }

    private func configureNavigationBar() {
        setNavigationBarBorder(true)
        navigationItem.largeTitleDisplayMode = .never
        configureTitleView()
        navigationItem.titleView = titleView
    }

    private func setNavigationBarBorder(_ isHidden: Bool) {
        if isHidden {
            navigationController?.navigationBar.barTintColor = .systemBackground
            navigationController?.navigationBar.setValue(true, forKey: navigationBarBorder)
        } else {
            navigationController?.navigationBar.barTintColor = nil
            navigationController?.navigationBar.setValue(false, forKey: navigationBarBorder)
        }
    }

    private func configureTitleView() {
        titleView.addSubview(titleLabel)
        titleLabel.makeSuperView(withSafeArea: true)
    }
    
    private func configureCollectionView() {
        collectionView.backgroundColor = .systemBackground
        collectionView.alwaysBounceVertical = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.backgroundView = emptyView
    }

    private func configurerDiffableDataSource() {
        makeCellRegistration()
        makeHeaderRegistration()
        configureCell()
        configureHeader()
        applySnapshot()
        emptyView.configure(isLoading: false, message: nil)
    }

    private func applySnapshot() {
        var snapshot = Snapshot()

        snapshot.appendSections(Section.allCases)
        snapshot.appendItems(items, toSection: .header)
        snapshot.appendItems(casts, toSection: .cast)
        snapshot.appendItems(aboutItems, toSection: .about)
        snapshot.appendItems(infoItems, toSection: .info)

        collectionViewDiffableDataSource?.apply(snapshot)
    }

    // MARK: - Methods
    func configure(item: EpisodeItem) {
        self.item = item
        titleLabel.text = item.name
    }

    private func setData() {
        guard let item = item else {
            return
        }
        items.append(item)
        guard let guestStars = item.guest_stars else {
            return
        }
        let casts = guestStars.isEmpty ? [CastItem()] : guestStars
        self.casts.append(contentsOf: casts)
    }

    private func presentOverviewViewController(overview text: String) {
        let controller = OverviewViewController()
        guard let title = titleLabel.text else {
            return
        }
        controller.configure(title: title, overview: text)
        let navigationController = UINavigationController(rootViewController: controller)
        present(navigationController, animated: true)
    }
    
    // MARK: - Objc Methods
    
    // MARK: - Deinit
    deinit {
        
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Extension
extension EpisodeViewController {
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let snapshot = collectionViewDiffableDataSource?.snapshot() else {
            return
        }
        guard let item = collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
            return
        }
        let section = snapshot.sectionIdentifier(containingItem: item)
        switch section {
            case .about:
                guard let item = self.item else {
                    return
                }
                guard let overview = item.overview else {
                    return
                }
                presentOverviewViewController(overview: overview)
            default:
                return
        }
    }
}

extension EpisodeViewController: DetailHeaderCellProtocol {
    func didPressOverviewLabel(overview text: String) {
        presentOverviewViewController(overview: text)
    }
}

extension EpisodeViewController {
    private func makeCellRegistration() {
        headerCellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            cell.delegate = self
            cell.configure(item: self?.item)
        })
        personCellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            guard let _ = self else {
                return
            }
            cell.configure(item: item)
        })
        aboutCellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            cell.configure(item: self?.item)
        })
        infoCellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            cell.configure(item: self?.item)
        })
    }

    private func makeHeaderRegistration() {
        headerRegistration = .init(elementKind: headerKind, handler: { [weak self] (header, kind, indexPath) in
            guard let item = self?.collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
                return
            }
            header.configure(item: item)
        })

        sectionHeaderRegistration = .init(elementKind: headerKind, handler: { [weak self] (header, kind, indexPath) in
            guard let snapshot = self?.collectionViewDiffableDataSource?.snapshot() else {
                return
            }

            guard let item = self?.collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
                return
            }

            let section = snapshot.sectionIdentifier(containingItem: item)

            switch section {
                case .cast:
                    header.configure(title: Section.cast.rawValue)
                case .about:
                    header.configure(title: Section.about.rawValue)
                case .info:
                    header.configure(title: Section.info.rawValue)
                default:
                    return
            }
        })
    }

    private func configureCell() {
        guard let headerCellRegistration = headerCellRegistration else {
            return
        }
        guard let personCellRegistration = personCellRegistration else {
            return
        }
        guard let aboutCellRegistration = aboutCellRegistration else {
            return
        }
        guard let infoCellRegistration = infoCellRegistration else {
            return
        }

        collectionViewDiffableDataSource = .init(collectionView: collectionView, cellProvider: { [weak self] (collectionView, indexPath, item) -> UICollectionViewCell? in

            guard let _ = self else {
                return UICollectionViewCell()
            }

            guard let snapshot = self?.collectionViewDiffableDataSource?.snapshot() else {
                return UICollectionViewCell()
            }

            let section = snapshot.sectionIdentifier(containingItem: item)

            switch section {
                case .header:
                    let item = item as? EpisodeItem
                    let cell = collectionView.dequeueConfiguredReusableCell(using: headerCellRegistration, for: indexPath, item: item)
                    return cell
                case .cast:
                    let item = item as? CastItem
                    let cell = collectionView.dequeueConfiguredReusableCell(using: personCellRegistration, for: indexPath, item: item)
                    return cell
                case .about:
                    let item = item as? EpisodeItem
                    let cell = collectionView.dequeueConfiguredReusableCell(using: aboutCellRegistration, for: indexPath, item: item)
                    return cell
                case .info:
                    let item = item as? EpisodeItem
                    let cell = collectionView.dequeueConfiguredReusableCell(using: infoCellRegistration, for: indexPath, item: item)
                    return cell
                default:
                    return UICollectionViewCell()
            }
        })
    }

    private func configureHeader() {
        guard let headerRegistration = headerRegistration else {
            return
        }

        guard let sectionHeaderRegistration = sectionHeaderRegistration else {
            return
        }

        collectionViewDiffableDataSource?.supplementaryViewProvider = .some({ [weak self] (collectionView, kind, indexPath) -> UICollectionReusableView? in

            guard let _ = self else {
                return UICollectionViewCell()
            }

            guard let snapshot = self?.collectionViewDiffableDataSource?.snapshot() else {
                return UICollectionViewCell()
            }

            guard let item = self?.collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
                return UICollectionViewCell()
            }
            
            let section = snapshot.sectionIdentifier(containingItem: item)

            switch section {
                case .header:
                    let header = collectionView.dequeueConfiguredReusableSupplementary(using: headerRegistration, for: indexPath)
                    return header
                default:
                    let header = collectionView.dequeueConfiguredReusableSupplementary(using: sectionHeaderRegistration, for: indexPath)
                    return header
            }
        })
    }
}

// MARK: - Previews
// Previews
struct EpisodeContentView: UIViewControllerRepresentable {
    func makeUIViewController(context: Context) -> UIViewController {
        let controller = EpisodeViewController()
        return controller
    }

    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {}

    typealias UIViewControllerType = UIViewController
}

struct EpisodeContentView_Previews: PreviewProvider {
    static var previews: some View {
        EpisodeContentView()
            .edgesIgnoringSafeArea(.all)
    }
}
