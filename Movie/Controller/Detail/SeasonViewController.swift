//
//  SeasonViewController.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/24/20.
//

import SwiftUI

fileprivate typealias SeasonHeaderRegistration = UICollectionView.SupplementaryRegistration<SeasonHeader>
fileprivate typealias SeasonCellRegistration = UICollectionView.CellRegistration<EpisodeCell, EpisodeItem>
fileprivate typealias SeasonDiffableDataSource = UICollectionViewDiffableDataSource<Section, EpisodeItem>

fileprivate enum Section {
    case main
}

// MARK: - Class
class SeasonViewController: UICollectionViewController {
    
    // MARK: - Declarations (Properties)
    private var headerRegistration: SeasonHeaderRegistration?
    private var cellRegistration: SeasonCellRegistration?
    private var collectionViewDiffableDataSource: SeasonDiffableDataSource?
    private let headerKind = UICollectionView.elementKindSectionHeader
    private var item: SeasonDetailItem? {
        didSet {
            setData()
        }
    }
    private var episodes = [EpisodeItem]()
    private var tvId = 0
    private var seasonId = 0
    private let main = DispatchQueue.main
    private let navigationBarBorder = "hidesShadow"
    private var contentOffsetY: CGFloat = 132
    
    // MARK: - Declarations (UI)
    fileprivate lazy var emptyView = CustomEmptyView(frame: collectionView.frame)

    fileprivate let titleView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()

    fileprivate let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 17)
        label.textColor = .label
        label.backgroundColor = .clear
        label.alpha = .zero
        return label
    }()
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func viewWillAppear(_ animated: Bool) {
        setNavigationBarBorder(true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setNavigationBarBorder(false)
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let headerSize: CGFloat = UIDevice.current.hasNotch ? 92 : 64
        let scrollContentOffsetY = scrollView.contentOffset.y + headerSize
        if scrollContentOffsetY >= contentOffsetY {
            titleLabel.alpha = 1
        } else {
            titleLabel.alpha = .zero
        }
    }
    
    // MARK: - Configuration
    private func setupView() {
        view.accessibilityIdentifier = "seasonDetailView"
        configureNavigationBar()
        configureCollectionView()
        configureCollectionViewDiffableDataSource()
        getResults()
    }

    private func configureNavigationBar() {
        setNavigationBarBorder(true)
        navigationItem.largeTitleDisplayMode = .never
        configureTitleView()
        navigationItem.titleView = titleView
    }

    private func setNavigationBarBorder(_ isHidden: Bool) {
        if isHidden {
            navigationController?.navigationBar.setValue(true, forKey: navigationBarBorder)
        } else {
            navigationController?.navigationBar.setValue(false, forKey: navigationBarBorder)
        }
    }
    
    private func configureCollectionView() {
        collectionView.backgroundColor = .systemBackground
        collectionView.alwaysBounceVertical = true
        collectionView.backgroundView = emptyView
    }

    private func configureTitleView() {
        titleView.addSubview(titleLabel)
        titleLabel.makeSuperView(withSafeArea: true)
    }

    private func configureCollectionViewDiffableDataSource() {
        makeCellRegistration()
        makeHeaderRegistration()
        configureCell()
        configureHeader()
    }

    private func applySnapshot() {
        var snapshot = NSDiffableDataSourceSnapshot<Section, EpisodeItem>()
        snapshot.appendSections([.main])
        snapshot.appendItems(episodes, toSection: .main)
        collectionViewDiffableDataSource?.apply(snapshot)
    }

    // MARK: - Methods
    func configure(tvId tv: Int, seasonId season: Int, seasonName name: String) {
        tvId = tv
        seasonId = season
        titleLabel.text = name
    }

    private func getResults() {
        Networking.manager.getSeasonDetail(tvId: tvId, seasonId: seasonId) { [weak self] (result) in
            switch result {
                case .success(let item):
                    self?.item = item
                case .failure(let error):
                    Errors.error(in: FileIndentifier.Season.rawValue, at: #line, error: error)
            }
        }
    }

    private func setData() {
        guard let episodes = item?.episodes else {
            return
        }
        self.episodes.append(contentsOf: episodes)
        main.async {
            self.applySnapshot()
            self.emptyView.configure(isLoading: false, message: nil)
        }
    }

    private func presentOverviewViewController(overview text: String) {
        let controller = OverviewViewController()
        guard let title = item?.name else {
            return
        }
        controller.configure(title: title, overview: text)
        let navigationController = UINavigationController(rootViewController: controller)
        present(navigationController, animated: true)
    }

    private func presentEpisodeViewController(item: EpisodeItem) {
        let controller = EpisodeViewController()
        controller.configure(item: item)
        navigationController?.pushViewController(controller, animated: true)
    }

    // MARK: - Objc Methods
    
    // MARK: - Deinit
    deinit {
        setNavigationBarBorder(false)
    }
    
}

// MARK: - Extension
extension SeasonViewController {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let width = collectionView.frame.width
        let height: CGFloat = 140
        let size = CGSize(width: width, height: height)
        return size
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let item = collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
            return
        }
        guard let crew = item.crew, let casts = item.guest_stars, let overview = item.overview  else {
            return
        }
        if crew.isEmpty && casts.isEmpty && overview.isEmpty {
            return
        }
        presentEpisodeViewController(item: item)
    }
}

extension SeasonViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width - 64
        let height: CGFloat = 112
        let size = CGSize(width: width, height: height)
        return size
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return .zero
    }
}

extension SeasonViewController: SeasonHeaderProtocol {
    func didPressOverviewLabel(overview text: String) {
        presentOverviewViewController(overview: text)
    }
}

extension SeasonViewController {
    private func makeCellRegistration() {
        cellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            guard let _ = self else {
                return
            }
            cell.configure(item: item)
        })
    }

    private func makeHeaderRegistration() {
        headerRegistration = .init(elementKind: headerKind, handler: { [weak self] (header, kind, indexPath) in
            guard let item = self?.item else {
                return
            }
            header.delegate = self
            header.configure(item: item)
        })
    }

    private func configureCell() {
        guard let registration = cellRegistration else {
            return
        }
        collectionViewDiffableDataSource = .init(collectionView: collectionView, cellProvider: { [weak self] (collectionView, indexPath, item) -> UICollectionViewCell? in
            guard let _ = self else {
                return UICollectionViewCell()
            }
            let cell = collectionView.dequeueConfiguredReusableCell(using: registration, for: indexPath, item: item)
            return cell
        })
    }

    private func configureHeader() {
        guard let registration = headerRegistration else {
            return
        }
        collectionViewDiffableDataSource?.supplementaryViewProvider = .some({ [weak self] (collectionView, kind, indexPath) -> UICollectionReusableView? in
            guard let _ = self else {
                return UICollectionReusableView()
            }
            let header = collectionView.dequeueConfiguredReusableSupplementary(using: registration, for: indexPath)
            return header
        })
    }
}

// MARK: - Previews
// Previews
struct SeasonContentView: UIViewControllerRepresentable {
    func makeUIViewController(context: Context) -> UIViewController {
        let layout = UICollectionViewFlowLayout()
        let controller = SeasonViewController(collectionViewLayout: layout)
        return controller
    }

    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {}

    typealias UIViewControllerType = UIViewController
}

struct SeasonContentView_Previews: PreviewProvider {
    static var previews: some View {
        SeasonContentView()
            .edgesIgnoringSafeArea(.all)
    }
}
