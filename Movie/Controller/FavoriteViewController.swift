//
//  FavoriteViewController.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/31/20.
//

import UIKit
import LinkPresentation

fileprivate typealias FavoriteCellRegistration = UICollectionView.CellRegistration<FavoriteCell, MediaItem>
fileprivate typealias FavoriteDiffableDataSource = UICollectionViewDiffableDataSource<Section, MediaItem>
fileprivate typealias Snapshot = NSDiffableDataSourceSnapshot<Section, MediaItem>

fileprivate enum Section {
    case main
}

// MARK: - Class
final class FavoriteViewController: UICollectionViewController {
    
    // MARK: - Declarations (Properties)
    fileprivate var cellRegistration: FavoriteCellRegistration?
    fileprivate var collectionViewDiffableDataSource: FavoriteDiffableDataSource?
    private let notificationCenter = NotificationCenter.default
    private lazy var interaction = UIContextMenuInteraction(delegate: self)
    private var sharingItem: MediaItem?
    private var sharingItemImageView = CustomImageView()
    
    // MARK: - Declarations (UI)
    fileprivate lazy var emptyView: CustomFavoriteEmptyView = {
        let view = CustomFavoriteEmptyView(frame: collectionView.frame)
        view.delegate = self
        return view
    }()
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        defaults.setNewFavoriteItemCount(count: .zero)
        navigationController?.tabBarItem.badgeValue = nil
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        view.accessibilityIdentifier = "favoritesView"
        configureCollectionView()
        configureDiffableDataSource()
        getFavoriteItems()
        notificationObserver()
    }

    private func configureCollectionView() {
        collectionView.backgroundColor = .systemBackground
        collectionView.backgroundView = emptyView
    }

    private func configureDiffableDataSource() {
        makeCellRegistration()
        configureCellRegistration()
    }

    private func applySnapshot() {
        let items = defaults.favoritedItems()
        var snapshot = Snapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems(items, toSection: .main)
        collectionViewDiffableDataSource?.apply(snapshot, animatingDifferences: true)
    }

    // MARK: - Methods
    private func getFavoriteItems() {
        applySnapshot()
        guard let snapshot = collectionViewDiffableDataSource?.snapshot() else {
            return
        }
        if snapshot.numberOfItems > .zero {
            emptyView.isHidden = true
            collectionView.alwaysBounceVertical = true
        } else {
            emptyView.isHidden = false
            collectionView.alwaysBounceVertical = false
        }
        collectionView.reloadData()
    }

    private func presentDetailViewController(item: MediaItem) {
        guard let type = item.media_type else {
            return
        }
        let controller = DetailViewController(mediaType: type)
        controller.configure(item: item)
        navigationController?.pushViewController(controller, animated: true)
    }

    private func notificationObserver() {
        notificationCenter.addObserver(self, selector: #selector(observeItemsChanges(_:)), name: favoritedItemChangesNotification, object: nil)
    }

    private func shareAction(item: MediaItem) -> UIAction {
        sharingItem = item
        let path = item.backdrop_path ?? item.poster_path
        sharingItemImageView.configure(path: path)
        let action = UIAction(title: "Share", image: UIImage(systemName: "square.and.arrow.up")) { [unowned self] (_) in
            let items: [Any] = [
                self
            ]
            let controller = UIActivityViewController(activityItems: items, applicationActivities: nil)
            controller.popoverPresentationController?.sourceView = view
            controller.excludedActivityTypes = [
                .addToReadingList,
                .assignToContact,
                .markupAsPDF,
                .openInIBooks,
                .print,
                .postToFlickr,
                .postToVimeo,
                .saveToCameraRoll,
            ]
            self.present(controller, animated: true)
        }
        return action
    }

    private func deleteAction(item: MediaItem) -> UIAction {
        let action = UIContextMenuActions.deleteAction(item: item) { [weak self] in
            self?.getFavoriteItems()
            // Post a notification
            self?.notificationCenter.post(name: unfavoritedItemNotification, object: nil)
        }
        return action
    }

    private func createContextMenu(indexPath: IndexPath) -> UIMenu {
        guard let item = collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
            return UIMenu()
        }
        let menu = UIMenu(children: [
            UIContextMenuActions.copyAction(item: item),
            shareAction(item: item),
            deleteAction(item: item),
        ])
        return menu
    }
    
    // MARK: - Objc Methods
    @objc private func observeItemsChanges(_ sender: NotificationCenter?) {
        getFavoriteItems()
    }
    
    // MARK: - Deinit
    deinit {
        notificationCenter.removeObserver(self)
        print("Deinit \(FileIndentifier.Favorite.rawValue)")
    }
}

// MARK: - Extension
extension FavoriteViewController {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let padding: CGFloat = 24
        let insets = UIEdgeInsets(top: .zero, left: padding, bottom: 16, right: padding)
        return insets
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let item = collectionViewDiffableDataSource?.itemIdentifier(for: indexPath) else {
            return
        }
        presentDetailViewController(item: item)
    }

    override func collectionView(_ collectionView: UICollectionView, contextMenuConfigurationForItemAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        .init(identifier: nil, previewProvider: nil) { [weak self] (_) -> UIMenu? in
            self?.createContextMenu(indexPath: indexPath)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        4
    }
}

extension FavoriteViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width - 48 - 12) / 2
        let height: CGFloat = 200
        let size = CGSize(width: width, height: height)
        return size
    }
}

extension FavoriteViewController: CustomFavoriteEmptyViewProtocol {
    func didPressButton(tag: Int) {
        if tag == .zero {
            tabBarController?.selectedIndex = .zero
        } else {
            tabBarController?.selectedIndex = 2
        }
    }
}

extension FavoriteViewController: UIActivityItemSource {
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        // Let UIKit knows the type of data we wanna share
        guard let title = sharingItem?.title ?? sharingItem?.name else {
            return ""
        }
        return title
    }

    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        // The custom body of what we wanna share. For instance, this will be the custom message for the mail's body or twitter's.
        let title = (sharingItem?.title ?? sharingItem?.name) ?? ""
        let description = sharingItem?.overview ?? ""
        guard let dummyLink = URL(string: "www.dummylink.com") else {
            return title + "\n\n" + description
        }

        switch activityType {
            case UIActivity.ActivityType.mail:
                let message = title + "\n\n" + description + "\n\n" + "\(dummyLink)"
                return message
            default:
                let url = "\(dummyLink)"
                return url
        }
    }

    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        // The custom subject of what we wanna share in mail
        guard let subject = sharingItem?.title ?? sharingItem?.name else {
            return "The subject"
        }
        return subject
    }

    func activityViewControllerLinkMetadata(_ activityViewController: UIActivityViewController) -> LPLinkMetadata? {
        let metadata = LPLinkMetadata()
        guard let image = sharingItemImageView.image else {
            return metadata
        }
        metadata.iconProvider = .init(object: image)
        guard let item = sharingItem else {
            return metadata
        }
        let title = item.title ?? item.name
        metadata.title = title
        return metadata
    }
}

extension FavoriteViewController: UIContextMenuInteractionDelegate {
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        nil
    }
}

extension FavoriteViewController {
    func makeCellRegistration() {
        cellRegistration = .init(handler: { [weak self] (cell, indexPath, item) in
            guard let self = self else {
                return
            }
            cell.addInteraction(self.interaction)
            cell.configure(item: item)
        })
    }

    func configureCellRegistration() {
        guard let registration = cellRegistration else {
            return
        }
        collectionViewDiffableDataSource = .init(collectionView: collectionView, cellProvider: { [weak self] (collectionView, indexPath, item) -> UICollectionViewCell? in
            guard let _ = self else {
                return UICollectionViewCell()
            }
            let cell = collectionView.dequeueConfiguredReusableCell(using: registration, for: indexPath, item: item)
            return cell
        })
    }
}
