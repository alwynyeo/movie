//
//  Errors.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/25/20.
//

// MARK: - Struct
struct Errors {
    static func error(in file: String, at line: Int, error message: Error? = nil) {
        if let error = message {
            print("[\(file) Error: \(error.localizedDescription) at line \(line)]")
        } else {
            print("[\(file) Error at line \(line)]")
        }
    }
}
