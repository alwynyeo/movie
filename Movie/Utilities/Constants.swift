//
//  Constants.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/26/20.
//

import UIKit

// MARK: - Typealias
typealias CompletionHandler = (_ success: Bool?, _ error: Error?) -> Void
typealias MediaResultsHandler = (Result<[MediaItem], Error>) -> Void
typealias MediaItemHandler = (Result<MediaItem, Error>) -> Void
typealias CreditResultsHandler = (Result<[CastItem], Error>) -> Void
typealias SeasonResultHandler = (Result<SeasonDetailItem, Error>) -> Void
typealias EpisodeResultHandler = (Result<EpisodeItem, Error>) -> Void
typealias EmptyHandler = () -> Void

// MARK: - Constants
let defaults = UserDefaults.standard
let favoritedItemChangesNotification = Notification.Name("favoriteItemChangesNotification")
let unfavoritedItemNotification = Notification.Name("unFavoriteItemNotification")
let mainTabBarController = UIApplication.shared.windows.first?.rootViewController as? MainTabBarController
