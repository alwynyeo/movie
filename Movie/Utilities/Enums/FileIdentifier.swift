//
//  FileIdentifier.swift
//  Movie
//
//  Created by Alwyn Yeo on 1/14/21.
//

enum FileIndentifier: String {
    case Main = "MainTabBarController"
    case Home = "HomeViewController"
    case Detail = "DetailViewController"
    case Search = "SearchViewController"
    case DetailList = "DetailListViewController"
    case PersonDetail = "PersonDetailViewController"
    case Favorite = "FavoriteViewController"
    case Networking = "Networking"
    case SearchHeader = "SearchHeader"
    case SearchCell = "SearchCell"
    case SearchSection = "SearchSection"
    case DetailHeader = "DetailHeaderView"
    case Overview = "OverviewViewController"
    case DetailListCell = "DetailListCell"
    case Season = "SeasonViewController"
    case Episode = "EpisodeViewController"
}
