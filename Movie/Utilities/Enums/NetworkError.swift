//
//  Enums.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/25/20.
//

import Foundation

enum NetworkError: Error {
    case invalidURL
    case invalidResponse
    case invalidData
    case failedToDecode(Error?)
    case responseStatusCode(Int)
    case error(Error?)

    private func getStatusCodeMessage(_ code: Int) -> String {
        switch code {
            case 400:
                return "Bad Request Error with error code \(code)"
            case 401:
                return "Unauthorized Error with error code \(code)"
            case 402:
                return "Payment Required Error with error code \(code)"
            case 403:
                return "Forbidden Error with error code \(code)"
            case 404:
                return "Not Found Error with error code \(code)"
            case 405:
                return "Method Not Allowed Error with error code \(code)"
            case 407:
                return "Not Acceptable Error with error code \(code)"
            case 408:
                return "Request Timeout Error with error code \(code)"
            case 409:
                return "Conflict Error with error code \(code)"
            case 410:
                return "Gone Error with error code \(code)"
            case 411:
                return "Length Required Error with error code \(code)"
            case 412:
                return "Precondition Failed Error with error code \(code)"
            case 413:
                return "Payload Too Large Error with error code \(code)"
            case 414:
                return "URI Too Long Error with error code \(code)"
            case 415:
                return "Unsupported Media Type Error with error code \(code)"
            case 416:
                return "Range Not Satisfiable Error with error code \(code)"
            case 417:
                return "Expectation Failed Error with error code \(code)"
            case 421:
                return "Misdirected Request Error with error code \(code)"
            case 422:
                return "Unprocessable Entity Error with error code \(code)"
            case 423:
                return "Locked Required Error with error code \(code)"
            case 424:
                return "Failed Dependency Error with error code \(code)"
            case 425:
                return "Too Early Error with error code \(code)"
            case 428:
                return "Upgrade Required Required Error with error code \(code)"
            case 429:
                return "Precondition Required Error with error code \(code)"
            case 431:
                return "Request Header Fields Too Large Error with error code \(code)"
            case 451:
                return "Unavailable For Legal Reasons Error with error code \(code)"
            case 500:
                return "Internal Server Error with error code \(code)"
            case 501:
                return "Request Method Not Recognized Error with error code \(code)"
            case 502:
                return "Bad Gateway Error with error code \(code)"
            case 503:
                return "Service Unavailable Error with error code \(code)"
            case 504:
                return "Gateway Timeout Error with error code \(code)"
            case 505:
                return "HTTP Version Not Supported Error with error code \(code)"
            case 507:
                return "Internal Server Error with error code \(code)"
            case 508:
                return "Loop Detected In Server While Processing The Request Error with error code \(code)"
            case 510:
                return "Not Extended Error with error code \(code)"
            case 511:
                return "Network Authentication Required Error with error code \(code)"
            default:
                return "\(code)"
        }
    }
}

extension NetworkError: LocalizedError {
    var errorDescription: String? {
        switch self {
            case .invalidURL:
                return NSLocalizedString("The url is invalid", comment: "")
            case .invalidResponse:
                return NSLocalizedString("The response is invalid", comment: "")
            case .invalidData:
                return NSLocalizedString("The data is invalid", comment: "")
            case .failedToDecode(let error):
                return NSLocalizedString("The data was failed to decode due to \(String(describing: error?.localizedDescription))", comment: "")
            case .responseStatusCode(let code):
                return NSLocalizedString(getStatusCodeMessage(code), comment: "")
            case .error(let error):
                return NSLocalizedString((String(describing: error?.localizedDescription)), comment: "")
        }
    }
}
