//
//  HomeSectionType.swift
//  Movie
//
//  Created by Alwyn Yeo on 1/14/21.
//

enum HomeSectionType: String {
    case topRated = "top_rated"
    case nowPlaying = "now_playing"
    case airToday = "airing_today"
    case popular = "popular"
    case upcoming = "upcoming"
}
