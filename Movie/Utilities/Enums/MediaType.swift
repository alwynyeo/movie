//
//  MediaType.swift
//  Movie
//
//  Created by Alwyn Yeo on 1/14/21.
//

enum MediaType: String {
    case movie = "movie"
    case tv = "tv"
    case person = "person"
}
