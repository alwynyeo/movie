//
//  UIContextMenuActions.swift
//  Movie
//
//  Created by Alwyn Yeo on 1/9/21.
//

import UIKit

// MARK: - Struct
struct UIContextMenuActions {
    static func copyAction(item: MediaItem) -> UIAction {
        let action = UIAction(title: "Copy Name", image: UIImage(systemName: "doc.on.doc")) { (_) in
            if let text = item.title ?? item.name {
                UIPasteboard.general.string = text
            }
        }
        return action
    }

    static func addAction(item: MediaItem, selectedMediaType type: String? = nil, completion: @escaping EmptyHandler) -> UIAction {
        let action = UIAction(title: "Add to Favorites", image: UIImage(systemName: "heart")) { (_) in
            // FavoriteItem
            var item = item
            if let type = type {
                item.setMediaType(value: type)
            }
            defaults.favoritedItem(item: item)
            completion()
        }
        return action
    }

    static func deleteAction(item: MediaItem, completion: @escaping EmptyHandler) -> UIAction {
        let action = UIAction(title: "Delete from Favorites", image: UIImage(systemName: "trash"), attributes: .destructive) { (_) in
            // UnfavoriteItem
            defaults.unfavoriteItem(item: item)
            completion()
        }
        return action
    }

    static func shareAction(item: MediaItem) {
        
    }
}
