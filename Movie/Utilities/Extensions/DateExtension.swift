//
//  DateExtension.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/27/20.
//

import Foundation

// MARK: - Extension
extension Date {
    // Get today's date
    static func today() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
    }
}
