//
//  NetworkExtension.swift
//  Movie
//
//  Created by Alwyn Yeo on 1/14/21.
//

import Network

// MARK: - Extension
extension NWPathMonitor {
    typealias NetworkCompletionHandler = (_ status: NWPath.Status) -> Void
    func startMonitoringNetworkChanges(completion: @escaping NetworkCompletionHandler) {
        let queue = DispatchQueue.global(qos: .background)
        pathUpdateHandler = .some({ [weak self] (path) in
            guard let _ = self else {
                return
            }
            let status = path.status
            completion(status)
        })
        start(queue: queue)
    }
}
