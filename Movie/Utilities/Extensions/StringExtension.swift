//
//  StringExtension.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/30/20.
//

import UIKit

// MARK: - Extension
extension String {
    subscript(i: Int) -> String {
        return String(self[index(startIndex, offsetBy: i)])
    }

    // Checks for whitespaces
    func containsText() -> Bool {
        if trimmingCharacters(in: .whitespaces).isEmpty {
            // Return false if string containts only whitespaces
            return false
        }
        // Return true if string containts text & whitespaces
        return true
    }
}
