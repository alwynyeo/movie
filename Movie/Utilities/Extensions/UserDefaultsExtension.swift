//
//  UserDefaultsExtension.swift
//  Movie
//
//  Created by Alwyn Yeo on 1/3/21.
//

import Foundation

extension UserDefaults {
    static let newFavoriteCountKey = "newFavoriteItemCountKey"
    private static let favoriteKey = "favoriteItemKey"
    private static let encoder = JSONEncoder()
    private static let decoder = JSONDecoder()

    func favoritedItem(item: MediaItem) {
        var items = favoritedItems()
        var newFavoriteCount = newFavoriteItemCount()
        items.append(item)
        setValues(items: items)
        newFavoriteCount += 1
        setNewFavoriteItemCount(count: newFavoriteCount)
    }

    func unfavoriteItem(item: MediaItem) {
        var items = favoritedItems()
        var newFavoriteCount = newFavoriteItemCount()
        items.removeAll(where: { $0.id == item.id })
        setValues(items: items)
        if newFavoriteCount <= .zero {
            newFavoriteCount = .zero
        } else {
            newFavoriteCount -= 1
        }
        setNewFavoriteItemCount(count: newFavoriteCount)
    }

    func setNewFavoriteItemCount(count: Int) {
        set(count, forKey: UserDefaults.newFavoriteCountKey)
    }

    func favoritedItems() -> [MediaItem] {
        guard let data = value(forKey: UserDefaults.favoriteKey) as? Data else {
            return []
        }
        guard let items = try? UserDefaults.decoder.decode(Array<MediaItem>.self, from: data) else {
            return []
        }
        return items
    }

    func newFavoriteItemCount() -> Int {
        return integer(forKey: UserDefaults.newFavoriteCountKey)
    }

    private func setValues(items: [MediaItem]) {
        let encoded = try? UserDefaults.encoder.encode(items)
        set(encoded, forKey: UserDefaults.favoriteKey)
    }
}
