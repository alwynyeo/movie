//
//  UIViewExtension.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/26/20.
//

import UIKit

// MARK: - Extension
extension UIView {
    func makeSuperView(withSafeArea safeArea: Bool = false) {
        translatesAutoresizingMaskIntoConstraints = false
        if let superview = superview {
            if safeArea {
                topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor).isActive = true
                leadingAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.leadingAnchor).isActive = true
                trailingAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.trailingAnchor).isActive = true
                bottomAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.bottomAnchor).isActive = true
            } else {
                topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
                leadingAnchor.constraint(equalTo: superview.leadingAnchor).isActive = true
                trailingAnchor.constraint(equalTo: superview.trailingAnchor).isActive = true
                bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
            }
        }
    }

    func makeView(top: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, trailing: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, constants: UIEdgeInsets = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: constants.top).isActive = true
        }
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: constants.left).isActive = true
        }
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: -constants.right).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -constants.bottom).isActive = true
        }
    }

    func makeCenter(to view: UIView?, withSafeArea safeArea: Bool = false, centerXConstant: CGFloat = .zero, centerYConstant: CGFloat = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        if let view = view {
            if safeArea {
                centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor, constant: centerXConstant).isActive = true
                centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor, constant: centerYConstant).isActive = true
                return
            }
            centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: centerXConstant).isActive = true
            centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: centerYConstant).isActive = true
        }
    }

    func makeCenterX(to view: UIView?, withSafeArea safeArea: Bool = false, constant: CGFloat = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        if let view = view {
            if safeArea {
                centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor, constant: constant).isActive = true
                return
            }
            centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: constant).isActive = true
        }
    }

    func makeCenterY(to view: UIView?, withSafeArea safeArea: Bool = false, constant: CGFloat = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        if let view = view {
            if safeArea {
                centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor, constant: constant).isActive = true
                return
            }
            centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: constant).isActive = true
        }
    }
    
    func makeSize(size: CGSize) {
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(equalToConstant: size.width).isActive = true
        heightAnchor.constraint(equalToConstant: size.height).isActive = true
    }

    func makeWidth(width size: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(equalToConstant: size).isActive = true
    }

    func makeHeight(height size: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalToConstant: size).isActive = true
    }

    func addSubviews(_ views: [UIView]) {
        views.forEach { (view) in
            addSubview(view)
        }
    }
}
