//
//  UIDeviceExtension.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/10/20.
//

import UIKit

// MARK: - Extension
extension UIDevice {
    var hasNotch: Bool {
        let keyWindow = UIApplication.shared.windows.first
        if #available(iOS 11.0, *) {
            // Case 1: Portrait && top safe area inset >= 44
            let case1 = !UIDevice.current.orientation.isLandscape && keyWindow?.safeAreaInsets.top ?? .zero >= 44

            // Case 2: Landscape && left / right safe area inset > 0
            let case2 = UIDevice.current.orientation.isLandscape && keyWindow?.safeAreaInsets.top ?? .zero > .zero || keyWindow?.safeAreaInsets.right ?? .zero > .zero

            return case1 || case2
        } else {
            // Fallback on earlier versions
            return false
        }
    }
}
