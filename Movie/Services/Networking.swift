//
//  Networking.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/27/20.
//

import Foundation

// MARK: - Class
final class Networking {
    
    // MARK: - Declarations
    final private let apiKey = "47bbfb24763a46c352ef750877c887a6"
    final private var task: URLSessionTask?
    final private var components: URLComponents = {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.themoviedb.org"
        return components
    }()
    final private let configuration: URLSessionConfiguration = {
        let configuration = URLSessionConfiguration.default
        configuration.waitsForConnectivity = true
        configuration.allowsExpensiveNetworkAccess = true
        configuration.allowsConstrainedNetworkAccess = true
        configuration.timeoutIntervalForResource = 5
        return configuration
    }()
    final private let decoder = JSONDecoder()
    final lazy var session: URLSessionProtocol = URLSession(configuration: configuration)
    final private let successCode = 200
    
    // MARK: - Singleton
    static let manager = Networking()
    
    // MARK: - Methods
    func getTrendings(mediaType type: String, completion: @escaping MediaResultsHandler) {
        components.path = "/3/trending/\(type)/week"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
        ]

        guard let url = components.url else {
            completion(.failure(NetworkError.invalidURL))
            preconditionFailure("Failed to construct URL at line \(#line)")
        }

        var mediaResult: MediaResult?

        task = session.dataTask(with: url) { [weak self] (data, response, error) in
            guard error == nil else {
                completion(.failure(NetworkError.error(error)))
                return
            }

            guard let response = response as? HTTPURLResponse else {
                completion(.failure(NetworkError.invalidResponse))
                return
            }

            guard response.statusCode == self?.successCode else {
                completion(.failure(NetworkError.responseStatusCode(response.statusCode)))
                return
            }

            guard let data = data else {
                completion(.failure(NetworkError.invalidData))
                return
            }

            do {
                mediaResult = try self?.decoder.decode(MediaResult.self, from: data)
            } catch let error {
                completion(.failure(NetworkError.failedToDecode(error)))
            }

            guard let results = mediaResult?.results else {
                completion(.failure(NetworkError.error(error)))
                return
            }
            completion(.success(results))
        }
        task?.resume()
    }

    func getSearchResults(search query: String, page: Int = 1, mediaType type: String = "multi", completion: @escaping MediaResultsHandler) {
        components.path = "/3/search/\(type)"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "language", value: "en-US"),
            URLQueryItem(name: "query", value: query),
            URLQueryItem(name: "page", value: "\(page)"),
            URLQueryItem(name: "include_adult", value: "\(false)"),
            URLQueryItem(name: "region", value: "US"), // change to locale region code
        ]

        guard let url = components.url else {
            completion(.failure(NetworkError.invalidURL))
            preconditionFailure("Failed to construct URL at line \(#line)")
        }

        var mediaResult: MediaResult?

        task?.cancel()
        task = session.dataTask(with: url) { [weak self] (data, response, error) in
            guard error == nil else {
                completion(.failure(NetworkError.error(error)))
                return
            }

            guard let response = response as? HTTPURLResponse else {
                completion(.failure(NetworkError.invalidResponse))
                return
            }

            guard response.statusCode == self?.successCode else {
                completion(.failure(NetworkError.responseStatusCode(response.statusCode)))
                return
            }

            guard let data = data else {
                completion(.failure(NetworkError.invalidData))
                return
            }

            do {
                mediaResult = try self?.decoder.decode(MediaResult.self, from: data)
            } catch let error {
                completion(.failure(NetworkError.failedToDecode(error)))
            }

            guard let results = mediaResult?.results else {
                return
            }
            completion(.success(results))
        }
        task?.resume()
    }

    func getDetail(mediaType type: String, id: Int, completion: @escaping MediaItemHandler) {
        components.path = "/3/\(type)/\(id)"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "language", value: "en-US"),
        ]

        guard let url = components.url else {
            completion(.failure(NetworkError.invalidURL))
            preconditionFailure("Failed to construct URL at line \(#line)")
        }

        var mediaItem: MediaItem?

        task = nil
        task = session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard error == nil else {
                completion(.failure(NetworkError.error(error)))
                return
            }

            guard let response = response as? HTTPURLResponse else {
                completion(.failure(NetworkError.invalidResponse))
                return
            }

            guard response.statusCode == self?.successCode else {
                completion(.failure(NetworkError.responseStatusCode(response.statusCode)))
                return
            }

            guard let data = data else {
                completion(.failure(NetworkError.invalidData))
                return
            }

            do {
                mediaItem = try self?.decoder.decode(MediaItem.self, from: data)
            } catch let error {
                completion(.failure(NetworkError.failedToDecode(error)))
            }

            guard let mediaItem = mediaItem else {
                return
            }
            completion(.success(mediaItem))
        })
        task?.resume()
    }

    func getRelated(mediaType type: String, mediaId id: Int, page: Int = 1, completion: @escaping MediaResultsHandler) {
        components.path = "/3/\(type)/\(id)/similar"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "language", value: "en-US"),
            URLQueryItem(name: "page", value: "\(page)"),
        ]

        guard let url = components.url else {
            completion(.failure(NetworkError.invalidURL))
            preconditionFailure("Failed to construct URL at line \(#line)")
        }

        var mediaResult: MediaResult?

        task = nil
        task = session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard error == nil else {
                completion(.failure(NetworkError.error(error)))
                return
            }

            guard let response = response as? HTTPURLResponse else {
                completion(.failure(NetworkError.invalidResponse))
                return
            }

            guard response.statusCode == self?.successCode else {
                completion(.failure(NetworkError.responseStatusCode(response.statusCode)))
                return
            }

            guard let data = data else {
                completion(.failure(NetworkError.invalidData))
                return
            }

            do {
                mediaResult = try self?.decoder.decode(MediaResult.self, from: data)
            } catch let error {
                completion(.failure(NetworkError.failedToDecode(error)))
            }

            let results = mediaResult?.results ?? [MediaItem()]
            completion(.success(results))
        })
        task?.resume()
    }

    func getCasts(mediaType type: String, id: Int, completion: @escaping CreditResultsHandler) {
        components.path = "/3/\(type)/\(id)/credits"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "language", value: "en-US"),
        ]

        guard let url = components.url else {
            completion(.failure(NetworkError.invalidURL))
            preconditionFailure("Failed to construct URL at line \(#line)")
        }

        var creditItem: CreditItem?

        task = nil
        task = session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard error == nil else {
                completion(.failure(NetworkError.error(error)))
                return
            }

            guard let response = response as? HTTPURLResponse else {
                completion(.failure(NetworkError.invalidResponse))
                return
            }

            guard response.statusCode == self?.successCode else {
                completion(.failure(NetworkError.responseStatusCode(response.statusCode)))
                return
            }

            guard let data = data else {
                completion(.failure(NetworkError.invalidData))
                return
            }

            do {
                creditItem = try self?.decoder.decode(CreditItem.self, from: data)
            } catch let error {
                completion(.failure(NetworkError.failedToDecode(error)))
            }

            let casts = creditItem?.cast ?? [CastItem()]
            completion(.success(casts))
        })
        task?.resume()
    }

    func getSeasonDetail(tvId tv: Int, seasonId season: Int, completion: @escaping SeasonResultHandler) {
        components.path = "/3/tv/\(tv)/season/\(season)"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "language", value: "en-US"),
        ]

        guard let url = components.url else {
            completion(.failure(NetworkError.invalidURL))
            preconditionFailure("Failed to construct URL at line \(#line)")
        }

        var seasonDetailItem: SeasonDetailItem?

        task = nil
        task = session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard error == nil else {
                completion(.failure(NetworkError.error(error)))
                return
            }

            guard let response = response as? HTTPURLResponse else {
                completion(.failure(NetworkError.invalidResponse))
                return
            }

            guard response.statusCode == self?.successCode else {
                completion(.failure(NetworkError.responseStatusCode(response.statusCode)))
                return
            }

            guard let data = data else {
                completion(.failure(NetworkError.invalidData))
                return
            }

            do {
                seasonDetailItem = try self?.decoder.decode(SeasonDetailItem.self, from: data)
            } catch let error {
                completion(.failure(NetworkError.failedToDecode(error)))
            }

            guard let seasonDetailItem = seasonDetailItem else {
                return
            }
            completion(.success(seasonDetailItem))
        })
        task?.resume()
    }

    func getCredits(personId id: Int, completion: @escaping MediaResultsHandler) {
        components.path = "/3/person/\(id)/combined_credits"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "language", value: "en-US"),
        ]

        guard let url = components.url else {
            completion(.failure(NetworkError.invalidURL))
            preconditionFailure("Failed to construct URL at line \(#line)")
        }

        var personCredit: PersonCredit?

        task = nil
        task = session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard error == nil else {
                completion(.failure(NetworkError.error(error)))
                return
            }

            guard let response = response as? HTTPURLResponse else {
                completion(.failure(NetworkError.invalidResponse))
                return
            }

            guard response.statusCode == self?.successCode else {
                completion(.failure(NetworkError.responseStatusCode(response.statusCode)))
                return
            }

            guard let data = data else {
                completion(.failure(NetworkError.invalidData))
                return
            }

            do {
                personCredit = try self?.decoder.decode(PersonCredit.self, from: data)
            } catch let error {
                completion(.failure(NetworkError.failedToDecode(error)))
            }

            let cast = personCredit?.cast ?? [MediaItem()]
            completion(.success(cast))
        })
        task?.resume()
    }

    func getHomeResults(of section: HomeSectionType, mediaType type: String, page: Int = 1, completion: @escaping MediaResultsHandler) {
        components.path = "/3/\(type)/\(section.rawValue)"
        components.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "language", value: "en-US"),
            URLQueryItem(name: "page", value: "\(page)"),
        ]

        guard let url = components.url else {
            completion(.failure(NetworkError.invalidURL))
            preconditionFailure("Failed to construct URL at line \(#line)")
        }

        var mediaResult: MediaResult?

        task = nil
        task = session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard error == nil else {
                completion(.failure(NetworkError.error(error)))
                return
            }

            guard let response = response as? HTTPURLResponse else {
                completion(.failure(NetworkError.invalidResponse))
                return
            }

            guard response.statusCode == self?.successCode else {
                completion(.failure(NetworkError.responseStatusCode(response.statusCode)))
                return
            }

            guard let data = data else {
                completion(.failure(NetworkError.invalidData))
                return
            }

            do {
                mediaResult = try self?.decoder.decode(MediaResult.self, from: data)
            } catch let error {
                completion(.failure(NetworkError.failedToDecode(error)))
            }

            guard let results = mediaResult?.results else {
                return
            }
            completion(.success(results))
        })
        task?.resume()
    }
}
