//
//  MockUserDefaults.swift
//  Movie
//
//  Created by Alwyn Yeo on 1/15/21.
//

import Foundation

class MockUserDefaults: UserDefaults {
    private(set) var isFavorited = false
    override func set(_ value: Bool, forKey defaultName: String) {
        if defaultName == "testing" {
            isFavorited = value
        }
    }
}
