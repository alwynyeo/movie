//
//  URLSessionDataTaskMock.swift
//  MovieSlowTests
//
//  Created by Alwyn Yeo on 1/13/21.
//

import Foundation

final class URLSessionDataTaskMock: URLSessionDataTask {
    typealias CompletionHandler = (Data?, URLResponse?, Error?) -> Void
    var completionHandler: CompletionHandler?
    var taskResponse: (Data?, URLResponse?, Error?)?

    override init() {}

    override func resume() {
        DispatchQueue.main.async {
            self.completionHandler?(self.taskResponse?.0, self.taskResponse?.1, self.taskResponse?.2)
        }
    }
}
