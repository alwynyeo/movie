//
//  URLSessionProtocol.swift
//  MovieTests
//
//  Created by Alwyn Yeo on 1/12/21.
//

import Foundation

typealias DataTaskResultHandler = (Data?, URLResponse?, Error?) -> Void

protocol URLSessionProtocol {
    func dataTask(with url: URL, completionHandler: @escaping DataTaskResultHandler) -> URLSessionDataTask
    func dataTask(with request: URLRequest, completionHandler: @escaping DataTaskResultHandler) -> URLSessionDataTask
}

extension URLSession: URLSessionProtocol {}
