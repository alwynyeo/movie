//
//  URLSessionMock.swift
//  MovieSlowTests
//
//  Created by Alwyn Yeo on 1/13/21.
//

import Foundation

final class URLSessionMock: URLSessionProtocol {
    var expectedUrl: URL?
    var expectedRequest: URLRequest?
    private let dataTaskMock: URLSessionDataTaskMock

    init(data: Data? = nil, response: URLResponse? = nil, error: Error? = nil) {
        dataTaskMock = URLSessionDataTaskMock()
        dataTaskMock.taskResponse = (data, response, error)
    }

    convenience init?(json data: [String: Any], response: URLResponse? = nil, error: Error? = nil) {
        guard let data = try? JSONSerialization.data(withJSONObject: data, options: []) else {
            return nil
        }
        self.init(data: data, response: response, error: error)
    }

    func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        self.expectedUrl = url
        self.dataTaskMock.completionHandler = completionHandler
        return self.dataTaskMock
    }

    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        self.expectedRequest = request
        self.dataTaskMock.completionHandler = completionHandler
        return self.dataTaskMock
    }
}
