//
//  EpisodeItem.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/24/20.
//

import Foundation

// MARK: - Struct
struct EpisodeItem: Decodable {
    
    // MARK: - Declarations
    private let uuid = UUID()
    private(set) var air_date: String?
    private(set) var crew: [CastItem]?
    private(set) var episode_number: Int?
    private(set) var guest_stars: [CastItem]?
    private(set) var name: String?
    private(set) var overview: String?
    private(set) var id: Int?
    private(set) var production_code: String?
    private(set) var season_number: Int?
    private(set) var still_path: String?
    private(set) var vote_average: Double?
    private(set) var vote_count: Int?

    private enum CodingKeys: String, CodingKey {
        case air_date
        case crew
        case episode_number
        case guest_stars
        case name
        case overview
        case id
        case production_code
        case season_number
        case still_path
        case vote_average
        case vote_count
    }
}

extension EpisodeItem: Hashable {}

extension EpisodeItem: Identifiable {}
