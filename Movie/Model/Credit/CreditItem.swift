//
//  CreditItem.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/20/20.
//

// MARK: - Struct
struct CreditItem: Decodable {
    
    // MARK: - Declarations
    private(set) var id: Int?
    private(set) var cast: [CastItem]?
}

extension CreditItem: Hashable {}

extension CreditItem: Identifiable {}
