//
//  CastItem.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/20/20.
//

// MARK: - Struct
struct CastItem: Decodable {
    
    // MARK: - Declarations
    private(set) var id: Int?
    private(set) var gender: Int?
    private(set) var adult: Bool?
    private(set) var name: String?
    private(set) var original_name: String?
    private(set) var popularity: Double?
    private(set) var profile_path: String?
    private(set) var character: String?
    private(set) var department: String?
    private(set) var job: String?
}

extension CastItem: Hashable {}

extension CastItem: Identifiable {}
