//
//  SeasonItem.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/20/20.
//

// MARK: - Struct
struct SeasonItem: Codable {
    
    // MARK: - Declarations
    private(set) var air_date: String?
    private(set) var episode_count: Int?
    private(set) var id: Int?
    private(set) var name: String?
    private(set) var overview: String?
    private(set) var poster_path: String?
    private(set) var season_number: Int?
}

extension SeasonItem: Hashable {}

extension SeasonItem: Identifiable {}
