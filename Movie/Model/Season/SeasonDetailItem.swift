//
//  SeasonDetailItem.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/24/20.
//

// MARK: - Struct
struct SeasonDetailItem: Decodable {
    
    // MARK: - Declarations
    private(set) var air_date: String?
    private(set) var episodes: [EpisodeItem]?
    private(set) var name: String?
    private(set) var overview: String?
    private(set) var id: Int?
    private(set) var poster_path: String?
    private(set) var season_number: Int?
}

extension SeasonDetailItem: Hashable {}

extension SeasonDetailItem: Identifiable {}
