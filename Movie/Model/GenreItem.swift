//
//  GenreItem.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/15/20.
//

// MARK: - Struct
struct GenreItem: Codable {
    
    // MARK: - Declarations
    private(set) var id: Int?
    private(set) var name: String?
}

extension GenreItem: Hashable {}

extension GenreItem: Identifiable {}
