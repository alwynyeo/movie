//
//  PersonCredit.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/28/20.
//

// MARK: - Struct
struct PersonCredit: Decodable {
    
    // MARK: - Declarations
    private(set) var cast: [MediaItem]?
    private(set) var id: Int?
}

extension PersonCredit: Hashable {}

extension PersonCredit: Identifiable {}
