//
//  MediaResult.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/26/20.
//

// MARK: - Struct
struct MediaResult: Decodable {

    // MARK: - Declarations
    private(set) var page: Int?
//    private(set) var total_pages: Int?
    private(set) var results: [MediaItem]?
//    private(set) var total_results: Int?
}

extension MediaResult: Hashable {}
