//
//  MediaItem.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/27/20.
//

import Foundation

// MARK: - Struct
struct MediaItem: Codable {
    
    // MARK: - Declarations
    private let uuid = UUID()
    private(set) var id: Int?
    private(set) var title: String?
    private(set) var original_title: String?
    private(set) var overview: String?
    private(set) var media_type: String?
    private(set) var genre_ids: [Int]?
    private(set) var popularity: Double?
    private(set) var backdrop_path: String?
    private(set) var poster_path: String?
    private(set) var video: Bool?
    private(set) var vote_average: Double?
    private(set) var vote_count: Int?
    private(set) var original_language: String?
    private(set) var release_date: String?
    private(set) var adult: Bool?
    private(set) var origin_country: [String]?
    private(set) var first_air_date: String?
    private(set) var name: String?
    private(set) var gender: Int?
    private(set) var profile_path: String?
    private(set) var tagline: String?
    private(set) var genres: [GenreItem]?
    private(set) var status: String?
    private(set) var seasons: [SeasonItem]?
    private(set) var number_of_episodes: Int?
    private(set) var number_of_seasons: Int?
    private(set) var original_name: String?
    private(set) var episode_run_time: [Int]?
    private(set) var runtime: Int?
    private(set) var languages: [String]?

    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case original_title
        case overview
        case media_type
        case genre_ids
        case popularity
        case backdrop_path
        case poster_path
        case video
        case vote_average
        case vote_count
        case original_language
        case release_date
        case adult
        case origin_country
        case first_air_date
        case name
        case gender
        case profile_path
        case tagline
        case genres
        case status
        case seasons
        case number_of_episodes
        case number_of_seasons
        case original_name
        case episode_run_time
        case runtime
        case languages
    }

    mutating func setMediaType(value: String) {
        media_type = value
    }
}

extension MediaItem: Hashable {}

extension MediaItem: Identifiable {}
