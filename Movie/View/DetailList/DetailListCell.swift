//
//  DetailListCell.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/4/20.
//

import UIKit

// MARK: - Class
final class DetailListCell: UICollectionViewCell {
    
    // MARK: - Declarations (Properties)
    private var item: MediaItem? {
        didSet {
            setData()
        }
    }
    private var imageViewLeadingConstraint: NSLayoutConstraint?
    private var imageViewTrailingConstraint: NSLayoutConstraint?
    private var imageViewBottomConstraint: NSLayoutConstraint?
    private var imageViewWidthConstraint: NSLayoutConstraint?
    private var imageViewHeightConstraint: NSLayoutConstraint?
    private var imageViewCenterXConstraint: NSLayoutConstraint?
    private let shapeLayer = CAShapeLayer()
    private var cornerRadius: CGFloat = .zero
    private var shadowPath: CGPath?
    private var shadowOffset: CGSize = .zero
    
    // MARK: - Declarations (UI)
    fileprivate lazy var imageView: CustomImageView = {
        let frame = CGRect(x: .zero, y: .zero, width: bounds.width, height: bounds.width)
        let imageView = CustomImageView(frame: frame)
        return imageView
    }()

    fileprivate let titleLabel = CustomLabel(font: .systemFont(ofSize: 14), textAlignment: .center, numberOfLines: .zero)
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .clear
        layer.insertSublayer(shapeLayer, at: .zero)
        shapeLayer.isHidden = true
        configureImageView()
    }

    private func configureImageView() {
        addSubview(imageView)
        let size = bounds.width
        imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageViewLeadingConstraint = imageView.leadingAnchor.constraint(equalTo: leadingAnchor)
        imageViewTrailingConstraint = imageView.trailingAnchor.constraint(equalTo: trailingAnchor)
        imageViewBottomConstraint = imageView.bottomAnchor.constraint(equalTo: bottomAnchor)
        imageViewWidthConstraint = imageView.widthAnchor.constraint(equalToConstant: size)
        imageViewHeightConstraint = imageView.heightAnchor.constraint(equalToConstant: size)
        imageViewCenterXConstraint = imageView.centerXAnchor.constraint(equalTo: centerXAnchor)
    }

    private func configureTitleLabel() {
        addSubview(titleLabel)
        let constants = UIEdgeInsets(top: 8, left: .zero, bottom: .zero, right: .zero)
        titleLabel.makeView(top: imageView.bottomAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, constants: constants)
    }
    
    // MARK: - Methods
    func configure(item: MediaItem) {
        self.item = item
    }

    private func setData() {
        setTitleLabel()
        setImageView()
        configureShapeLayer()
    }

    private func setTitleLabel() {
        switch item?.media_type {
            case MediaType.person.rawValue:
                configureTitleLabel()
                titleLabel.text = item?.name
                setPersonImageConstraints()
            default:
                titleLabel.removeFromSuperview()
                setFilmImageConstraints()
        }
    }

    private func setImageView() {
        let mediaType = item?.media_type
        switch mediaType {
            case MediaType.person.rawValue:
                imageView.configure(path: item?.profile_path)
            default:
                imageView.configure(path: item?.backdrop_path ?? item?.poster_path)
        }
        shapeLayer.isHidden = false
    }

    private func configureShapeLayer() {
        imageView.layer.cornerRadius = cornerRadius
        shapeLayer.shadowColor = UIColor.systemGray.cgColor
        shapeLayer.shadowOpacity = 0.3
        shapeLayer.shadowRadius = 7
        shapeLayer.shadowPath = shadowPath
        shapeLayer.shadowOffset = shadowOffset
        shapeLayer.shouldRasterize = true
        shapeLayer.rasterizationScale = UIScreen.main.scale
    }

    private func setFilmImageConstraints() {
        imageViewWidthConstraint?.isActive = false
        imageViewHeightConstraint?.isActive = false
        imageViewCenterXConstraint?.isActive = false
        imageViewLeadingConstraint?.isActive = true
        imageViewTrailingConstraint?.isActive = true
        imageViewBottomConstraint?.isActive = true

        cornerRadius = 10
        let rect = CGRect(x: .zero, y: bounds.maxY, width: bounds.width, height: 10)
        shadowPath = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius).cgPath
        shadowOffset = .zero
    }

    private func setPersonImageConstraints() {
        imageViewLeadingConstraint?.isActive = false
        imageViewTrailingConstraint?.isActive = false
        imageViewBottomConstraint?.isActive = false
        imageViewWidthConstraint?.isActive = true
        imageViewHeightConstraint?.isActive = true
        imageViewCenterXConstraint?.isActive = true

        cornerRadius = imageView.bounds.width / 2
        shadowPath = UIBezierPath(roundedRect: imageView.bounds, cornerRadius: cornerRadius).cgPath
        shadowOffset = .zero
    }

    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
