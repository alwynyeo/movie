//
//  DetailListFooterView.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/7/20.
//

import UIKit
import Network

// MARK: - Class
class DetailListFooterView: UICollectionReusableView {
    // MARK: - Declarations (Properties)
    private let main = DispatchQueue.main

    // MARK: - Declarations (UI)
    fileprivate let activityIndicatorView = CustomActivityIndicatorView(style: .medium)

    fileprivate let titleLabel: UILabel = {
        let label = UILabel()
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5

        let titleAttributes: [NSAttributedString.Key : Any] = [
            .font : UIFont.systemFont(ofSize: 17, weight: .bold),
            .foregroundColor : UIColor.label,
        ]
        let descriptionAttributes: [NSAttributedString.Key : Any] = [
            .font : UIFont.systemFont(ofSize: 17, weight: .regular),
            .foregroundColor : UIColor.label,
        ]

        let title = "Couldn't Load Results"
        let message = "The network is currently unavailable."
        let attributedText = NSMutableAttributedString(string: title, attributes: titleAttributes)
        let spacing = NSAttributedString(string: "\n")
        let description = NSAttributedString(string: message, attributes: descriptionAttributes)
        let range = NSRange(location: .zero, length: attributedText.length)

        attributedText.append(spacing) // 1. Add spacing
        attributedText.append(description)
        attributedText.addAttribute(.paragraphStyle, value: paragraphStyle, range: range) // 2. Add paragraph line

        label.attributedText = attributedText
        label.textAlignment = .center
        label.numberOfLines = .zero
        label.isHidden = true
        return label
    }()

    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .systemBackground
        configureActivityIndicatorView()
        configureTitleLabel()
        networkChangesObserver()
    }

    // MARK: - Methods
    private func configureActivityIndicatorView() {
        addSubview(activityIndicatorView)
        activityIndicatorView.makeCenter(to: self)
        activityIndicatorView.makeSuperView(withSafeArea: true)
    }

    private func configureTitleLabel() {
        addSubview(titleLabel)
        titleLabel.makeCenter(to: self)
        titleLabel.makeSuperView(withSafeArea: true)
    }

    private func isAnimating(_ animating: Bool) {
        if animating {
            main.async {
                self.titleLabel.isHidden = true
                self.activityIndicatorView.startAnimating()
            }
        } else {
            main.async {
                self.activityIndicatorView.stopAnimating()
                self.titleLabel.isHidden = false
            }
        }
    }

    private func networkChangesObserver() {
        let monitor = NWPathMonitor()
        monitor.startMonitoringNetworkChanges { [weak self] (status) in
            switch status {
                case .satisfied:
                    self?.isAnimating(true)
                case .unsatisfied:
                    self?.isAnimating(false)
                case .requiresConnection:
                    print("Requires Connection")
                default:
                    return
            }
        }
    }

    // MARK: - Objc Methods
    // MARK: - //
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Extension
extension DetailListFooterView {}
