//
//  SectionHeader.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/10/20.
//

import UIKit

// MARK: - Protocol
protocol SectionHeaderProtocol: AnyObject {
    func didPressSeeAllButton(section title: String)
    func didPressSeeAllButton(section title: String, homeSectionType type: HomeSectionType)
}

// MARK: - Class
final class SectionHeader: UICollectionReusableView {
    
    // MARK: - Declarations (Properties)
    weak var delegate: SectionHeaderProtocol?
    private var titleLabelLeadingConstraint: NSLayoutConstraint?
    private var seeAllButtonTrailingConstraint: NSLayoutConstraint?
    private var title = ""
    private var homeSectionType: HomeSectionType?
    private let padding: CGFloat = 8

    // MARK: - Declarations (UI)
    fileprivate let titleLabel: CustomLabel = {
        let label = CustomLabel(font: .boldSystemFont(ofSize: 22))
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    fileprivate lazy var seeAllButton: CustomSeeAllButton = {
        let button = CustomSeeAllButton(type: .system)
        button.isHidden = true
        button.addTarget(self, action: #selector(handleCustomButton(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .systemBackground
        configureTitleLabel()
        configureCustomSeeAllButton()
    }

    private func configureTitleLabel() {
        addSubview(titleLabel)
        titleLabel.makeCenterY(to: self)
        titleLabelLeadingConstraint = titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor)
        titleLabelLeadingConstraint?.isActive = true
    }

    private func configureCustomSeeAllButton() {
        addSubview(seeAllButton)
        seeAllButtonTrailingConstraint = seeAllButton.trailingAnchor.constraint(equalTo: trailingAnchor)
        seeAllButtonTrailingConstraint?.isActive = true
        seeAllButton.makeCenterY(to: self)
    }
    
    // MARK: - Methods
    func configure(title text: String, homeSectionType type: HomeSectionType? = .none) {
        title = text
        titleLabel.text = title

        if text == DetailSection.about.rawValue || text == DetailSection.info.rawValue {
            backgroundColor = .secondarySystemBackground
            titleLabelLeadingConstraint?.constant = 32
        } else {
            backgroundColor = .systemBackground
            titleLabelLeadingConstraint?.constant = .zero
        }
        homeSectionType = type
    }

    func showSeeAllButton(_ show: Bool = true) {
        if show {
            seeAllButton.isHidden = false
        } else {
            seeAllButton.isHidden = true
        }
    }

    func enableConstraint(_ enable: Bool) {
        if enable {
            titleLabelLeadingConstraint?.constant = padding
            seeAllButtonTrailingConstraint?.constant = -padding
        } else {
            titleLabelLeadingConstraint?.constant = .zero
            seeAllButtonTrailingConstraint?.constant = -16
        }
    }

    // MARK: - Objc Methods
    @objc private func handleCustomButton(_ sender: CustomSeeAllButton?) {
        guard let type = homeSectionType else {
            delegate?.didPressSeeAllButton(section: title)
            return
        }
        delegate?.didPressSeeAllButton(section: title, homeSectionType: type)
    }
   
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Extension
extension SectionHeaderProtocol {
    func didPressSeeAllButton(section title: String) {}
    func didPressSeeAllButton(section title: String, homeSectionType type: HomeSectionType) {}
}
