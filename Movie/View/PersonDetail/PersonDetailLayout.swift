//
//  PersonDetailLayout.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/28/20.
//

import UIKit

// MARK: - Class
final class PersonDetailLayout: UICollectionViewCompositionalLayout {
    static func layout() -> UICollectionViewCompositionalLayout {
        let smallerPadding: CGFloat = 16
        let padding: CGFloat = 32
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
        let itemContentInsets = NSDirectionalEdgeInsets(top: .zero, leading: .zero, bottom: .zero, trailing: smallerPadding)
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = itemContentInsets
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.6), heightDimension: .absolute(126))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        let headerHeight: CGFloat = 40
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(headerHeight))
        let section = NSCollectionLayoutSection(group: group)
        let supplementaryItems = [NSCollectionLayoutBoundarySupplementaryItem.init(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)]
        let sectionContentInsets = NSDirectionalEdgeInsets(top: .zero, leading: padding, bottom: padding, trailing: smallerPadding)
        section.boundarySupplementaryItems = supplementaryItems
        section.orthogonalScrollingBehavior = .continuousGroupLeadingBoundary
        section.contentInsets = sectionContentInsets
        return .init(section: section)
    }
}
