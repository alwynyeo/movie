//
//  SearchCell.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/26/20.
//

import SwiftUI
import SDWebImage

// MARK: - Class
final class SearchCell: UICollectionViewCell {
    // MARK: - Declarations (Properties)
    private var item: MediaItem? {
        didSet {
            setData()
        }
    }
    private var profileImageViewWidthConstraint: NSLayoutConstraint?
    private var imageViewWidth: CGFloat = 110

    // MARK: - Declarations (UI)
    fileprivate let imageView = CustomImageView()

    fileprivate let titleLabel = CustomLabel(text: "Title", numberOfLines: 2)

    fileprivate lazy var separatorView = CustomSeparatorView()

    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .clear
        configureImageView()
        configureTitleLabel()
        configureSeparatorView()
    }

    private func configureImageView() {
        let padding: CGFloat = 8
        addSubview(imageView)

        let constants = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        imageView.makeView(top: safeAreaLayoutGuide.topAnchor, leading: safeAreaLayoutGuide.leadingAnchor, trailing: nil, bottom: safeAreaLayoutGuide.bottomAnchor, constants: constants)
    }

    private func setProfileImageViewFrame(width: CGFloat, cornerRadius value: CGFloat) {
        let height: CGFloat = 62
        let bounds = CGRect(x: .zero, y: .zero, width: width, height: height)

        profileImageViewWidthConstraint?.isActive = false

        imageView.bounds = bounds
        imageView.layer.cornerRadius = imageView.bounds.width / value

        profileImageViewWidthConstraint = imageView.widthAnchor.constraint(equalToConstant: width)

        profileImageViewWidthConstraint?.isActive = true
    }

    private func configureTitleLabel() {
        addSubview(titleLabel)

        let padding: CGFloat = 16
        let constants = UIEdgeInsets(top: .zero, left: padding, bottom: .zero, right: .zero)

        titleLabel.makeView(top: nil, leading: imageView.safeAreaLayoutGuide.trailingAnchor, trailing: safeAreaLayoutGuide.trailingAnchor, bottom: nil, constants: constants)
        titleLabel.makeCenterY(to: imageView, withSafeArea: true)
    }

    private func configureSeparatorView() {
        addSubview(separatorView)

        let height: CGFloat = 0.6
        separatorView.heightAnchor.constraint(equalToConstant: height).isActive = true
        separatorView.makeView(top: nil, leading: titleLabel.safeAreaLayoutGuide.leadingAnchor, trailing: safeAreaLayoutGuide.trailingAnchor, bottom: safeAreaLayoutGuide.bottomAnchor)
    }

    // MARK: - Methods
    func configure(item: MediaItem) {
        self.item = item
    }

    private func setData() {
        setTitleLabel()
        setImageView()
    }

    private func setTitleLabel() {
        let mediaType = item?.media_type
        switch mediaType {
            case MediaType.movie.rawValue:
                titleLabel.text = item?.title
            default:
                titleLabel.text = item?.name
        }
    }

    private func setImageView() {
        let host = "https://image.tmdb.org/t/p/w500"
        let mediaType = item?.media_type
        switch mediaType {
            case MediaType.person.rawValue:
                setPersonImage(host: host)
            default:
                setFilmImage(host: host)
        }
    }

    private func setFilmImage(host: String) {
        imageViewWidth = 110
        setProfileImageViewFrame(width: imageViewWidth, cornerRadius: 30)
        imageView.configure(path: item?.backdrop_path ?? item?.poster_path)
    }

    private func setPersonImage(host: String) {
        imageViewWidth = 60
        setProfileImageViewFrame(width: imageViewWidth, cornerRadius: 2)
        imageView.configure(path: item?.profile_path)
    }

    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
