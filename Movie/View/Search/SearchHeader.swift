//
//  SearchHeader.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/26/20.
//

import UIKit

// MARK: - Protocol
protocol SearchHeaderProtocol: AnyObject {
    func didPressSeeAllButton(section: SearchSection?)
}

// MARK: - Class
final class SearchHeader: UICollectionReusableView {
    // MARK: - Declarations (Properties)
    private var section: SearchSection? {
        didSet {
            setData()
        }
    }
    weak var delegate: SearchHeaderProtocol?

    // MARK: - Declarations (UI)
    fileprivate let titleLabel = CustomLabel(font: .boldSystemFont(ofSize: 22))

    fileprivate lazy var seeAllButton: CustomSeeAllButton = {
        let button = CustomSeeAllButton(type: .system)
        button.addTarget(self, action: #selector(handleCustomButton(_:)), for: .touchUpInside)
        return button
    }()

    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .systemBackground
        configureTitleLabel()
        configureSeeAllButton()
    }

    private func configureTitleLabel() {
        addSubview(titleLabel)
        let constants = UIEdgeInsets(top: .zero, left: 8, bottom: .zero, right: .zero)
        titleLabel.makeView(top: nil, leading: safeAreaLayoutGuide.leadingAnchor, trailing: nil, bottom: safeAreaLayoutGuide.bottomAnchor, constants: constants)
    }

    private func configureSeeAllButton() {
        addSubview(seeAllButton)
        let constants = UIEdgeInsets(top: .zero, left: 8, bottom: .zero, right: .zero)
        seeAllButton.makeView(top: nil, leading: nil, trailing: safeAreaLayoutGuide.trailingAnchor, bottom: nil, constants: constants)
        seeAllButton.makeCenterY(to: titleLabel)
    }

    // MARK: - Methods
    func configure(section: SearchSection, numberOfItems items: Int) {
        self.section = section
        if items > 3 {
            seeAllButton.isHidden = false
        } else {
            seeAllButton.isHidden = true
        }
    }

    private func setData() {
        titleLabel.text = section?.rawValue
    }

    // MARK: - Objc Methods
    @objc private func handleCustomButton(_ sender: CustomSeeAllButton?) {
        delegate?.didPressSeeAllButton(section: section)
    }

    // MARK: - //
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Extension
extension SearchHeaderProtocol {
    func didPressSeeAllButton(section: SearchSection?) {}
}
