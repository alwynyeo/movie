//
//  SearchLayout.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/4/20.
//

import UIKit

// MARK: - Class
final class SearchLayout: UICollectionViewCompositionalLayout {
    // MARK: - Methods
    static func searchLayout() -> UICollectionViewCompositionalLayout {
        let padding: CGFloat = 24
        let width: CGFloat = 1
        let itemHeight: CGFloat = 78
        let groupHeight: CGFloat = 266
        let headerHeight: CGFloat = 46

        // 1. Item
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(width), heightDimension: .absolute(itemHeight))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)

        // 2. Group
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(width), heightDimension: .absolute(groupHeight))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])

        // 3. Section
        let section = NSCollectionLayoutSection(group: group)
        let sectionContentInsets = NSDirectionalEdgeInsets(top: padding / 4, leading: padding, bottom: .zero, trailing: padding)
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(width), heightDimension: .absolute(headerHeight))

        // Section header
        section.boundarySupplementaryItems = [NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)]
        section.contentInsets = sectionContentInsets

        let layout = UICollectionViewCompositionalLayout(section: section)
        return layout
    }
   
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Extension
extension SearchLayout {}
