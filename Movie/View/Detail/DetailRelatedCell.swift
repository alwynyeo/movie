//
//  DetailRelatedCell.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/10/20.
//

import UIKit

// MARK: - Class
class DetailRelatedCell: UICollectionViewCell {
    
    // MARK: - Declarations (Properties)
    private var item: MediaItem? {
        didSet {
            setData()
        }
    }
    // MARK: - Declarations (UI)
    fileprivate let imageView: CustomImageView = {
        let imageView = CustomImageView(cornerRadius: 10)
        return imageView
    }()
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .clear
        configureImageView()
    }

    private func configureImageView() {
        addSubview(imageView)
        imageView.makeSuperView()
    }
    
    // MARK: - Methods
    func configure(item: MediaItem) {
        self.item = item
    }

    private func setData() {
        imageView.configure(path: item?.backdrop_path ?? item?.poster_path)
    }

    // MARK: - Objc Methods
    
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
