//
//  DetailHeaderCell.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/10/20.
//

import UIKit

// MARK: - Protocol
protocol DetailHeaderCellProtocol: AnyObject {
    func didPressOverviewLabel(overview text: String)
}

// MARK: - Class
class DetailHeaderCell: UICollectionViewCell {
    
    // MARK: - Declarations (Properties)
    private var item: AnyHashable? {
        didSet {
            setData()
        }
    }
    weak var delegate: DetailHeaderCellProtocol?
    
    // MARK: - Declarations (UI)
    fileprivate let titleLabel = CustomLabel(font: .systemFont(ofSize: 14, weight: .heavy), text: "OVERVIEW", textColor: .systemGray)

    fileprivate lazy var overviewLabel = CustomLabel(text: "Description is unavailable", textAlignment: .natural, numberOfLines: 3)
    
    fileprivate lazy var stackView: CustomStackView = {
        let view = CustomStackView(axis: .vertical, distribution: .fillProportionally, spacing: 3, views: [
            titleLabel,
            overviewLabel,
        ])
        return view
    }()

    fileprivate lazy var tapGestureRecognizer: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapGestureRecognizer(_:)))
        tap.delegate = self
        return tap
    }()
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .systemBackground
        isUserInteractionEnabled = true
        addGestureRecognizer(tapGestureRecognizer)
        configureStackView()
    }

    private func configureStackView() {
        addSubview(stackView)
        let constants = UIEdgeInsets(top: 16, left: 32, bottom: .zero, right: 32)
        stackView.makeView(top: topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: bottomAnchor, constants: constants)
    }
    
    // MARK: - Methods
    func configure(item: AnyHashable) {
        self.item = item
    }

    private func setData() {
        if let item = self.item as? MediaItem {
            guard let overview = item.overview else {
                return
            }
            overviewLabel.text = overview
        } else {
            guard let item = self.item as? EpisodeItem else {
                return
            }
            guard let overview = item.overview else {
                return
            }
            overviewLabel.text = overview
        }
    }

    // MARK: - Objc Methods
    @objc private func handleTapGestureRecognizer(_ sender: UITapGestureRecognizer?) {
        guard let text = overviewLabel.text else {
            return
        }
        if text.count == .zero {
            return
        }
        delegate?.didPressOverviewLabel(overview: text)
    }

    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Extension
extension DetailHeaderCell: UIGestureRecognizerDelegate {}

extension DetailHeaderCellProtocol {
    func didPressOverviewLabel(overview text: String) {}
}
