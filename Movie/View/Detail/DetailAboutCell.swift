//
//  DetailAboutCell.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/10/20.
//

import UIKit

// MARK: - Class
class DetailAboutCell: UICollectionViewCell {
    
    // MARK: - Declarations (Properties)
    private var item: AnyHashable? {
        didSet {
            setData()
        }
    }
    
    // MARK: - Declarations (UI)
    fileprivate let titleLabel = CustomLabel(font: .boldSystemFont(ofSize: 20), text: "Title")

    fileprivate let genreLabel = CustomLabel(font: .boldSystemFont(ofSize: 14), text: "GENRE", textColor: .secondaryLabel)

    fileprivate let overviewLabel = CustomLabel(font: .systemFont(ofSize: 14), text: "Description is unavailable.", textColor: .secondaryLabel, textAlignment: .natural, numberOfLines: 4)

    fileprivate lazy var aboutView: UIView = {
        let width = self.frame.width
        let frame = CGRect(x: .zero, y: .zero, width: width, height: width)
        let view = UIView(frame: frame)
        view.layer.cornerRadius = 10
        view.backgroundColor = .tertiarySystemBackground
        return view
    }()

    fileprivate lazy var stackView: CustomStackView = {
        let view = CustomStackView(axis: .vertical, spacing: 1, views: [
            titleLabel,
            genreLabel,
        ])
        return view
    }()

    fileprivate let separatorView = CustomSeparatorView()
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .secondarySystemBackground
        configureAboutView()
        configureStackView()
        configureOverviewLabel()
        configureSeparatorView()
    }

    private func configureAboutView() {
        addSubview(aboutView)
        let padding: CGFloat = 32
        let constants = UIEdgeInsets(top: .zero, left: padding, bottom: padding, right: padding)
        aboutView.makeView(top: topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: bottomAnchor, constants: constants)
    }

    private func configureStackView() {
        aboutView.addSubview(stackView)
        let padding: CGFloat = 16
        let constants = UIEdgeInsets(top: padding, left: padding, bottom: .zero, right: padding)
        stackView.makeView(top: aboutView.topAnchor, leading: aboutView.leadingAnchor, trailing: aboutView.trailingAnchor, bottom: nil, constants: constants)
    }

    private func configureOverviewLabel() {
        aboutView.addSubview(overviewLabel)
        let padding: CGFloat = 16
        let constants = UIEdgeInsets(top: padding, left: padding, bottom: 8, right: padding)
        overviewLabel.makeView(top: stackView.bottomAnchor, leading: aboutView.leadingAnchor, trailing: aboutView.trailingAnchor, bottom: aboutView.bottomAnchor, constants: constants)
    }

    private func configureSeparatorView() {
        addSubview(separatorView)
        let width = frame.width - 64
        let height: CGFloat = 0.6
        let size = CGSize(width: width, height: height)
        let padding: CGFloat = 32
        let constants = UIEdgeInsets(top: .zero, left: padding, bottom: .zero, right: padding)
        separatorView.makeSize(size: size)
        separatorView.makeView(top: nil, leading: leadingAnchor, trailing: trailingAnchor, bottom: bottomAnchor, constants: constants)
    }

    // MARK: - Methods
    func configure(item: AnyHashable) {
        self.item = item
    }

    private func setData() {
        if let item = self.item as? MediaItem {
            titleLabel.text = item.title ?? item.name
            overviewLabel.text = item.overview
        } else {
            guard let item = self.item as? EpisodeItem else {
                return
            }
            titleLabel.text = item.name
            overviewLabel.text = item.overview
        }
    }

    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
