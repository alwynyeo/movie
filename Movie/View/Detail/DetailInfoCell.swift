//
//  DetailInfoCell.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/18/20.
//

import UIKit

// MARK: - Class
class DetailInfoCell: UICollectionViewCell {
    
    // MARK: - Declarations (Properties)
    private var item: AnyHashable? {
        didSet {
            setData()
        }
    }
    private var releasedDate = ""

    // MARK: - Declarations (UI)
    fileprivate lazy var infoView: UIView = {
        let frame = self.frame
        let view = UIView(frame: frame)
        view.backgroundColor = .clear
        return view
    }()

    fileprivate let statusLabel: UILabel = {
        let label = UILabel()
        label.textColor = .secondaryLabel
        label.font = .systemFont(ofSize: 13)
        label.backgroundColor = .clear
        label.numberOfLines = .zero
        return label
    }()

    fileprivate let releasedDateLabel: UILabel = {
        let label = UILabel()
        label.textColor = .secondaryLabel
        label.font = .systemFont(ofSize: 13)
        label.backgroundColor = .clear
        label.numberOfLines = .zero
        return label
    }()

    fileprivate let runtimeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .secondaryLabel
        label.font = .systemFont(ofSize: 13)
        label.backgroundColor = .clear
        label.numberOfLines = .zero
        return label
    }()

    fileprivate let seasonLabel: UILabel = {
        let label = UILabel()
        label.textColor = .secondaryLabel
        label.font = .systemFont(ofSize: 13)
        label.backgroundColor = .clear
        label.numberOfLines = .zero
        return label
    }()

    fileprivate let episodeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .secondaryLabel
        label.font = .systemFont(ofSize: 13)
        label.backgroundColor = .clear
        label.numberOfLines = .zero
        return label
    }()

    fileprivate let ratingLabel: UILabel = {
        let label = UILabel()
        label.textColor = .secondaryLabel
        label.font = .systemFont(ofSize: 13)
        label.backgroundColor = .clear
        label.numberOfLines = .zero
        return label
    }()

    fileprivate let languageLabel: UILabel = {
        let label = UILabel()
        label.textColor = .secondaryLabel
        label.font = .systemFont(ofSize: 13)
        label.backgroundColor = .clear
        label.numberOfLines = .zero
        return label
    }()

    fileprivate lazy var stackView: CustomStackView = {
        let view = CustomStackView(axis: .vertical, distribution: .fillProportionally, spacing: 2, views: [
            statusLabel,
            releasedDateLabel,
            runtimeLabel,
            seasonLabel,
            episodeLabel,
            ratingLabel,
            languageLabel
        ])
        return view
    }()

    private let titleAttributes = [
        NSAttributedString.Key.foregroundColor: UIColor.label,
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13),
    ]

    private let dataAttributes = [
        NSAttributedString.Key.foregroundColor: UIColor.secondaryLabel,
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13),
    ]

    private let paragraphStyle: NSMutableParagraphStyle = {
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 2
        return style
    }()

    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .secondarySystemBackground
        configureInfoView()
        configureStackView()
    }

    private func configureInfoView() {
        addSubview(infoView)
        let padding: CGFloat = 32
        let constants = UIEdgeInsets(top: .zero, left: padding, bottom: padding, right: padding)
        infoView.makeView(top: topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: bottomAnchor, constants: constants)
    }

    private func configureStackView() {
        infoView.addSubview(stackView)
        stackView.makeView(top: infoView.topAnchor, leading: infoView.leadingAnchor, trailing: infoView.trailingAnchor, bottom: infoView.bottomAnchor)
    }

    // MARK: - Methods
    func configure(item: AnyHashable) {
        self.item = item
    }

    private func setData() {
        setStatusInfo()
        setReleasedDateInfo()
        setRuntimeInfo()
        setSeasonInfo()
        setEpisodeInfo()
        setRatingInfo()
        setLanguageInfo()
    }

    private func setStatusInfo() {
        if let item = self.item as? MediaItem {
            let status = item.status ?? "To be specified"
            let title = "Status"
            let attributedText = NSMutableAttributedString(string: title, attributes: titleAttributes)
            let spacing = NSAttributedString(string: "\n")
            let statusAttributedText = NSAttributedString(string: status, attributes: dataAttributes)
            attributedText.append(spacing)
            attributedText.append(statusAttributedText)
            attributedText.addAttribute(.paragraphStyle, value: paragraphStyle, range: range(text: attributedText))
            statusLabel.attributedText = attributedText
        }
    }

    private func setReleasedDateInfo() {
        if let item = self.item as? MediaItem {
            releasedDate = (item.release_date ?? item.first_air_date) ?? "To be continued"
        } else {
            guard let item = self.item as? EpisodeItem else {
                return
            }
            releasedDate = item.air_date ?? "To be continued"
        }
        var title = releasedDate > Date.today() ? "To be released\t" : "Released\t"
        if releasedDate.isEmpty {
            releasedDate = "To be specified"
            title = "To be released"
        }
        let attributedText = NSMutableAttributedString(string: title, attributes: titleAttributes)
        let spacing = NSAttributedString(string: "\n")
        let releasedDateAttributedText = NSAttributedString(string: releasedDate, attributes: dataAttributes)
        attributedText.append(spacing)
        attributedText.append(releasedDateAttributedText)
        attributedText.addAttribute(.paragraphStyle, value: paragraphStyle, range: range(text: attributedText))
        releasedDateLabel.attributedText = attributedText
    }

    private func setRuntimeInfo() {
        if let item = self.item as? MediaItem {
            guard let runtime = item.runtime else {
                return
            }
            let title = "Run Time"
            let attributedText = NSMutableAttributedString(string: title, attributes: titleAttributes)
            let spacing = NSAttributedString(string: "\n")
            let runtimeAttributedText = NSAttributedString(string: formattedRuntime(runtime: runtime), attributes: dataAttributes)
            attributedText.append(spacing)
            attributedText.append(runtimeAttributedText)
            attributedText.addAttribute(.paragraphStyle, value: paragraphStyle, range: range(text: attributedText))
            runtimeLabel.attributedText = attributedText
        }
    }

    private func setSeasonInfo() {
        var seasons = 0
        var title = ""
        if let item = self.item as? MediaItem {
            guard let seasonCount = item.number_of_seasons else {
                return
            }
            seasons = seasonCount
            title = "Number of seasons"
        } else {
            guard let item = self.item as? EpisodeItem else {
                return
            }
            seasons = item.season_number ?? .zero
            title = seasons == .zero ? "Intro" : "Season number"
        }
        let season = seasons == .zero ? "No season number" : "\(seasons)"
        let attributedText = NSMutableAttributedString(string: title, attributes: titleAttributes)
        let spacing = NSAttributedString(string: "\n")
        let statusAttributedText = NSAttributedString(string: season, attributes: dataAttributes)
        attributedText.append(spacing)
        attributedText.append(statusAttributedText)
        attributedText.addAttribute(.paragraphStyle, value: paragraphStyle, range: range(text: attributedText))
        seasonLabel.attributedText = attributedText
    }

    private func setEpisodeInfo() {
        var episodes = 0
        var title = ""
        if let item = self.item as? MediaItem {
            guard let episodeCount = item.number_of_episodes else {
                return
            }
            episodes = episodeCount
            title = "Number of episodes"
        } else {
            guard let item = self.item as? EpisodeItem else {
                return
            }
            episodes = item.episode_number ?? .zero
            title = "Episode number"
        }
        let attributedText = NSMutableAttributedString(string: title, attributes: titleAttributes)
        let spacing = NSAttributedString(string: "\n")
        let statusAttributedText = NSAttributedString(string: "\(episodes)", attributes: dataAttributes)
        attributedText.append(spacing)
        attributedText.append(statusAttributedText)
        attributedText.addAttribute(.paragraphStyle, value: paragraphStyle, range: range(text: attributedText))
        episodeLabel.attributedText = attributedText
    }

    private func setRatingInfo() {
        var rating = ""
        if let item = self.item as? MediaItem {
            rating = releasedDate > Date.today() ? "No rating" : "\(item.vote_average ?? .zero)"
        } else {
            guard let item = self.item as? EpisodeItem else {
                return
            }
            rating = releasedDate > Date.today() ? "No rating" : "\(item.vote_average ?? .zero)"
        }
        if rating.isEmpty {
            rating = "No rating"
        }
        let title = "Rating"
        let attributedText = NSMutableAttributedString(string: title, attributes: titleAttributes)
        let spacing = NSAttributedString(string: "\n")
        let ratingAttributedText = NSAttributedString(string: rating, attributes: dataAttributes)
                attributedText.append(spacing)
        attributedText.append(ratingAttributedText)
        attributedText.addAttribute(.paragraphStyle, value: paragraphStyle, range: range(text: attributedText))
        ratingLabel.attributedText = attributedText
    }

    private func setLanguageInfo() {
        if let item = self.item as? MediaItem {
            let languageCode = item.original_language ?? "Unavailable"
            let language = languageCode == "cn" ? "Cantonese" : Locale.current.localizedString(forLanguageCode: languageCode) ?? languageCode
            let title = "Language"
            let attributedText = NSMutableAttributedString(string: title, attributes: titleAttributes)
            let spacing = NSAttributedString(string: "\n")
            let ratingAttributedText = NSAttributedString(string: language, attributes: dataAttributes)
            attributedText.append(spacing)
            attributedText.append(ratingAttributedText)
            attributedText.addAttribute(.paragraphStyle, value: paragraphStyle, range: range(text: attributedText))
            languageLabel.attributedText = attributedText
        }
    }

    private func formattedRuntime(runtime: Int) -> String {
        guard runtime != .zero else {
            return "To be specified"
        }
        let hours = runtime / 60
        let minutes = runtime % 60
        var formattedRuntime = "\(hours) hr \(minutes) min"
        if hours == .zero && minutes != .zero {
            formattedRuntime = "\(minutes) min"
        } else if hours != .zero && minutes == .zero {
            formattedRuntime = "\(hours) hr"
        }
        return formattedRuntime
    }

    private func range(text: NSAttributedString) -> NSRange {
        return .init(location: .zero, length: text.length)
    }

    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
