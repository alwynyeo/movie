//
//  DetailLayout.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/10/20.
//

import UIKit

class DetailLayout: UICollectionViewCompositionalLayout {
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let layoutAttributes = super.layoutAttributesForElements(in: rect)

        layoutAttributes?.forEach({ [weak self] (attributes) in
            guard let _ = self else {
                return
            }

            if attributes.representedElementKind == UICollectionView.elementKindSectionHeader {
                if attributes.indexPath.section == .zero {
                    guard let collectionView = collectionView else {
                        return
                    }
                    let contentOffsetY = collectionView.contentOffset.y
                    let width = attributes.frame.width
                    var defaultHeight: CGFloat = UIDevice.current.hasNotch ? 700 : 565
                    guard contentOffsetY < .zero else {
                        return
                    }
                    defaultHeight -= contentOffsetY
                    attributes.frame = CGRect(x: .zero, y: contentOffsetY, width: width, height: defaultHeight)
                }
            }
        })

        return layoutAttributes
    }

    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        true
    }
}
