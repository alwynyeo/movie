//
//  DetailSeasonCell.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/9/20.
//

import UIKit

// MARK: - Class
class DetailSeasonCell: UICollectionViewCell {
    
    // MARK: - Declarations (Properties)
    private var item: SeasonItem? {
        didSet {
            setData()
        }
    }
    private lazy var width = frame.width
    private lazy var height = frame.height / 2
    
    // MARK: - Declarations (UI)
    fileprivate lazy var imageView: CustomImageView = {
        let imageView = CustomImageView(cornerRadius: 10)
        return imageView
    }()

    fileprivate let seasonNumberLabel = CustomLabel(font: .boldSystemFont(ofSize: 14), text: "Season number", textColor: .secondaryLabel)

    fileprivate let titleLabel = CustomLabel(font: .boldSystemFont(ofSize: 17), text: "Title")

    fileprivate let overviewLabel = CustomLabel(font: .systemFont(ofSize: 14), text: "The description is unavailable.", textColor: .secondaryLabel, numberOfLines: 3)

    fileprivate let episodeLabel = CustomLabel(font: .boldSystemFont(ofSize: 14), text: "Number of episodes", textColor: .secondaryLabel)

    fileprivate lazy var stackView: CustomStackView = {
        let view = CustomStackView(axis: .vertical, distribution: .fillProportionally, views: [
            seasonNumberLabel,
            titleLabel,
            overviewLabel,
            episodeLabel,
        ])
        return view
    }()

    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .systemBackground
        configureImageView()
        configureStackView()
    }

    private func configureImageView() {
        addSubview(imageView)
        let size = CGSize(width: width, height: height)
        imageView.makeSize(size: size)
        imageView.makeView(top: topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil)
    }

    private func configureStackView() {
        addSubview(stackView)
        let constants = UIEdgeInsets(top: 8, left: .zero, bottom: 16, right: .zero)
        stackView.makeView(top: imageView.bottomAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: bottomAnchor, constants: constants)
    }

    // MARK: - Methods
    func configure(item: SeasonItem) {
        self.item = item
    }

    private func setData() {
        setSeasonInfo()
        setImageView()
    }

    private func setSeasonInfo() {
        let season = item?.season_number ?? .zero
        let name = item?.name ?? "Title"
        let overview = item?.overview ?? "The description is unavailable"
        let episodes = item?.episode_count ?? .zero
        let date = item?.air_date ?? "Air date is unavailable"
        seasonNumberLabel.text = season > .zero ? "Season \(season)" : "Intro"
        titleLabel.text = name
        overviewLabel.text = overview.isEmpty ? "The description is unavailable." : overview
        episodeLabel.text = date + " • " + (episodes > 1 ? "Episodes: \(episodes)" : "Episode: \(episodes)")
    }

    private func setImageView() {
        imageView.configure(path: item?.poster_path)
    }
    
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
