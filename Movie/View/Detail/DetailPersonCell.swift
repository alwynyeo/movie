//
//  DetailPersonCell.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/10/20.
//

import UIKit

// MARK: - Class
class DetailPersonCell: UICollectionViewCell {
    
    // MARK: - Declarations (Properties)
    private var item: CastItem? {
        didSet {
            setData()
        }
    }
    
    // MARK: - Declarations (UI)
    fileprivate lazy var imageView: CustomImageView = {
        let width = frame.width
        let frame = CGRect(x: .zero, y: .zero, width: width, height: width)
        let cornerRadius = width / 2
        let imageView = CustomImageView(frame: frame, cornerRadius: cornerRadius)
        return imageView
    }()

    fileprivate let nameLabel = CustomLabel(text: "Unavailable", textAlignment: .center)

    fileprivate let characterLabel = CustomLabel(font: .systemFont(ofSize: 14), text: "Unavailable", textColor: .secondaryLabel, textAlignment: .center, numberOfLines: 3)
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .systemBackground
        configureImageView()
        configureNameLabel()
        configureCharacterLabel()
    }

    private func configureImageView() {
        addSubview(imageView)
        let width = frame.width
        let size = CGSize(width: width, height: width)
        imageView.makeSize(size: size)
        imageView.makeView(top: topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil)
    }

    private func configureNameLabel() {
        addSubview(nameLabel)
        let constants = UIEdgeInsets(top: 8, left: .zero, bottom: .zero, right: .zero)
        nameLabel.makeView(top: imageView.bottomAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, constants: constants)
    }

    private func configureCharacterLabel() {
        addSubview(characterLabel)
        let constants = UIEdgeInsets(top: 4, left: .zero, bottom: .zero, right: .zero)
        characterLabel.makeView(top: nameLabel.bottomAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, constants: constants)
    } 

    // MARK: - Methods
    func configure(item: CastItem) {
        self.item = item
    }

    private func setData() {
        if let name = item?.name {
            nameLabel.text = name
        }
        if let character = item?.character {
            characterLabel.text = character
        } else if let department = item?.department {
            characterLabel.text = department
        } else if let job = item?.job {
            characterLabel.text = job
        }
        setImageView()
    }

    private func setImageView() {
        let host = "https://image.tmdb.org/t/p/original"
        guard let path = item?.profile_path else {
            imageView.image = #imageLiteral(resourceName: "defaultImage")
            return
        }
        let file = host + path
        guard let url = URL(string: file) else {
            Errors.error(in: FileIndentifier.SearchCell.rawValue, at: #line)
            return
        }
        imageView.sd_setImage(with: url)
    }
    
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
