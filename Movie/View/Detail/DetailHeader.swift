//
//  DetailHeader.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/14/20.
//

import UIKit

// MARK: - Class
class DetailHeader: UICollectionReusableView {
    
    // MARK: - Declarations (Properties)
    private var item: AnyHashable? {
        didSet {
            setData()
        }
    }
    
    // MARK: - Declarations (UI)
    fileprivate let imageView = CustomImageView()

    fileprivate let activityIndicatorView = CustomActivityIndicatorView(style: .medium)
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .systemBackground
        configureImageView()
        configureActivityIndicatorView()
    }

    private func configureImageView() {
        addSubview(imageView)
        imageView.makeSuperView()
    }

    private func configureActivityIndicatorView() {
        addSubview(activityIndicatorView)
        activityIndicatorView.makeSuperView()
    }
    
    // MARK: - Methods
    func configure(item: AnyHashable) {
        self.item = item
    }

    private func setData() {
        if let item = self.item as? MediaItem {
            guard let path = item.poster_path ?? item.backdrop_path else {
                activityIndicatorView.stopAnimating()
                return
            }
            setFilmImage(path: path)
        } else {
            let item = self.item as? EpisodeItem
            guard let path = item?.still_path else {
                activityIndicatorView.stopAnimating()
                return
            }
            setFilmImage(path: path)
        }
    }

    private func setFilmImage(path: String) {
        let host = "https://image.tmdb.org/t/p/original"
        let file = host + path
        guard let url = URL(string: file) else {
            return
        }
        imageView.sd_setImage(with: url) { [weak self] (_, _, _, _) in
            self?.activityIndicatorView.stopAnimating()
        }
    }

    // MARK: - Objc Methods
   
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Extension
extension DetailHeader {}
