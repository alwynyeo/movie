//
//  FavoriteCell.swift
//  Movie
//
//  Created by Alwyn Yeo on 1/1/21.
//

import UIKit

// MARK: - Class
class FavoriteCell: UICollectionViewCell {
    
    // MARK: - Declarations (Properties)
    private var item: MediaItem? {
        didSet {
            setData()
        }
    }
    
    // MARK: - Declarations (UI)
    fileprivate let imageView = CustomImageView(cornerRadius: 5)

    fileprivate let titleLabel = CustomLabel(text: "Title")

    fileprivate let dateLabel = CustomLabel(font: .systemFont(ofSize: 14), text: "Released Date", textColor: .secondaryLabel)

    fileprivate lazy var stackView: CustomStackView = {
        let view = CustomStackView(axis: .vertical, views: [
            titleLabel,
            dateLabel,
        ])
        return view
    }()

    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .clear
        isUserInteractionEnabled = true
        configureImageView()
        configureStackView()
    }

    private func configureImageView() {
        addSubview(imageView)
        let width = frame.width - 16
        let height = frame.height / 1.38
        let size = CGSize(width: width, height: height)
        let padding: CGFloat = 8
        let constants = UIEdgeInsets(top: padding, left: padding, bottom: .zero, right: padding)
        imageView.makeSize(size: size)
        imageView.makeView(top: topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, constants: constants)
    }

    private func configureStackView() {
        addSubview(stackView)
        let padding: CGFloat = 8
        let constants = UIEdgeInsets(top: 4, left: padding, bottom: padding, right: padding)
        stackView.makeView(top: imageView.bottomAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: bottomAnchor, constants: constants)
    }

    // MARK: - Methods
    func configure(item: MediaItem) {
        self.item = item
    }

    private func setData() {
        let title = (item?.title ?? item?.name) ?? "Title"
        titleLabel.text = title
        dateLabel.text = item?.release_date ?? item?.first_air_date
        setImageView()
        imageView.configure(path: item?.backdrop_path ?? item?.poster_path)
    }

    private func setImageView() {
        let host = "https://image.tmdb.org/t/p/original"
        guard let path = item?.backdrop_path ?? item?.poster_path else {
            return
        }
        let file = host + path
        guard let url = URL(string: file) else {
            Errors.error(in: FileIndentifier.SearchCell.rawValue, at: #line)
            return
        }
        imageView.sd_setImage(with: url)
    }
    
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
