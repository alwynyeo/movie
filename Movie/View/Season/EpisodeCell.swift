//
//  EpisodeCell.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/24/20.
//

import UIKit

// MARK: - Class
class EpisodeCell: UICollectionViewCell {
    
    // MARK: - Declarations (Properties)
    private var item: EpisodeItem? {
        didSet {
            setData()
        }
    }
    
    // MARK: - Declarations (UI)
    fileprivate let imageView: CustomImageView = {
        let imageView = CustomImageView(cornerRadius: 10)
        return imageView
    }()

    fileprivate let titleLabel = CustomLabel(font: .boldSystemFont(ofSize: 17), text: "Title")

    fileprivate let overviewLabel = CustomLabel(font: .systemFont(ofSize: 14), text: "Description", textColor: .secondaryLabel, numberOfLines: 2)

    fileprivate let dateLabel = CustomLabel(font: .systemFont(ofSize: 14), text: "Air date", textColor: .secondaryLabel)

    fileprivate lazy var stackView: CustomStackView = {
        let view = CustomStackView(axis: .vertical, distribution: .fillProportionally, spacing: 2, views: [
            titleLabel,
            overviewLabel,
            dateLabel,
        ])
        return view
    }()
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .systemBackground
        configureImageView()
        configureStackView()
    }

    private func configureImageView() {
        addSubview(imageView)
        let width: CGFloat = 80
        let size = CGSize(width: width, height: width)
        imageView.makeCenterY(to: self)
        imageView.makeView(top: nil, leading: leadingAnchor, trailing: nil, bottom: nil)
        imageView.makeSize(size: size)
    }

    private func configureStackView() {
        addSubview(stackView)
        let constants = UIEdgeInsets(top: .zero, left: 16, bottom: .zero, right: .zero)
        stackView.makeView(top: imageView.topAnchor, leading: imageView.trailingAnchor, trailing: trailingAnchor, bottom: imageView.bottomAnchor, constants: constants)
    }

    // MARK: - Methods
    func configure(item: EpisodeItem) {
        self.item = item
    }

    private func setData() {
        setEpisodeInfo()
        setImageView()
    }

    private func setEpisodeInfo() {
        let name = item?.name ?? "Title"
        let episode = item?.episode_number ?? .zero
        let date = item?.air_date ?? "To be announced"
        let overview = item?.overview ?? "The description is unavailable."
        titleLabel.text = name.isEmpty ? "Title" : name
        overviewLabel.text = overview.isEmpty ? "The description is unavailable." : overview
        dateLabel.font = date.isEmpty ? .boldSystemFont(ofSize: 14) : .systemFont(ofSize: 14)
        dateLabel.text = "Episode \(episode) • " + (date.isEmpty ? "To be announced" : date)
    }

    private func setImageView() {
        imageView.configure(path: item?.still_path)
    }
    
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
