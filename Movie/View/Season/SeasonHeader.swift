//
//  SeasonHeaderView.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/24/20.
//

import UIKit

// MARK: - Protocol
protocol SeasonHeaderProtocol: AnyObject {
    func didPressOverviewLabel(overview text: String)
}

// MARK: - Class
class SeasonHeader: UICollectionReusableView {
    
    // MARK: - Declarations (Properties)
    private var item: SeasonDetailItem? {
        didSet {
            setData()
        }
    }
    weak var delegate: SeasonHeaderProtocol?
    
    // MARK: - Declarations (UI)
    fileprivate let imageView: CustomImageView = {
        let imageView = CustomImageView(cornerRadius: 20)
        return imageView
    }()

    fileprivate let titleLabel = CustomLabel(font: .boldSystemFont(ofSize: 22), text: "Title")

    fileprivate lazy var overviewLabel: CustomLabel = {
        let label = CustomLabel(font: .systemFont(ofSize: 14), text: "Description", numberOfLines: 2)
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(tapGestureRecognizer)
        return label
    }()

    fileprivate let episodeCountLabel = CustomLabel(font: .boldSystemFont(ofSize: 14), text: "Number of episodes", textColor: .secondaryLabel)

    fileprivate let seasonNumberLabel = CustomLabel(font: .boldSystemFont(ofSize: 14), text: "Season number", textColor: .secondaryLabel)

    fileprivate lazy var stackView: CustomStackView = {
        let view = CustomStackView(axis: .vertical, distribution: .fillProportionally, spacing: 2, views: [
            titleLabel,
            overviewLabel,
            seasonNumberLabel,
            episodeCountLabel,
        ])
        return view
    }()

    fileprivate lazy var tapGestureRecognizer: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapGestureRecognizer(_:)))
        return tap
    }()

    fileprivate let separatorView = CustomSeparatorView()
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .systemBackground
        configureImageView()
        configureStackView()
        configureSeparatorView()
    }

    private func configureImageView() {
        addSubview(imageView)
        let size = CGSize(width: 100, height: 100)
        let constants = UIEdgeInsets(top: .zero, left: 32, bottom: .zero, right: .zero)
        imageView.makeSize(size: size)
        imageView.makeView(top: topAnchor, leading: leadingAnchor, trailing: nil, bottom: nil, constants: constants)
    }

    private func configureStackView() {
        addSubview(stackView)
        let constants = UIEdgeInsets(top: .zero, left: 16, bottom: .zero, right: 32)
        stackView.makeView(top: imageView.topAnchor, leading: imageView.trailingAnchor, trailing: trailingAnchor, bottom: imageView.bottomAnchor, constants: constants)
    }

    private func configureSeparatorView() {
        addSubview(separatorView)
        let width = frame.width - 64
        let height: CGFloat = 0.6
        let size = CGSize(width: width, height: height)
        let padding: CGFloat = 32
        let constants = UIEdgeInsets(top: .zero, left: padding, bottom: 8, right: padding)
        separatorView.makeSize(size: size)
        separatorView.makeView(top: nil, leading: leadingAnchor, trailing: trailingAnchor, bottom: bottomAnchor, constants: constants)
    }

    // MARK: - Methods
    func configure(item: SeasonDetailItem) {
        self.item = item
    }

    private func setData() {
        setSeasonInfo()
        setImageView()
    }

    private func setSeasonInfo() {
        let name = item?.name ?? "Title"
        let overview = item?.overview ?? "The description is unavailable."
        let season = item?.season_number ?? .zero
        let date = item?.air_date ?? "Date is unavailable"
        let episodes = item?.episodes?.count ?? .zero
        titleLabel.text = name
        overviewLabel.text = overview.isEmpty ? "The description is unavailable." : overview
        seasonNumberLabel.text = season > .zero ? "Season \(season)" : "Intro"
        episodeCountLabel.text = date + " • " + (episodes > 1 ? "Episodes: \(episodes)" : "Episode: \(episodes)")
    }

    private func setImageView() {
        imageView.configure(path: item?.poster_path)
    }

    // MARK: - Objc Methods
    @objc private func handleTapGestureRecognizer(_ sender: UITapGestureRecognizer?) {
        guard let overview = item?.overview else {
            return
        }
        if overview.isEmpty {
            return
        }
        delegate?.didPressOverviewLabel(overview: overview)
    }
   
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Extension
extension SeasonHeaderProtocol {
    func didPressOverviewLabel(overview text: String) {}
}
