//
//  CustomSeeAllButton.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/18/20.
//

import UIKit

// MARK: - Class
class CustomSeeAllButton: UIButton {
    
    // MARK: - Declarations (Properties)
    
    // MARK: - Declarations (UI)
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    convenience init(title: String) {
        self.init(frame: .zero)
        setTitle(title, for: .normal)
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .systemBackground
        setTitle("See All", for: .normal)
        setTitleColor(.systemBlue, for: .normal)
    }
    
    // MARK: - Methods
   
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Extension
extension CustomSeeAllButton {}
