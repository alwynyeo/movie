//
//  CustomEmptyView.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/30/20.
//

import UIKit

// MARK: - Class
final class CustomEmptyView: UIView {
    
    // MARK: - Declarations (Properties)
    
    // MARK: - Declarations (UI)
    fileprivate let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .systemGray
        label.font = .systemFont(ofSize: 17)
        label.textAlignment = .center
        label.numberOfLines = .zero
        return label
    }()

    let activityIndicatorView = CustomActivityIndicatorView(style: .medium)
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .systemBackground
        configureTitleLabel()
        configureActivityIndicatorView()
    }

    private func configureTitleLabel() {
        addSubview(titleLabel)

        let constant: CGFloat = -50
        let constants = UIEdgeInsets(top: .zero, left: 16, bottom: .zero, right: 16)
        titleLabel.makeCenter(to: self, centerYConstant: constant)
        titleLabel.makeView(top: nil, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, constants: constants)
    }

    private func configureActivityIndicatorView() {
        addSubview(activityIndicatorView)
        activityIndicatorView.makeCenter(to: titleLabel)
    }
    
    // MARK: - Methods
    func configure(isLoading: Bool, message: String?) {
        if isLoading {
            titleLabel.text = message
            activityIndicatorView.startAnimating()
        } else {
            if let message = message {
                titleLabel.text = message
            } else {
                titleLabel.text = nil
            }
            activityIndicatorView.stopAnimating()
        }
    }

    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Extension
extension CustomEmptyView {}
