//
//  CustomStackView.swift
//  Movie
//
//  Created by Alwyn Yeo on 1/11/21.
//

import UIKit

// MARK: - Class
class CustomStackView: UIStackView {
    
    // MARK: - Declarations (Properties)
    
    // MARK: - Declarations (UI)
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    convenience init(axis: NSLayoutConstraint.Axis = .horizontal, distribution: UIStackView.Distribution = .fill, alignment: UIStackView.Alignment = .fill, spacing: CGFloat = .zero, views: [UIView]) {
        self.init(frame: .zero)
        setUp(axis: axis, distribution: distribution, alignment: alignment, spacing: spacing, views: views)
    }

    // MARK: - Configuration (UI)
    private func setUp(axis: NSLayoutConstraint.Axis, distribution: UIStackView.Distribution, alignment: UIStackView.Alignment, spacing: CGFloat = .zero, views: [UIView]) {
        self.axis = axis
        self.distribution = distribution
        self.alignment = alignment
        self.spacing = spacing
        views.forEach { (view) in
            self.addArrangedSubview(view)
        }
    }
    
    // MARK: - Methods
   
    // MARK: - //
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Extension
extension CustomStackView {}
