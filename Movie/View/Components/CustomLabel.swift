//
//  CustomLabel.swift
//  Movie
//
//  Created by Alwyn Yeo on 1/15/21.
//

import UIKit

// MARK: - Class
class CustomLabel: UILabel {
    
    // MARK: - Declarations (Properties)
    
    // MARK: - Declarations (UI)
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView(font: .systemFont(ofSize: 17), text: "Title")
    }

    convenience init(frame: CGRect = .zero, font: UIFont = .systemFont(ofSize: 17), text: String? = nil, textColor: UIColor = .label, textAlignment: NSTextAlignment = .left, numberOfLines: Int = 1) {
        self.init(frame: frame)
        setupView(font: font, text: text, textColor: textColor, textAlignment: textAlignment, numberOfLines: numberOfLines)
    }
    
    // MARK: - Configuration (UI)
    private func setupView(font: UIFont, text: String?, textColor: UIColor = .label, textAlignment: NSTextAlignment = .left, numberOfLines: Int = 1) {
        backgroundColor = .clear
        self.font = font
        self.text = text
        self.textColor = textColor
        self.textAlignment = textAlignment
        self.numberOfLines = numberOfLines
    }
    
    // MARK: - Methods
   
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Extension
extension CustomLabel {}
