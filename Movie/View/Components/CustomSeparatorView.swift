//
//  CustomSeparatorView.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/25/20.
//

import UIKit

// MARK: - Class
final class CustomSeparatorView: UIView {
    
    // MARK: - Declarations (Properties)
    
    // MARK: - Declarations (UI)
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .separator
    }
    
    // MARK: - Methods
   
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Extension
extension CustomSeparatorView {}
