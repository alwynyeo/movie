//
//  CustomImageView.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/29/20.
//

import UIKit

// MARK: - Class
class CustomImageView: UIImageView {

    // MARK: - Declarations (Properties)
    private let host = "https://image.tmdb.org/t/p/original"

    // MARK: - Declarations (UI)

    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    convenience init(cornerRadius radius: CGFloat = .zero) {
        self.init(frame: .zero)
        layer.cornerRadius = radius
    }

    convenience init(frame: CGRect, cornerRadius radius: CGFloat) {
        self.init(frame: frame)
        layer.cornerRadius = radius
    }

    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .systemGray
        image = #imageLiteral(resourceName: "defaultImage")
        contentMode = .scaleAspectFill
        clipsToBounds = true
        layer.masksToBounds = true
        translatesAutoresizingMaskIntoConstraints = false
    }

    // MARK: - Methods
    func configure(path: String?) {
        guard let path = path else {
            image = #imageLiteral(resourceName: "defaultImage")
            return
        }
        setImageView(path: path)
    }

    private func setImageView(path: String) {
        let file = host + path
        guard let url = URL(string: file) else {
            return
        }
        sd_setImage(with: url)
    }

    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

// MARK: - Extension
extension CustomSeeAllButton {}
