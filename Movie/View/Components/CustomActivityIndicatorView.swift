//
//  CustomActivityIndicatorView.swift
//  Movie
//
//  Created by Alwyn Yeo on 11/30/20.
//

import UIKit

// MARK: - Class
final class CustomActivityIndicatorView: UIActivityIndicatorView {
    
    // MARK: - Declarations (Properties)
    
    // MARK: - Declarations (UI)
    
    // MARK: - Overrides
    override init(style: UIActivityIndicatorView.Style) {
        super.init(style: style)
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        startAnimating()
        hidesWhenStopped = true
    }
    
    // MARK: - Methods
   
    // MARK: - //
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Extension
extension CustomActivityIndicatorView {}
