//
//  CustomNavigationController.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/28/20.
//

import UIKit

class CustomNavigationController: UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if let controller = viewControllers.last {
            return controller.preferredStatusBarStyle
        }
        return .default
    }
}
