//
//  CustomFavoriteEmptyView.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/31/20.
//

import UIKit

// MARK: - Protocol
protocol CustomFavoriteEmptyViewProtocol: AnyObject {
    func didPressButton(tag: Int)
}

// MARK: - Class
class CustomFavoriteEmptyView: UIView {
    
    // MARK: - Declarations (Properties)
    weak var delegate: CustomFavoriteEmptyViewProtocol?
    
    // MARK: - Declarations (UI)
    fileprivate let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "No Movies or TV Shows"
        label.textColor = .label
        label.font = .boldSystemFont(ofSize: 22)
        label.textAlignment = .center
        label.numberOfLines = .zero
        return label
    }()

    fileprivate let descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "Use the Home tab to discover new movies or tv shows, or search for your favorite movies or tv shows. Movies or TV Shows you add to favorites tab will appear here."
        label.textColor = .secondaryLabel
        label.font = .systemFont(ofSize: 14)
        label.textAlignment = .center
        label.numberOfLines = .zero
        return label
    }()

    fileprivate lazy var homeTabButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Discover From Home", for: .normal)
        button.setTitleColor(.systemBlue, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 17)
        button.titleLabel?.textAlignment = .center
        button.tag = .zero
        button.addTarget(self, action: #selector(handleTabButtons(_:)), for: .touchUpInside)
        return button
    }()

    fileprivate lazy var searchTabButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Search For a Movie or Tv Show", for: .normal)
        button.setTitleColor(.systemBlue, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 17)
        button.titleLabel?.textAlignment = .center
        button.tag = 1
        button.addTarget(self, action: #selector(handleTabButtons(_:)), for: .touchUpInside)
        return button
    }()

    fileprivate lazy var stackView: CustomStackView = {
        let stackView = CustomStackView(axis: .vertical, alignment: .center, spacing: 6, views: [
            titleLabel,
            descriptionLabel,
            homeTabButton,
            searchTabButton,
        ])
        return stackView
    }()
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .systemBackground
        configureStackView()
    }

    private func configureStackView() {
        addSubview(stackView)
        let constants = UIEdgeInsets(top: .zero, left: 32, bottom: .zero, right: 32)
        stackView.makeView(top: nil, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, constants: constants)
        stackView.makeCenterY(to: self)
    }

    // MARK: - Methods

    // MARK: - Objc Methods
    @objc private func handleTabButtons(_ sender: UIButton?) {
        guard let tag = sender?.tag else {
            return
        }
        delegate?.didPressButton(tag: tag)
    }

    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - Extension
extension CustomFavoriteEmptyViewProtocol {
    func didPressButton(tag: Int) {}
}
