//
//  HomeMovieCell.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/29/20.
//

import UIKit

// MARK: - Class
class HomeMovieCell: UICollectionViewCell {
    
    // MARK: - Declarations (Properties)
    private var item: MediaItem? {
        didSet {
            setData()
        }
    }
    private let padding: CGFloat = 8

    // MARK: - Declarations (UI)
    fileprivate let imageView = CustomImageView(cornerRadius: 5)

    fileprivate let titleLabel = CustomLabel(font: .boldSystemFont(ofSize: 20), text: "Title")

    fileprivate let taglineLabel = CustomLabel(text: "Tagline", textColor: .secondaryLabel, numberOfLines: 2)

    fileprivate let dateLabel = CustomLabel(font: .boldSystemFont(ofSize: 14), text: "Released Date • Language", textColor: .secondaryLabel)

    fileprivate lazy var stackView: CustomStackView = {
        let view = CustomStackView(axis: .vertical, distribution: .fillProportionally, views: [
            titleLabel,
            taglineLabel,
            dateLabel,
        ])
        return view
    }()
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Configuration
    private func setupView() {
        backgroundColor = .clear
        configureImageView()
        configureStackView()
    }

    private func configureImageView() {
        addSubview(imageView)
        let width = frame.width - 16
        let height = frame.height - 120 - 8
        let size = CGSize(width: width, height: height)
        let constants = UIEdgeInsets(top: padding, left: padding, bottom: .zero, right: padding)
        imageView.makeSize(size: size)
        imageView.makeView(top: topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, constants: constants)
    }

    private func configureStackView() {
        addSubview(stackView)
        let constants = UIEdgeInsets(top: padding, left: padding, bottom: .zero, right: padding)
        stackView.makeView(top: imageView.bottomAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: bottomAnchor, constants: constants)
    }
    
    // MARK: - Methods
    func configure(item: MediaItem) {
        self.item = item
    }

    private func setData() {
        let title = item?.title ?? item?.name ?? "Title"
        let tagline = item?.tagline ?? item?.overview ?? "The description is unavailable"
        titleLabel.text = title
        taglineLabel.text = tagline.isEmpty ? "The description is unavailable" : tagline
        setDateInfo()
        imageView.configure(path: item?.backdrop_path ?? item?.poster_path)
    }

    private func setDateInfo() {
        let date = (item?.release_date ?? item?.first_air_date) ?? "Date is unavailable"
        let languageCode = item?.original_language ?? "Unavailable"
        let language = languageCode == "cn" ? "Cantonese" : Locale.current.localizedString(forLanguageCode: languageCode) ?? languageCode
        dateLabel.text = date + " • " + language
    }
    
    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
