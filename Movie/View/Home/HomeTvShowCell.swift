//
//  HomeTvShowCell.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/29/20.
//

import UIKit

// MARK: - Class
class HomeTvShowCell: UICollectionViewCell {
    
    // MARK: - Declarations (Properties)
    private var item: MediaItem? {
        didSet {
            setData()
        }
    }
    private let padding: CGFloat = 8
    private let value: CGFloat = 80
    private lazy var imageWidth = value - padding

    // MARK: - Declarations (UI)
    private lazy var imageViewFrame = CGRect(x: .zero, y: .zero, width: 80, height: 80)
    fileprivate lazy var imageView = CustomImageView(frame: imageViewFrame, cornerRadius: 16)
    fileprivate let separatorView = CustomSeparatorView()

    fileprivate let titleLabel = CustomLabel(text: "Title")

    fileprivate let taglineLabel = CustomLabel(font: .systemFont(ofSize: 14), text: "Tagline", textColor: .secondaryLabel, numberOfLines: 2)

    fileprivate let dateLabel = CustomLabel(font: .boldSystemFont(ofSize: 14), text: "Released Date", textColor: .secondaryLabel)

    fileprivate lazy var stackView: CustomStackView = {
        let view = CustomStackView(axis: .vertical, distribution: .fillProportionally, spacing: 1, views: [
            titleLabel,
            taglineLabel,
            dateLabel,
        ])
        return view
    }()
    
    // MARK: - Overrides
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // MARK: - Configuration (UI)
    private func setupView() {
        backgroundColor = .clear
        configureImageView()
        configureStackView()
        configureSeparatorView()
    }

    private func configureImageView() {
        addSubview(imageView)
        let size = CGSize(width: imageWidth, height: imageWidth)
        let constants = UIEdgeInsets(top: .zero, left: padding, bottom: .zero, right: .zero)
        imageView.makeSize(size: size)
        imageView.makeView(top: nil, leading: leadingAnchor, trailing: nil, bottom: nil, constants: constants)
        imageView.makeCenterY(to: self)
    }

    private func configureStackView() {
        addSubview(stackView)
        let constants = UIEdgeInsets(top: .zero, left: 16, bottom: .zero, right: padding)
        stackView.makeView(top: imageView.topAnchor, leading: imageView.trailingAnchor, trailing: trailingAnchor, bottom: imageView.bottomAnchor, constants: constants)
        stackView.makeCenterY(to: self)
    }

    private func configureSeparatorView() {
        addSubview(separatorView)
        let width = frame.width - (imageView.frame.width + 24)
        let height: CGFloat = 0.6
        let size = CGSize(width: width, height: height)
        let constants = UIEdgeInsets(top: .zero, left: 16, bottom: .zero, right: .zero)
        separatorView.makeSize(size: size)
        separatorView.makeView(top: nil, leading: imageView.trailingAnchor, trailing: stackView.trailingAnchor, bottom: bottomAnchor, constants: constants)
    }

    // MARK: - Methods
    func configure(item: MediaItem) {
        self.item = item
    }

    private func setData() {
        let title = item?.title ?? item?.name ?? "Title"
        let tagline = item?.tagline ?? item?.overview ?? "The description is unavailable"
        titleLabel.text = title
        taglineLabel.text = tagline.isEmpty ? "The description is unavailable" : tagline
        setDateInfo()
        imageView.configure(path: item?.backdrop_path ?? item?.poster_path)
    }

    private func setDateInfo() {
        let date = (item?.release_date ?? item?.first_air_date) ?? "Date is unavailable"
        let languageCode = item?.original_language ?? "Unavailable"
        let language = languageCode == "cn" ? "Cantonese" : Locale.current.localizedString(forLanguageCode: languageCode) ?? languageCode
        dateLabel.text = date + " • " + language
    }

    // MARK: - //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
