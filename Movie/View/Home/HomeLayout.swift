//
//  HomeLayout.swift
//  Movie
//
//  Created by Alwyn Yeo on 12/29/20.
//

import UIKit

class HomeLayout: UICollectionViewCompositionalLayout {
    static func layout() -> UICollectionViewCompositionalLayout {
        .init { (section, _) -> NSCollectionLayoutSection? in
            if section < 4 {
                return horizontalSection()
            } else {
                return verticalSection()
            }
        }
    }

    private static func horizontalSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.892), heightDimension: .absolute(292))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        let sectionContentInsets = NSDirectionalEdgeInsets(top: .zero, leading: 24, bottom: 16, trailing: 16)
        let headerHeight: CGFloat = 54
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(headerHeight))
        let supplementaryItems = [NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)]
        let section = NSCollectionLayoutSection(group: group)
        section.boundarySupplementaryItems = supplementaryItems
        section.orthogonalScrollingBehavior = .continuousGroupLeadingBoundary
        section.contentInsets = sectionContentInsets
        return section
    }

    private static func verticalSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1/3))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.892), heightDimension: .absolute(300))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
        let sectionContentInsets = NSDirectionalEdgeInsets(top: .zero, leading: 24, bottom: 16, trailing: 16)
        let headerHeight: CGFloat = 34
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(headerHeight))
        let supplementaryItems = [NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)]
        let section = NSCollectionLayoutSection(group: group)
        section.boundarySupplementaryItems = supplementaryItems
        section.orthogonalScrollingBehavior = .groupPaging
        section.contentInsets = sectionContentInsets
        return section
    }
}
